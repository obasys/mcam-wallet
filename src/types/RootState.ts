import ReducerState from './ReducerState';

interface RootState {
    app: ReducerState;
}

export default RootState;
