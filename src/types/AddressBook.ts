type AddressBook = {
    address: string;
    title: string;
    favorite: boolean;
};

export default AddressBook;
