type Currency = {
    symbol: string;
    title: string;
};

export default Currency;
