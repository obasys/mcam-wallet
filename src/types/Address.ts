type Address = {
    address: string;
    wif: string;
    index: number;
};

export default Address;
