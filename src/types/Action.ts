type Action = {
    type: string;
    payload?: Record<string, any>;
};

export default Action;
