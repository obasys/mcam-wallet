type Token = {
    tokenName: string;
    received: number;
    locked: number;
    balance: number;
    units: number;
};

export default Token;
