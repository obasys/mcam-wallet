import { AddressBook, Wallet } from './index';
import Currency from './Currency';
import Network from './Network';

interface ReducerState {
    password: {
        type: 'pin' | 'password';
        useBiometric?: boolean;
        value?: string;
    };
    currencies?: Currency[];
    currency?: Currency;
    isLegalAgreed: boolean;
    networks: Network[];
    network: Network;
    wallets: Wallet[];
    addressBook: AddressBook[];
    isMigrated: boolean;
    name: string;
    mempoolUTXOs: string[];
}

export default ReducerState;
