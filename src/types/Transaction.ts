type Transaction = {
    type: 'sent' | 'received';
    confirmations: number;
    hash: string;
    amount: number;
    to?: string;
    from?: string;
    time: number;
    fee: number;
    tokens: {
        name: string;
        amount: number;
    }[];
    timelock?: {
        time: number;
        amount: number;
    };
};

export default Transaction;
