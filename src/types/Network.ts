type Network = {
    title: string;
    host: string;
    scriptHash: number;
    publicKeyHash: number;
    wif: number;
};

export default Network;
