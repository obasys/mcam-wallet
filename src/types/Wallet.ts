import { Transaction, Address, Token } from '.';

type Wallet = {
    title: string;
    seedPhrase: string;
    transactions: Transaction[];
    tokens: Token[];
    balance: {
        confirmed: number;
        locked: number;
    };
    depositAddress: string;
    addresses: Address[];
    createdAt: number;
    uuid: string;
};

export default Wallet;
