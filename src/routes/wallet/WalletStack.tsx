import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { useTranslation } from 'react-i18next';
import { Header } from '../../components';
import { Deposit, History, SendTransaction, Settings, Tokens, Withdraw } from '../../screens';

interface WalletStackNavigationProps {}

const Stack = createStackNavigator();

const WalletStack: React.FC<WalletStackNavigationProps> = ({}) => {
    const { t } = useTranslation();

    return (
        <Stack.Navigator
            initialRouteName="History"
            headerMode="screen"
            screenOptions={{
                header: (props) => <Header {...props} />,
                headerStyle: {
                    height: 110,
                },
            }}
        >
            <Stack.Screen
                name="Deposit"
                component={Deposit}
                options={{
                    title: t('screenTitles.wallet.deposit'),
                }}
            />
            <Stack.Screen
                name="History"
                component={History}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="Withdraw"
                component={Withdraw}
                options={{
                    title: t('screenTitles.wallet.withdraw'),
                }}
            />
            <Stack.Screen
                name="SendTransaction"
                component={SendTransaction}
                options={{
                    headerShown: false,
                    gestureEnabled: false,
                }}
            />
            <Stack.Screen
                name="Settings"
                component={Settings}
                options={{
                    title: t('screenTitles.wallet.settings'),
                }}
            />
            <Stack.Screen
                name="Tokens"
                options={{
                    title: t('screenTitles.wallet.tokens'),
                }}
                component={Tokens}
            />
        </Stack.Navigator>
    );
};

export default WalletStack;
