import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';

export type WalletParamList = {
    History: undefined;
    Deposit: undefined;
    Withdraw: undefined;
    Settings: undefined;
    TransactionDetails: undefined;
    SendTransaction: undefined;
    Tokens: undefined;
};

export type WalletNavProps<T extends keyof WalletParamList> = {
    navigation: StackNavigationProp<WalletParamList, T>;
    route: RouteProp<WalletParamList, T>;
};
