import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';

export type PasswordParamList = {
    Password: undefined;
    ChangePasswordMethod: undefined;
};

export type PasswordNavProps<T extends keyof PasswordParamList> = {
    navigation: StackNavigationProp<PasswordParamList, T>;
    route: RouteProp<PasswordParamList, T>;
};
