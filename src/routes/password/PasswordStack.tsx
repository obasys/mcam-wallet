import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { useTranslation } from 'react-i18next';
import { PasswordParamList } from './PasswordParamList';
import { Header } from '../../components';
import { ChangePasswordMethod, Password } from '../../screens';

interface PasswordStackNavigationProps {}

const Stack = createStackNavigator<PasswordParamList>();

const PasswordStack: React.FC<PasswordStackNavigationProps> = ({}) => {
    const { t } = useTranslation();

    return (
        <Stack.Navigator
            initialRouteName="Password"
            headerMode="screen"
            screenOptions={{
                header: (props) => <Header barStyle="light" iconColor="#614384" {...props} />,
                headerTransparent: true,
                headerStyle: {
                    height: 110,
                },
            }}
        >
            <Stack.Screen
                name="Password"
                component={Password}
                options={{
                    title: '',
                }}
            />
            <Stack.Screen
                name="ChangePasswordMethod"
                options={{
                    title: t('screenTitles.password.changePasswordMethod'),
                }}
                component={ChangePasswordMethod}
            />
        </Stack.Navigator>
    );
};

export default PasswordStack;
