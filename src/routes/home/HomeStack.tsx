import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/Ionicons';
import { useTranslation } from 'react-i18next';
import { HomeParamList } from './HomeParamList';
import { Header, IconButton } from '../../components';
import { DeleteWallet, DepositRequest, GlobalSettings, Home, Language, ManageNetwork, Networks } from '../../screens';

interface HomeStackNavigationProps {}

const Stack = createStackNavigator<HomeParamList>();

const HomeStack: React.FC<HomeStackNavigationProps> = ({}) => {
    const { t } = useTranslation();

    return (
        <Stack.Navigator
            initialRouteName="Home"
            headerMode="screen"
            screenOptions={{
                header: (props) => <Header {...props} />,
                headerStyle: {
                    height: 110,
                },
            }}
        >
            <Stack.Screen
                name="Home"
                component={Home}
                options={{
                    headerShown: false,
                    gestureEnabled: false,
                }}
            />
            <Stack.Screen
                name="GlobalSettings"
                options={{
                    title: t('screenTitles.home.globalSettings'),
                }}
                component={GlobalSettings}
            />
            <Stack.Screen
                name="Language"
                options={{
                    title: t('screenTitles.home.language'),
                }}
                component={Language}
            />
            <Stack.Screen
                name="ManageNetwork"
                options={{
                    title: t('screenTitles.home.manageNetwork'),
                }}
                component={ManageNetwork}
            />
            <Stack.Screen
                name="Networks"
                options={({ route, navigation }) => ({
                    title: t('screenTitles.home.networks'),
                    headerRight: () => (
                        <IconButton
                            onPress={() =>
                                navigation.navigate('ManageNetwork', {
                                    host: '',
                                    title: '',
                                    publicKeyHash: '',
                                    wif: '',
                                    scriptHash: '',
                                })
                            }
                            disableBackground
                        >
                            <Icon name="add" size={28} />
                        </IconButton>
                    ),
                })}
                component={Networks}
            />
            <Stack.Screen
                name="DeleteWallet"
                options={{
                    headerShown: false,
                    gestureEnabled: false,
                }}
                component={DeleteWallet}
            />
            <Stack.Screen
                name="DepositRequest"
                component={DepositRequest}
                options={{
                    title: t('screenTitles.home.depositRequest'),
                }}
            />
        </Stack.Navigator>
    );
};

export default HomeStack;
