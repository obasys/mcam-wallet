import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';

export type AddressBookParamList = {
    AddressBook: undefined;
    ManageAddressBookItem: undefined;
};

export type AddressBookNavProps<T extends keyof AddressBookParamList> = {
    navigation: StackNavigationProp<AddressBookParamList, T>;
    route: RouteProp<AddressBookParamList, T>;
};
