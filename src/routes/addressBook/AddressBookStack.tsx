import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/Ionicons';
import { useTranslation } from 'react-i18next';
import { AddressBookParamList } from './AddressBookParamList';
import { Header, IconButton } from '../../components';
import { AddressBook, ManageAddressBookItem } from '../../screens';

interface AddressBookStackNavigationProps {}

const Stack = createStackNavigator<AddressBookParamList>();

const AddressBookStack: React.FC<AddressBookStackNavigationProps> = ({}) => {
    const { t } = useTranslation();

    return (
        <Stack.Navigator
            initialRouteName="AddressBook"
            headerMode="screen"
            screenOptions={{
                header: (props) => <Header {...props} />,
                headerStyle: {
                    height: 110,
                },
            }}
        >
            <Stack.Screen
                name="AddressBook"
                options={({ route, navigation }) => ({
                    title: t('screenTitles.addressBook.addressBook'),
                    headerRight: () => (
                        <IconButton
                            onPress={() =>
                                navigation.navigate('ManageAddressBookItem', {
                                    address: '',
                                    title: '',
                                    favorite: false,
                                })
                            }
                            disableBackground
                        >
                            <Icon name="add" size={28} />
                        </IconButton>
                    ),
                })}
                component={AddressBook}
            />
            <Stack.Screen
                name="ManageAddressBookItem"
                options={{
                    title: t('screenTitles.addressBook.addAddress'),
                }}
                component={ManageAddressBookItem}
            />
        </Stack.Navigator>
    );
};

export default AddressBookStack;
