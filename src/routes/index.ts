import React from 'react';
import { NavigationContainerRef } from '@react-navigation/native';

import RootStack from './root/RootStack';
import HomeStack from './home/HomeStack';
import WalletStack from './wallet/WalletStack';
import AddressBookStack from './addressBook/AddressBookStack';
import OnboardingStack from './onboarding/OnboardingStack';
import PasswordStack from './password/PasswordStack';
import ModalStack from './modal/ModalStack';

const navigationRef = React.createRef<NavigationContainerRef>();

export {
    RootStack,
    HomeStack,
    WalletStack,
    AddressBookStack,
    OnboardingStack,
    PasswordStack,
    ModalStack,
    navigationRef,
};
