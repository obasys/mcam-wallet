import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';

export type RootParamList = {
    Splash: undefined;
    QRCodeScanner: undefined;
    HomeStack: undefined;
    WalletStack: undefined;
    OnboardingStack: undefined;
    AddressBookStack: undefined;
    PasswordStack: undefined;
    FactoryReset: undefined;
};

export type RootNavProps<T extends keyof RootParamList> = {
    navigation: StackNavigationProp<RootParamList, T>;
    route: RouteProp<RootParamList, T>;
};
