import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { useTranslation } from 'react-i18next';
import { RootParamList } from './RootParamList';
import { Header } from '../../components';
import { FactoryReset, QRCodeScanner, Splash } from '../../screens';
import { AddressBookStack, HomeStack, OnboardingStack, PasswordStack, WalletStack } from '../';

interface RootStackNavigationProps {}

const Stack = createStackNavigator<RootParamList>();

const RootStack: React.FC<RootStackNavigationProps> = ({}) => {
    const { t } = useTranslation();

    return (
        <Stack.Navigator
            initialRouteName="Splash"
            headerMode="screen"
            screenOptions={{
                headerShown: false,
                header: (props) => <Header barStyle="light" {...props} />,
                headerTransparent: true,
                headerStyle: {
                    height: 110,
                },
            }}
        >
            <Stack.Screen name="Splash" component={Splash} />
            <Stack.Screen name="FactoryReset" component={FactoryReset} />
            <Stack.Screen
                name="QRCodeScanner"
                options={{
                    headerShown: true,
                    title: t('screenTitles.home.qrCodeScanner'),
                }}
                component={QRCodeScanner}
            />
            <Stack.Screen name="HomeStack" component={HomeStack} />
            <Stack.Screen name="WalletStack" component={WalletStack} />
            <Stack.Screen name="OnboardingStack" component={OnboardingStack} />
            <Stack.Screen name="AddressBookStack" component={AddressBookStack} />
            <Stack.Screen
                name="PasswordStack"
                options={{
                    gestureEnabled: false,
                }}
                component={PasswordStack}
            />
        </Stack.Navigator>
    );
};

export default RootStack;
