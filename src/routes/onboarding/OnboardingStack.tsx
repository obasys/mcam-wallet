import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { useTranslation } from 'react-i18next';
import { OnboardingParamList } from './OnboardingParamList';
import { Header } from '../../components';
import { Finished, GenerateWallet, Legal, Protect, RecoveryPhrase, Welcome } from '../../screens';

interface OnboardingStackNavigationProps {}

const Stack = createStackNavigator<OnboardingParamList>();

const OnboardingStack: React.FC<OnboardingStackNavigationProps> = ({}) => {
    const { t } = useTranslation();

    return (
        <Stack.Navigator
            initialRouteName="Welcome"
            headerMode="screen"
            screenOptions={{
                header: (props) => <Header barStyle="dark" backgroundColor="transparent" {...props} />,
                headerTransparent: true,
                headerStyle: {
                    height: 110,
                },
            }}
        >
            <Stack.Screen
                name="Welcome"
                component={Welcome}
                options={{
                    title: '',
                }}
            />
            <Stack.Screen
                name="Legal"
                component={Legal}
                options={{
                    title: t('screenTitles.onboarding.legal'),
                }}
            />
            <Stack.Screen
                name="Protect"
                options={{
                    title: '',
                }}
                component={Protect}
            />
            <Stack.Screen
                name="RecoveryPhrase"
                options={{
                    title: t('screenTitles.onboarding.recoveryPhrase'),
                }}
                component={RecoveryPhrase}
            />
            <Stack.Screen
                name="Finished"
                options={{
                    title: '',
                }}
                component={Finished}
            />
            <Stack.Screen
                name="GenerateWallet"
                options={{
                    headerShown: false,
                    gestureEnabled: false,
                }}
                component={GenerateWallet}
            />
        </Stack.Navigator>
    );
};

export default OnboardingStack;
