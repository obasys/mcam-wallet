import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { useTranslation } from 'react-i18next';
import { ModalParamList } from './ModalParamList';
import { ChooseWallet, QRCodeRequest, RecoveryTips, TransactionDetails } from '../../screens';
import { RootStack } from '..';

interface RootStackNavigationProps {}

const Stack = createStackNavigator<ModalParamList>();

const ModalStack = ({}) => {
    const { t } = useTranslation();

    return (
        <Stack.Navigator
            mode="modal"
            headerMode="none"
            screenOptions={({ route, navigation }) => ({
                gestureEnabled: true,
                cardOverlayEnabled: true,
                ...(Platform.OS === 'android'
                    ? TransitionPresets.ModalTransition
                    : TransitionPresets.ModalPresentationIOS),
            })}
        >
            <Stack.Screen name="RootStack" component={RootStack} />
            <Stack.Screen name="RecoveryTips" component={RecoveryTips} />
            <Stack.Screen
                name="TransactionDetails"
                component={TransactionDetails}
                options={{
                    title: t('screenTitles.wallet.transactionDetails'),
                }}
            />
            <Stack.Screen name="ChooseWallet" component={ChooseWallet} />
            <Stack.Screen name="QRCodeRequest" component={QRCodeRequest} />
        </Stack.Navigator>
    );
};

export default ModalStack;
