import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';

export type ModalParamList = {
    Root: undefined;
    RecoveryTips: undefined;
    TransactionDetails: undefined;
    QRCodeScanner: undefined;
    RootStack: undefined;
    ChooseWallet: undefined;
    QRCodeRequest: undefined;
};

export type ModalNavProps<T extends keyof ModalParamList> = {
    navigation: StackNavigationProp<ModalParamList, T>;
    route: RouteProp<ModalParamList, T>;
};
