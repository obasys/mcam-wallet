import * as bitcoin from 'bitcoinjs-lib';
import * as bip39 from 'bip39';
import * as bip32 from 'bip32';
import Config from 'react-native-config';

const network = {
    bip32: {
        public: 76066276,
        private: 76067358,
    },
    pubKeyHash: parseInt(Config.PUB_KEY_HASH),
    scriptHash: parseInt(Config.SCRIPT_HASH),
    wif: parseInt(Config.WIF),
    messagePrefix: '\x18Bitcoin Signed Message:\n',
    bech32: 'bc',
};

export const getAddress = (node: any, networkAddress: any) => {
    return bitcoin.payments.p2pkh({ pubkey: node.publicKey, network: networkAddress }).address!;
};

export const isAddress = (address: string) => {
    if (address.length <= 34 && address.match(new RegExp(Config.ADDRESS_REGEX))) {
        return true;
    }

    return false;
};

export const generateSeedPhrase = (size = 12) => {
    const mnemonic = require('assets/mnemonics.json');
    const seedPhrase: string[] = [];
    const randomNumbers: number[] = [];

    for (let i = 0; i < size; i++) {
        while (true) {
            const num = Math.floor(Math.random() * mnemonic.words.length);

            if (!randomNumbers.includes(num)) {
                randomNumbers.push(num);
                seedPhrase.push(mnemonic.words[num]);
                break;
            }
        }
    }

    return seedPhrase;
};

export const generateAddresses = (
    seedPhrase: string,
    startIndex = 0,
    endIndex = 0,
    derive: number,
    networkAddress: any = network,
    derivePath: string = Config.DERIVATION_PATH,
) => {
    const seed = bip39.mnemonicToSeedSync(seedPhrase);
    const root = bip32.fromSeed(seed, networkAddress);
    const branch = root.derivePath(`${derivePath}${derive}`);
    const result = [];

    for (let i = startIndex; i <= endIndex; i++) {
        const child = branch.derive(i);
        result.push({ index: i, wif: child.toWIF(), address: getAddress(child, networkAddress) });
    }

    return result;
};
