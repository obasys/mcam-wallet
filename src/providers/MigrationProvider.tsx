import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { showMessage } from 'react-native-flash-message';
import { AsyncStorage as DeprecatedAsyncStorage } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { getStoredState, purgeStoredState } from 'redux-persist';
import { v4 as uuidv4 } from 'uuid';
import { useTranslation } from 'react-i18next';
import SInfo from 'react-native-sensitive-info';
import { setMigrated, setNewWallet, setPassword } from '../redux/actions/appActions';
import { encryptData } from '../services/commonService';
import { RootState } from '../types';

interface MigrationProviderProps {
    children: any;
}

type MigrationContextType = {};

const migrationContextState: MigrationContextType = {};

export const MigrationContext = React.createContext<MigrationContextType>(migrationContextState);

export const MigrationProvider = ({ children }: MigrationProviderProps) => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const appReducer = useSelector((state: RootState) => state.app);
    const [oldState, setOldState] = useState<object | undefined>();

    const handleOldState = async () => {
        try {
            let unlockedPassword = '';
            let password = {
                value: '',
                useBiometric: false,
                type: 'password',
            };

            for (const timestamp in oldState.password) {
                unlockedPassword = oldState.password[timestamp].value;
                password = {
                    value: encryptData(oldState.password[timestamp].value, oldState.password[timestamp].value),
                    useBiometric: oldState.password[timestamp].useBiometric,
                    type: 'password',
                };

                try {
                    const settedItem = await SInfo.setItem('password', unlockedPassword, {
                        sharedPreferencesName: 'mySharedPrefs',
                        keychainService: 'myKeychain',
                        touchID: true,
                        showModal: true,
                        kSecAccessControl: 'kSecAccessControlBiometryAny',
                    });
                } catch (e) {
                    password.useBiometric = false;
                    showMessage({
                        message: t('migration.alerts.message'),
                        description: t('migration.alerts.description'),
                        type: 'danger',
                    });
                }

                dispatch(setPassword(password));

                break;
            }

            for (const timestamp in oldState.wallet) {
                if (timestamp !== 'isCreated' && timestamp !== 'defaultLanguage') {
                    const wallet = {
                        title: oldState.wallet[timestamp].title,
                        seedPhrase: encryptData(oldState.wallet[timestamp].seedPhrase.join(' '), unlockedPassword),
                        transactions: [],
                        balance: {
                            confirmed: oldState.wallet[timestamp].balance.confirmed,
                            locked: oldState.wallet[timestamp].balance.unconfirmed,
                        },
                        depositAddress: oldState.wallet[timestamp].receiveAddress,
                        addresses: [],
                        tokens: [],
                        uuid: uuidv4(),
                    };

                    const migratedAddresses = [];

                    for (const address in oldState.wallet[timestamp].addresses) {
                        migratedAddresses.push({
                            address,
                            wif: encryptData(
                                oldState.wallet[timestamp].addresses[address].privateKey,
                                unlockedPassword,
                            ),
                            index: oldState.wallet[timestamp].addresses[address].index,
                        });
                    }

                    wallet.addresses = [...migratedAddresses];
                    dispatch(setNewWallet(wallet));
                }
            }

            oldState && AsyncStorage.setItem('OLD_STATE', JSON.stringify(oldState));

            purgeStoredState({
                key: 'LIFTED_REDUX_STORE',
                storage: DeprecatedAsyncStorage,
            }).then(() => {
                dispatch(setMigrated({ isMigrated: true }));
            });
        } catch (e) {
            showMessage({
                message: t('migration.alerts.message'),
                description: e,
                type: 'danger',
            });
        }
    };

    useEffect(() => {
        if (!appReducer.isMigrated) {
            getStoredState({
                key: 'LIFTED_REDUX_STORE',
                storage: DeprecatedAsyncStorage,
            }).then((res) => (res === undefined ? dispatch(setMigrated({ isMigrated: true })) : setOldState(res)));
        }
    }, []);

    useEffect(() => {
        if (oldState !== undefined && 'wallet' in oldState && 'password' in oldState) {
            handleOldState();
        } else if (oldState !== undefined && (!('wallet' in oldState) || !('password' in oldState))) {
            dispatch(setMigrated({ isMigrated: true }));
        }
    }, [oldState]);

    return <MigrationContext.Provider value={{}}>{children}</MigrationContext.Provider>;
};
