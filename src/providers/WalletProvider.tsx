import React, { useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as bitcoin from 'bitcoinjs-lib';
import Config from 'react-native-config';
import { RootState, Token, Transaction, Wallet } from '../types';
import { PasswordContext, SocketContext } from '.';
import { updateWallet } from '../redux/actions/appActions';
import { decryptData, writeUInt64LE } from '../services/commonService';

interface WalletProviderProps {
    children: any;
}

type WalletContextType = {
    wallet?: Wallet;
    uuid?: string;
    setUuid: (arg1: string) => void;
    updateWalletInfo?: () => void;
    deleteTransactions?: () => void;
    changeWalletTitle?: (arg0: string) => void;
    sendTransation?: (withdrawAddress: string, amount: number, fee: number, timelock?: number) => Promise<void>;
    setStatuses?: React.Dispatch<any>;
    statuses?: {
        history?: 'syncing' | 'synced';
        withdraw?: 'sending' | 'sent' | 'waiting' | 'failed';
    };
};

const walletContextState: WalletContextType = {
    setUuid: () => {},
};

export const WalletContext = React.createContext<WalletContextType>(walletContextState);

export const WalletProvider = ({ children }: WalletProviderProps) => {
    const dispatch = useDispatch();
    const appReducer = useSelector((state: RootState) => state.app);
    const { socketRequest, socket } = useContext(SocketContext);
    const { unlockedPassword } = useContext(PasswordContext);
    const [uuid, setWalletUuid] = useState<string | undefined>();
    const [statuses, setStatuses] = useState<{
        history: 'syncing' | 'synced';
        withdraw: 'sending' | 'waiting' | 'sent' | 'failed';
    }>({
        history: 'synced',
        withdraw: 'waiting',
    });
    const wallet = appReducer.wallets.find((wallet: Wallet) => wallet.uuid === uuid);

    const [network, setNetwork] = useState({
        bip32: {
            public: 76066276,
            private: 76067358,
        },
        pubKeyHash: appReducer.network ? parseInt(appReducer.network.publicKeyHash) : parseInt(Config.PUB_KEY_HASH),
        scriptHash: appReducer.network ? parseInt(appReducer.network.scriptHash) : parseInt(Config.SCRIPT_HASH),
        wif: appReducer.network ? parseInt(appReducer.network.wif) : parseInt(Config.WIF),
        messagePrefix: '\x18Bitcoin Signed Message:\n',
        bech32: 'bc',
    });

    const getBalanceFromTransactions = (transactions: Transaction[]) => {
        const currentTime = Date.now() / 1000;
        const balance = {
            confirmed: 0,
            locked: 0,
        };

        for (let i = 0; i < transactions.length; ++i) {
            if (transactions[i].type == 'sent') {
                balance.confirmed -= transactions[i].amount;
            } else {
                balance.confirmed += transactions[i].amount;
            }

            if (transactions[i].timelock) {
                if (transactions[i].timelock!.time > 50000000 && transactions[i].timelock!.time > currentTime) {
                    balance.locked += transactions[i].timelock!.amount;
                } else if (transactions[i].timelock!.time <= 50000000) {
                    balance.locked += transactions[i].timelock!.amount;
                }
            }
        }

        balance.confirmed -= balance.locked;
        return balance;
    };

    const filterTransactionHashes = (blockchainTransactionsHashes: string[]) => {
        const localTransactionHashes = wallet?.transactions
            .filter((transaction: Transaction) => transaction.confirmations >= 1)
            .map((transaction: Transaction) => transaction.hash);
        const filteredBlockchainTransactionsHashes = [...new Set([...blockchainTransactionsHashes])];
        return filteredBlockchainTransactionsHashes.filter((hash: string) => !localTransactionHashes?.includes(hash));
    };

    const getTransactionHashesBy = async (address: string) => {
        return (await socketRequest!('address.history', [address])).tx;
    };

    const tokenOutput = (address: string, token: { name: string; amount: number }, timelock = 0) => {
        const tokenBuffer = Buffer.allocUnsafe(3 + 1 + 1 + token.name.length + 8 + 4);
        let offset = 0;

        tokenBuffer.write('Mlp', offset);
        offset += 3;
        tokenBuffer.write('t', offset);
        offset += 1;
        tokenBuffer.writeUInt8(token.name.length, offset);
        offset += 1;
        tokenBuffer.write(token.name, offset);
        offset += token.name.length;
        writeUInt64LE(tokenBuffer, token.amount, offset);
        offset += 8;

        if (timelock) {
            tokenBuffer.writeInt32LE(timelock, offset);
        }

        return bitcoin.script.compile([
            bitcoin.opcodes.OP_DUP,
            bitcoin.opcodes.OP_HASH160,
            bitcoin.address.fromBase58Check(address, network).hash,
            bitcoin.opcodes.OP_EQUALVERIFY,
            bitcoin.opcodes.OP_CHECKSIG,
            bitcoin.opcodes.OP_ALP_TOKEN,
            tokenBuffer,
            bitcoin.opcodes.OP_DROP,
        ]);
    };

    const getTokens = async () => {
        const walletAddresses: string[] = [...new Set<string>(wallet.addresses.map((address: any) => address.address))];
        let tokens: Token[] = [];

        for (let i = 0; i < walletAddresses.length; i++) {
            const addressBalance = await socketRequest!('address.balance', [walletAddresses[i]]);

            console.log('addressBalance', walletAddresses[i], addressBalance);

            if (addressBalance) {
                for (var j = 0; j < addressBalance.tokens.length; ++j) {
                    let token = tokens.find((item) => item.tokenName === addressBalance.tokens[j].tokenName);

                    if (token) {
                        token = {
                            ...token,
                            balance: addressBalance.tokens[j].balance + token.balance,
                            locked: addressBalance.tokens[j].locked + token.locked,
                            received: addressBalance.tokens[j].received + token.received,
                        };

                        tokens = [
                            ...tokens.filter((item) => item.tokenName !== addressBalance.tokens[j].tokenName),
                            token,
                        ];
                    } else {
                        tokens.push(addressBalance.tokens[j]);
                    }
                }
            }
        }

        return tokens;
    };

    const createTransaction = async (walletAddresses: string[], transactionVerbose: any) => {
        const transaction: Transaction = {
            hash: transactionVerbose.txid,
            confirmations: transactionVerbose.confirmations ? transactionVerbose.confirmations : 0,
            amount: 0,
            fee: 0,
            time: 'time' in transactionVerbose ? transactionVerbose.time : Math.round(Date.now() / 1000),
            type: 'received',
            tokens: [],
        };

        let isCoinbase = false;

        for (let k = 0; k < transactionVerbose.vin.length; k++) {
            if ('coinbase' in transactionVerbose.vin[k]) {
                transaction.from = 'coinbase';
                isCoinbase = true;
                continue;
            }

            if (walletAddresses.includes(transactionVerbose.vin[k].scriptPubKey.addresses[0])) {
                if (transactionVerbose.vin[k].scriptPubKey.type == 'transfer_token') {
                    const tokenIndex = transaction.tokens.findIndex(
                        (token) => token.name === transactionVerbose.vin[k].scriptPubKey.token.name,
                    );

                    if (tokenIndex !== -1) {
                        transaction.tokens[tokenIndex].amount += transactionVerbose.vin[k].scriptPubKey.token.amount;
                    } else {
                        transaction.tokens.push(transactionVerbose.vin[k].scriptPubKey.token);
                    }
                }

                transaction.amount += transactionVerbose.vin[k].value;
                transaction.type = 'sent';
                transaction.from = transactionVerbose.vin[k].scriptPubKey.addresses[0];
            }

            transaction.fee += !isCoinbase && transactionVerbose.vin[k].value;
        }

        if (transaction.amount == 0) {
            transaction.type = 'received';

            for (let k = 0; k < transactionVerbose.vout.length; k++) {
                if (transactionVerbose.vout[k].scriptPubKey.type != 'nonstandard') {
                    if (walletAddresses.includes(transactionVerbose.vout[k].scriptPubKey.addresses[0])) {
                        if (transactionVerbose.vout[k].scriptPubKey.type == 'transfer_token') {
                            const tokenIndex = transaction.tokens.findIndex(
                                (token) => token.name === transactionVerbose.vout[k].scriptPubKey.token.name,
                            );

                            if (tokenIndex !== -1) {
                                transaction.tokens[tokenIndex].amount +=
                                    transactionVerbose.vout[k].scriptPubKey.token.amount;
                            } else {
                                transaction.tokens.push(transactionVerbose.vout[k].scriptPubKey.token);
                            }
                        }

                        transaction.amount += transactionVerbose.vout[k].value;
                        transaction.to = transactionVerbose.vout[k].scriptPubKey.addresses[0];

                        if (transactionVerbose.vout[k].scriptPubKey.type == 'cltv') {
                            transaction.timelock = {
                                amount: transactionVerbose.vout[k].value,
                                time: transactionVerbose.vout[k].scriptPubKey.asm.split(' ')[0],
                            };
                        }
                    } else if (!isCoinbase) {
                        transaction.from = transactionVerbose.vout[k].scriptPubKey.addresses[0];
                    }

                    transaction.fee -= !isCoinbase && transactionVerbose.vout[k].value;
                }
            }
        } else {
            for (let k = 0; k < transactionVerbose.vout.length; k++) {
                if (transactionVerbose.vout[k].scriptPubKey.type != 'nonstandard') {
                    if (walletAddresses.includes(transactionVerbose.vout[k].scriptPubKey.addresses[0])) {
                        if (transactionVerbose.vout[k].scriptPubKey.type == 'transfer_token') {
                            const tokenIndex = transaction.tokens.findIndex(
                                (token) => token.name === transactionVerbose.vout[k].scriptPubKey.token.name,
                            );

                            if (tokenIndex !== -1) {
                                transaction.tokens[tokenIndex].amount -=
                                    transactionVerbose.vout[k].scriptPubKey.token.amount;
                            } else {
                                transaction.tokens.push(transactionVerbose.vout[k].scriptPubKey.token);
                            }
                        }

                        transaction.amount -= transactionVerbose.vout[k].value;

                        if (transactionVerbose.vout[k].scriptPubKey.type == 'cltv') {
                            transaction.timelock = {
                                amount: transactionVerbose.vout[k].value,
                                time: transactionVerbose.vout[k].scriptPubKey.asm.split(' ')[0],
                            };
                        }
                    } else {
                        transaction.to = transactionVerbose.vout[k].scriptPubKey.addresses[0];
                    }

                    transaction.fee -= !isCoinbase && transactionVerbose.vout[k].value;
                }
            }

            if (!('to' in transaction)) {
                transaction.to = transaction.from;
            }

            if (transaction.amount < 0) {
                transaction.type = 'received';
                transaction.amount *= -1;
            }

            for (let i = 0; i < transaction.tokens.length; ++i) {
                if (transaction.tokens[i].amount < 0) {
                    transaction.tokens[i].amount = transaction.tokens[i].amount * -1;
                }
            }
        }

        return transaction;
    };

    const updateWalletInfo = async (addresses?: string[]) => {
        setStatuses((prevState) => ({ ...prevState, history: 'syncing' }));

        const walletAddresses: string[] = addresses || [
            ...new Set<string>(wallet.addresses.map((address: any) => address.address)),
        ];
        let transactionsHashes: string[] = [];
        const promises: any = [];
        const newWalletTransactions: Transaction[] = [];

        try {
            for (let i = 0; i < walletAddresses.length; ++i) {
                transactionsHashes = [...transactionsHashes, ...(await getTransactionHashesBy(walletAddresses[i]))];
            }

            transactionsHashes = filterTransactionHashes(transactionsHashes);
            const transactions = await socketRequest!('transaction.batch', [transactionsHashes]);

            for (let i = 0; i < transactions.length; i++) {
                promises.push(
                    createTransaction(walletAddresses, transactions[i].result).then((transaction) => {
                        newWalletTransactions.push(transaction);
                    }),
                );
            }

            await Promise.all(promises);
        } catch (e) {
            setStatuses((prevState) => ({ ...prevState, history: 'synced' }));
        }

        const transactions = [...newWalletTransactions, ...wallet.transactions].filter(
            (transaction, index, self) => index === self.findIndex((t) => t.hash === transaction.hash),
        );

        const tokens = await getTokens();

        dispatch(updateWallet({ ...wallet, transactions, tokens, balance: getBalanceFromTransactions(transactions) }));

        setStatuses((prevState) => ({ ...prevState, history: 'synced' }));
    };

    const addTransactions = async (hashes: string[]) => {
        const promises: any = [];
        const newWalletTransactions: Transaction[] = [];

        if (hashes.length > 0) {
            const walletAddresses: string[] = wallet.addresses.map((address: any) => address.address);

            try {
                const transactions = await socketRequest!('transaction.batch', [hashes]);

                for (let i = 0; i < transactions.length; i++) {
                    promises.push(
                        createTransaction(walletAddresses, transactions[i].result).then((transaction) => {
                            newWalletTransactions.push(transaction);
                        }),
                    );
                }

                await Promise.all(promises);
            } catch (e) {
                throw e;
            }

            const transactions = [...newWalletTransactions, ...wallet.transactions].filter(
                (transaction, index, self) => index === self.findIndex((t) => t.hash === transaction.hash),
            );

            dispatch(updateWallet({ uuid, transactions, balance: getBalanceFromTransactions(transactions) }));
        }
    };

    const deleteTransactions = () => {
        dispatch(updateWallet({ ...wallet, transactions: [], balance: { confirmed: 0, locked: 0 } }));
    };

    const changeWalletTitle = (title: string) => {
        dispatch(updateWallet({ ...wallet, title }));
    };

    const sendTransation = async (
        withdrawAddress: string,
        amount: number,
        fee: number,
        timelock = 0,
        token: Token | undefined = undefined,
    ) => {
        setStatuses((prevState) => ({ ...prevState, withdraw: 'sending' }));

        const walletAddresses = wallet.addresses.filter(
            (address, index, self) => index === self.findIndex((a) => a.address === address.address),
        );

        let outputsAmount = 0;
        let tokenOutputsAmount = 0;
        const keyPairs = [];
        const scripts = [];
        const mempoolUTXOs = [];
        const txb = new bitcoin.TransactionBuilder(network);
        const timestamp = Math.floor(Date.now() / 1000);

        txb.setVersion(1);
        txb.setTimestamp(timestamp);

        if (token) {
            for (let i = 0; i < walletAddresses.length; i++) {
                const utxo = await socketRequest!('address.unspent', [
                    walletAddresses[i].address,
                    token.received,
                    token.tokenName,
                ]);

                if (utxo.length === 0) {
                    const utxo = await socketRequest!('address.unspent', [
                        walletAddresses[i].address,
                        0,
                        token.tokenName,
                    ]);
                }

                for (let k = 0; k < utxo.length; k++) {
                    if (Math.round(tokenOutputsAmount - amount) > 0) {
                        break;
                    }

                    txb.addInput(utxo[k].txid, utxo[k].index);

                    scripts.push(new Buffer(utxo[k].script, 'hex'));
                    keyPairs.push(
                        bitcoin.ECPair.fromWIF(decryptData(walletAddresses[i].wif, unlockedPassword!), network),
                    );

                    tokenOutputsAmount += parseInt(utxo[k].value);
                }
            }
        }

        try {
            for (let i = 0; i < walletAddresses.length; i++) {
                if ((await socketRequest!('address.balance', [walletAddresses[i].address])).balance > 0) {
                    const utxo = await socketRequest!('address.unspent', [walletAddresses[i].address]);

                    for (let k = 0; k < utxo.length; k++) {
                        /* if ("mempoolUTXOs" in wallet && wallet.mempoolUTXOs.includes(utxo[k].script)) {
break;
} */

                        if (token) {
                            if (outputsAmount - fee > 0) {
                                break;
                            }
                        } else if (outputsAmount - amount - fee > 0) {
                            break;
                        }

                        const decodedScript = bitcoin.script.decompile(new Buffer(utxo[k].script, 'hex'));

                        if (decodedScript && decodedScript.includes(bitcoin.opcodes.OP_CHECKLOCKTIMEVERIFY)) {
                            if (Buffer.isBuffer(decodedScript[0])) {
                                if (decodedScript[0].readUIntLE(0, decodedScript[0].length) <= 50000000) {
                                    txb.setLockTime((await socketRequest!('general.info')).blocks);
                                } else {
                                    txb.setLockTime((await socketRequest!('general.info')).mediantime);
                                }
                            } else {
                                txb.setLockTime((await socketRequest!('general.info')).blocks);
                            }

                            txb.addInput(utxo[k].txid, utxo[k].index, 0xfffffffe);
                        } else {
                            txb.addInput(utxo[k].txid, utxo[k].index);
                        }

                        scripts.push(new Buffer(utxo[k].script, 'hex'));
                        mempoolUTXOs.push(utxo[k].script);

                        keyPairs.push(
                            bitcoin.ECPair.fromWIF(decryptData(walletAddresses[i].wif, unlockedPassword!), network),
                        );

                        outputsAmount += parseInt(utxo[k].value);
                    }
                }
            }

            if (outputsAmount < fee) {
                throw 'Output amount error. No available UTXO for spending.';
            }

            if (token) {
                if (tokenOutputsAmount - amount > 0) {
                    txb.addOutput(
                        tokenOutput(wallet.depositAddress, {
                            name: token.tokenName,
                            amount: tokenOutputsAmount - amount,
                        }),
                        0,
                    );
                }

                if (timelock > 0) {
                    txb.addOutput(tokenOutput(withdrawAddress, { name: token.tokenName, amount }, timelock), 0);
                } else {
                    txb.addOutput(tokenOutput(withdrawAddress, { name: token.tokenName, amount }), 0);
                }

                if (outputsAmount - fee > 0) {
                    txb.addOutput(wallet.depositAddress, outputsAmount - fee);
                }
            } else {
                if (outputsAmount - amount - fee > 0) {
                    txb.addOutput(wallet.depositAddress, outputsAmount - amount - fee);
                }

                if (!isNaN(timelock) && timelock > 0) {
                    txb.addOutput(
                        bitcoin.script.compile([
                            bitcoin.script.number.encode(timelock),
                            bitcoin.opcodes.OP_CHECKLOCKTIMEVERIFY,
                            bitcoin.opcodes.OP_DROP,
                            bitcoin.opcodes.OP_DUP,
                            bitcoin.opcodes.OP_HASH160,
                            bitcoin.address.fromBase58Check(withdrawAddress).hash,
                            bitcoin.opcodes.OP_EQUALVERIFY,
                            bitcoin.opcodes.OP_CHECKSIG,
                        ]),
                        amount,
                    );
                } else {
                    txb.addOutput(withdrawAddress, amount);
                }
            }

            const tx = txb.buildIncomplete();

            for (let i = 0; i < keyPairs.length; i++) {
                const signatureHash = tx.hashForSignature(i, scripts[i], bitcoin.Transaction.SIGHASH_ALL);

                tx.setInputScript(
                    i,
                    bitcoin.script.compile([
                        bitcoin.script.signature.encode(
                            keyPairs[i].sign(signatureHash),
                            bitcoin.Transaction.SIGHASH_ALL,
                        ),
                        keyPairs[i].publicKey,
                    ]),
                );
            }

            const txHex = tx.toHex();

            console.log("TX: ", txHex)

            const broadcast = await socketRequest!('transaction.broadcast', [txHex]);

            if (broadcast.match(new RegExp(Config.TRANSACTION_REGEX))) {
                await addTransactions([broadcast]);
            } else {
                throw Error('Failed broadcasting transaction');
            }
        } catch (e) {
            throw Error(e);
        }

        // dispatch(updateWallet({uuid, mempoolUTXOs: "mempoolUTXOs" in wallet ? [...mempoolUTXOs,  ...wallet.mempoolUTXOs] : mempoolUTXOs}));
        setStatuses((prevState) => ({ ...prevState, withdraw: 'sent' }));
    };

    const subscribeToAddresses = async () => {
        for (let i = 0; i < wallet.addresses.length; ++i) {
            const sub = await socketRequest!('subscribe.address', [wallet.addresses[i].address]);
        }
    };

    const unsubscribeFromAddresses = async (walletAddresses: string[]) => {
        socket.removeAllListeners('address.update');

        for (let i = 0; i < walletAddresses.length; ++i) {
            await socketRequest!('unsubscribe.address', [walletAddresses[i]]);
        }
    };

    const setUuid = (uuid: string) => {
        if (wallet) {
            const walletAddresses = wallet.addresses.map((address: any) => address.address);
            unsubscribeFromAddresses(walletAddresses);
        }

        setWalletUuid(uuid);
    };

    useEffect(() => {
        if (uuid && uuid.length > 0) {
            subscribeToAddresses();

            socket.on('address.update', (data: any) => {
                addTransactions(data.result.tx);
            });
        }
    }, [uuid]);

    useEffect(() => {
        if (appReducer.network) {
            setNetwork({
                bip32: {
                    public: 76066276,
                    private: 76067358,
                },
                pubKeyHash: parseInt(appReducer.network.publicKeyHash),
                scriptHash: parseInt(appReducer.network.scriptHash),
                wif: parseInt(appReducer.network.wif),
                messagePrefix: '\x18Bitcoin Signed Message:\n',
                bech32: 'bc',
            });
        }
    }, [appReducer.network]);

    return (
        <WalletContext.Provider
            value={{
                wallet,
                uuid,
                setUuid,
                updateWalletInfo,
                deleteTransactions,
                changeWalletTitle,
                sendTransation,
                statuses,
                setStatuses,
            }}
        >
            {children}
        </WalletContext.Provider>
    );
};
