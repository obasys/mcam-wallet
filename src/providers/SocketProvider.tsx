import React, { useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { AppState, AppStateStatus } from 'react-native';
import io from 'socket.io-client';
import Config from 'react-native-config';
import * as Promise from 'bluebird';
import { RootState } from '../types';

interface SocketProviderProps {
    children: any;
}

type RequestMethod =
    | 'general.info'
    | 'general.tokens'
    | 'general.fee'
    | 'address.balance'
    | 'address.tokens'
    | 'address.unspent'
    | 'address.history'
    | 'address.check'
    | 'address.mempool.raw'
    | 'transaction.info'
    | 'transaction.batch'
    | 'transaction.broadcast'
    | 'subscribe.blocks'
    | 'subscribe.address'
    | 'unsubscribe.address';

type SocketContextType = {
    socket?: any;
    socketRequest?: (method: RequestMethod, params?: Array<any>) => any;
    isConnected: boolean;
};

const socketContextState: SocketContextType = {
    isConnected: false,
};

export const SocketContext = React.createContext<SocketContextType>(socketContextState);

export const SocketProvider = ({ children }: SocketProviderProps) => {
    const appState = useRef(AppState.currentState);
    const appReducer = useSelector((state: RootState) => state.app);
    console.log(appReducer.network);
    const [socket, setSocket] = useState(
        io(appReducer.network && appReducer.network.host !== '' ? appReducer.network.host : Config.MAIN_NETWORK_HOST, {
            transports: ['websocket'],
            autoConnect: true,
            reconnection: true,
        }),
    );
    const [isConnected, setIsConnected] = useState(false);

    socket.on('connect', () => {
        setIsConnected(socket.connected);
    });

    socket.on('reconnect', () => {
        console.log('reconnect');
    });

    const socketRequest = async (method: RequestMethod, params: Array<any> = []) => {
        if (!isConnected) {
            await new Promise((resolve: any) => setTimeout(resolve, 1000));

            if (!isConnected) {
                throw new Error(`Socket is not connected!`);
            }
        }

        try {
            let result = await socket.emitAsync(method, ...params!);
        } catch (data) {
            if (data.error) {
                throw new Error(`${method}: ${JSON.stringify(data.error)}`);
            }

            return data.result;
        }
    };

    const handleAppStateChange = (nextAppState: AppStateStatus) => {
        if (appState.current.match(/inactive|background/) && nextAppState === 'active') {
            socket.connect();
        }

        appState.current = nextAppState;
    };

    useEffect(() => {
        AppState.addEventListener('change', handleAppStateChange);

        return () => {
            AppState.removeEventListener('change', handleAppStateChange);
        };
    }, []);

    useEffect(() => {
        socket.emitAsync = Promise.promisify(socket.emit);
    }, [socket]);

    useEffect(() => {
        if (appReducer.network && appReducer.network.host !== '') {
            socket.close();
            setSocket(
                io(appReducer.network.host, { transports: ['websocket'], autoConnect: true, reconnection: true }),
            );
        } else {
            socket.close();
            setSocket(
                io(Config.MAIN_NETWORK_HOST, { transports: ['websocket'], autoConnect: true, reconnection: true }),
            );
        }
    }, [appReducer.network]);

    return <SocketContext.Provider value={{ socket, socketRequest, isConnected }}>{children}</SocketContext.Provider>;
};
