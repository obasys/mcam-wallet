import {
    CLEAR_REDUCER_KEY,
    DELETE_ADDRESS_BOOK_ITEM,
    DELETE_NETWORK,
    DELETE_WALLET,
    RESET,
    SAVE_ADDRESS_BOOK_ITEM,
    SAVE_NETWORK,
    SET_LEGAL,
    SET_MIGRATED,
    SET_NAME,
    SET_NETWORK,
    SET_NEW_WALLET,
    SET_PASSWORD,
    SET_TOKENS,
    UNSET_NETWORK,
    UPDATE_WALLET,
    UPDATE_WALLETS,
} from '../actions/appActions';

import { Action, ReducerState, Wallet } from '../../types';

const initialState: ReducerState = {
    isLegalAgreed: false,
    password: {
        type: 'pin',
        value: undefined,
        useBiometric: undefined,
    },
    currency: undefined,
    currencies: [],
    network: undefined,
    networks: [],
    wallets: [],
    addressBook: [],
    isMigrated: false,
    name: '',
};

const initialWallet: Wallet = {
    title: '',
    seedPhrase: '',
    transactions: [],
    tokens: [],
    balance: {
        confirmed: 0,
        locked: 0,
    },
    depositAddress: '',
    addresses: [],
    createdAt: Date.now() / 1000,
    uuid: '',
    mempoolUTXOs: [],
};

const appReducer = (state = initialState, action: Action) => {
    switch (action.type) {
        case SET_PASSWORD: {
            return {
                ...state,
                password: { ...state.password, ...action.payload },
            };
        }

        case SET_LEGAL: {
            return {
                ...state,
                isLegalAgreed: action.payload!.isLegalAgreed,
            };
        }

        case SET_TOKENS: {
            return {
                ...state,
                tokens: action.payload,
            };
        }

        case SET_NAME: {
            return {
                ...state,
                name: action.payload!.name,
            };
        }

        case SET_NEW_WALLET: {
            return {
                ...state,
                wallets: [...state.wallets, { ...initialWallet, ...action.payload, createdAt: Date.now() / 1000 }],
            };
        }

        case SAVE_ADDRESS_BOOK_ITEM: {
            return {
                ...state,
                addressBook: [
                    ...state.addressBook.filter(
                        (addressBookItem) => addressBookItem.address !== action.payload!.address,
                    ),
                    action.payload,
                ],
            };
        }

        case DELETE_ADDRESS_BOOK_ITEM: {
            return {
                ...state,
                addressBook: [
                    ...state.addressBook.filter(
                        (addressBookItem) => addressBookItem.address !== action.payload!.address,
                    ),
                ],
            };
        }

        case CLEAR_REDUCER_KEY: {
            return {
                ...state,
                [action.payload!.key]: initialState[action!.payload.key],
            };
        }

        case UPDATE_WALLET: {
            return {
                ...state,
                wallets: [
                    ...state.wallets.filter((wallet) => wallet.uuid !== action.payload!.uuid),
                    {
                        ...state.wallets.find((wallet: Wallet) => wallet.uuid === action.payload!.uuid),
                        ...action.payload,
                    },
                ],
            };
        }

        case DELETE_WALLET: {
            return {
                ...state,
                wallets: state.wallets.filter((wallet) => wallet.uuid !== action.payload!.uuid),
            };
        }

        case UPDATE_WALLETS: {
            return {
                ...state,
                wallets: action.payload,
            };
        }

        case SET_MIGRATED: {
            return {
                ...state,
                isMigrated: action.payload!.isMigrated,
            };
        }

        case UNSET_NETWORK: {
            return {
                ...state,
                network: undefined,
            };
        }

        case SET_NETWORK: {
            return {
                ...state,
                network: action.payload,
            };
        }

        case SAVE_NETWORK: {
            return {
                ...state,
                networks: [
                    ...state.networks.filter((network) => network.host !== action.payload!.host),
                    action.payload,
                ],
                network: state.network
                    ? state.network.host === action.payload!.host
                        ? action.payload
                        : state.network
                    : undefined,
            };
        }

        case DELETE_NETWORK: {
            return {
                ...state,
                networks: [...state.networks.filter((network) => network.host !== action.payload!.host)],
                network: state.network
                    ? state.network.host === action.payload!.host
                        ? undefined
                        : state.network
                    : undefined,
            };
        }

        case RESET: {
            return { ...initialState, isMigrated: state.isMigrated };
        }

        default: {
            return state;
        }
    }
};

export default appReducer;
