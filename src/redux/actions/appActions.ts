import { Address, AddressBook, Network, Token, Wallet } from '../../types';

export const SET_PASSWORD = 'SET_PASSWORD';
export const SET_LEGAL = 'SET_LEGAL';
export const SET_TOKENS = 'SET_TOKENS';
export const SET_NEW_WALLET = 'SET_NEW_WALLET';
export const SAVE_ADDRESS_BOOK_ITEM = 'SAVE_ADDRESS_BOOK_ITEM';
export const DELETE_ADDRESS_BOOK_ITEM = 'DELETE_ADDRESS_BOOK_ITEM';
export const CLEAR_REDUCER_KEY = 'CLEAR_REDUCER_KEY';
export const UPDATE_WALLET = 'UPDATE_WALLET';
export const DELETE_WALLET = 'DELETE_WALLET';
export const UPDATE_WALLETS = 'UPDATE_WALLETS';
export const SET_MIGRATED = 'SET_MIGRATED';
export const RESET = 'RESET';
export const SET_NAME = 'SET_NAME';
export const SET_NETWORK = 'SET_NETWORK';
export const UNSET_NETWORK = 'UNSET_NETWORK';
export const SAVE_NETWORK = 'SAVE_NETWORK';
export const DELETE_NETWORK = 'DELETE_NETWORK';

export const setPassword = (payload: { type?: 'pin' | 'password'; value?: string; useBiometric?: boolean }) => ({
    type: SET_PASSWORD,
    payload,
});

export const setLegal = (payload: { isLegalAgreed: boolean }) => ({
    type: SET_LEGAL,
    payload,
});

export const setTokens = (payload: Token[]) => ({
    type: SET_TOKENS,
    payload,
});

export const changeName = (payload: { name: string }) => ({
    type: SET_NAME,
    payload,
});

export const setNewWallet = (payload: {
    title?: string;
    seedPhrase?: string;
    addresses?: Address[];
    depositAddress?: string;
}) => ({
    type: SET_NEW_WALLET,
    payload,
});

export const saveAddressBookItem = (payload: AddressBook) => ({
    type: SAVE_ADDRESS_BOOK_ITEM,
    payload,
});

export const deleteAddressBookItem = (payload: { address: string }) => ({
    type: DELETE_ADDRESS_BOOK_ITEM,
    payload,
});

export const clearReducerKey = (payload: {
    key:
        | 'isLegalAgreed'
        | 'isRecoveryTipsShowed'
        | 'network'
        | 'password'
        | 'exchangeRate'
        | 'networks'
        | 'wallets'
        | 'addressBook';
}) => ({
    type: CLEAR_REDUCER_KEY,
    payload,
});

export const updateWallet = (payload: Wallet) => ({
    type: UPDATE_WALLET,
    payload,
});

export const deleteWallet = (payload: { uuid: string }) => ({
    type: DELETE_WALLET,
    payload,
});

export const updateWallets = (payload: Wallet[]) => ({
    type: UPDATE_WALLETS,
    payload,
});

export const setMigrated = (payload: { isMigrated: boolean }) => ({
    type: SET_MIGRATED,
    payload,
});

export const reset = () => ({
    type: RESET,
});

export const setNetwork = (payload: Network) => ({
    type: SET_NETWORK,
    payload,
});

export const unsetNetwork = () => ({
    type: UNSET_NETWORK,
});

export const saveNetwork = (payload: Network) => ({
    type: SAVE_NETWORK,
    payload,
});

export const deleteNetwork = (payload: { host: string }) => ({
    type: DELETE_NETWORK,
    payload,
});
