import React from 'react';
import { StyleSheet, Text, TouchableOpacity, TouchableOpacityProps, View } from 'react-native';

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 4,
    },
    leftContentContainer: {
        marginRight: 5,
    },
    lgContainer: {
        minHeight: 45,
        padding: 10,
    },
    mdContainer: {
        minHeight: 30,
        paddingVertical: 5,
        paddingHorizontal: 10,
    },
    text: {
        color: 'black',
    },
    lgText: {
        fontSize: 16,
    },
    mdText: {
        fontSize: 12,
    },
    outlinedContainer: {
        borderColor: '#ECECEC',
        borderWidth: 1,
    },
    containedContainer: {
        backgroundColor: '#614384',
    },
    outlinedText: {
        color: 'black',
    },
    containedText: {
        color: 'white',
    },
});

interface ButtonProps extends TouchableOpacityProps {
    style?: Record<string, any>;
    textStyle?: Record<string, any>;
    title: string;
    border?: boolean;
    backgroundColor?: string;
    borderColor?: string;
    color?: string;
    size?: 'lg' | 'md';
    leftContent?: React.ReactNode;
    flex?: 0 | 1;
    mb?: number;
    mt?: number;
    ml?: number;
    mr?: number;
    mx?: number;
    my?: number;
}

const Button: React.FC<ButtonProps> = ({
    style,
    leftContent,
    textStyle,
    title,
    border,
    disabled,
    backgroundColor,
    borderColor,
    color,
    flex,
    size,
    mb,
    mt,
    ml,
    mr,
    mx,
    my,
    ...props
}: ButtonProps) => {
    return (
        <TouchableOpacity
            disabled={disabled}
            style={[
                styles.container,
                size === 'lg' ? styles.lgContainer : styles.mdContainer,
                style,
                {
                    borderWidth: border ? 1 : 0,
                    opacity: disabled ? 0.5 : 1,
                    backgroundColor,
                    borderColor,
                    marginBottom: mb,
                    marginTop: mt,
                    marginLeft: ml,
                    marginRight: mr,
                    marginHorizontal: mx,
                    marginVertical: my,
                    flex,
                },
            ]}
            {...props}
        >
            {leftContent && <View style={styles.leftContentContainer}>{leftContent}</View>}
            <Text style={[styles.text, size === 'lg' ? styles.lgText : styles.mdText, textStyle, { color }]}>
                {title}
            </Text>
        </TouchableOpacity>
    );
};

Button.defaultProps = {
    size: 'lg',
    backgroundColor: '#614384',
    borderColor: '#ECECEC',
    color: 'white',
    flex: 0,
};

export default Button;
