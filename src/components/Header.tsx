import React from "react"
import { StyleSheet, Text, SafeAreaView, Image, View, TouchableOpacity } from "react-native";
import { HStack } from "./";
import EntypoIcon from 'react-native-vector-icons/Entypo';

interface HeaderProps {
    barStyle?: "light" | "dark",
    scene?: any,
    previous?: any,
    navigation?: any
}

const Header: React.FC<HeaderProps> = ({barStyle, iconColor, backgroundColor, scene, previous, navigation}) => {
    const { options } = scene.descriptor;
    const title =
        options.headerTitle !== undefined
        ? options.headerTitle
        : options.title !== undefined
        ? options.title
        : scene.route.name;

    return (
        <View style={[styles.container, {backgroundColor: backgroundColor ? backgroundColor : barStyle === "light" ? "transparent" : "white"}, options.headerStyle]}>
            <HStack flex={1} justifyContent="flex-start">
                {options.headerLeft ? options.headerLeft() : previous && <TouchableOpacity onPress={navigation.goBack} style={styles.leftButton}>
                    <EntypoIcon name="chevron-thin-left" size={28} color={iconColor ? iconColor : barStyle === "light" ? "white" : "#614384"} />
                </TouchableOpacity>}

                <Text style={[styles.title, {color: barStyle === "light" ? "white" : "#614384"}]}>{title}</Text>


            </HStack>
            {options.headerRight && options.headerRight()}
        </View>
    );
};

Header.defaultProps = {
    barStyle: "dark"
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        paddingTop: 30,
        flexDirection: "row",
        alignItems: "center",
        flexWrap: "nowrap"
    },
    title: {
        fontSize: 18,
        fontWeight: "bold"
    },
    rightButton: {
    },
    leftButton: {
        marginRight: 10,
        color: '#614384'
    }
});

export default Header;
