import React from 'react';
import { StyleSheet, Text, View, ViewProps } from 'react-native';

const styles = StyleSheet.create({
    container: {
        borderRadius: 25,
        justifyContent: 'center',
        alignItems: 'center',
    },
    mdSizeContainer: {
        width: 50,
        height: 50,
    },
    smSizeContainer: {
        width: 35,
        height: 35,
    },
    titleText: {
        textTransform: 'uppercase',
    },
    mdSizeText: {
        fontSize: 21,
    },
    smSizeText: {
        fontSize: 18,
    },
});

interface AvatarProps extends ViewProps {
    backgroundColor?: string;
    color?: string;
    title: string;
    source?: unknown;
    size?: 'md' | 'sm';
}

const Avatar: React.FC<AvatarProps> = ({
    style,
    size,
    backgroundColor,
    color,
    source,
    title,
    ...props
}: AvatarProps) => (
    <View
        style={[
            styles.container,
            size === 'md' ? styles.mdSizeContainer : styles.smSizeContainer,
            style,
            { backgroundColor },
        ]}
        {...props}
    >
        <Text style={[styles.titleText, size === 'md' ? styles.mdSizeText : styles.smSizeText, { color }]}>
            {title[0]}
        </Text>
    </View>
);

Avatar.defaultProps = {
    backgroundColor: 'white',
    color: 'black',
    size: 'md',
};

export default Avatar;
