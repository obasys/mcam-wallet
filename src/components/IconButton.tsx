import React from 'react';
import { StyleSheet, Text, TouchableOpacity, TouchableOpacityProps, View } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconContainer: {
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    backgroundContainer: {
        width: 40,
        height: 40,
        borderRadius: 20,
    },
    text: {
        fontSize: 12,
        marginTop: 5,
        color: 'black',
    },
    outlinedContainer: {
        borderColor: '#ECECEC',
        borderWidth: 1,
    },
    containedContainer: {
        backgroundColor: '#614384',
    },
    outlinedText: {
        color: 'black',
    },
    containedText: {
        color: 'white',
    },
});

interface IconButtonProps extends TouchableOpacityProps {
    style?: Record<string, any>;
    title?: string;
    children: React.ReactNode;
    border?: boolean;
    disableBackground?: boolean;
    backgroundColor?: string;
    borderColor?: string;
    flex?: 0 | 1;
    color?: string;
    mb?: number;
    mt?: number;
    ml?: number;
    mr?: number;
    mx?: number;
    my?: number;
}

const IconButton: React.FC<IconButtonProps> = ({
    style,
    title,
    border,
    disableBackground,
    backgroundColor,
    borderColor,
    color,
    flex,
    children,
    mb,
    mt,
    ml,
    mr,
    mx,
    my,
    ...props
}: IconButtonProps) => {
    return (
        <TouchableOpacity
            style={[
                styles.container,
                {
                    marginBottom: mb,
                    marginTop: mt,
                    marginLeft: ml,
                    marginRight: mr,
                    marginHorizontal: mx,
                    marginVertical: my,
                    flex,
                },
            ]}
            {...props}
        >
            <View
                style={[
                    styles.iconContainer,
                    !disableBackground && styles.backgroundContainer,
                    style,
                    {
                        borderWidth: border ? 1 : 0,
                        backgroundColor: !disableBackground ? backgroundColor : 'transparent',
                        borderColor,
                    },
                ]}
            >
                {children}
            </View>
            {title && <Text style={[styles.text, { color }]}>{title}</Text>}
        </TouchableOpacity>
    );
};

IconButton.defaultProps = {
    backgroundColor: '#614384',
    borderColor: '#ECECEC',
    color: 'white',
    flex: 0,
};

export default IconButton;
