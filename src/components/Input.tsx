import React from 'react';
import { StyleSheet, Text, TextInput, TextInputProps, TouchableWithoutFeedback, View } from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

const styles = StyleSheet.create({
    container: {
        width: '100%',
        minHeight: 40,
        borderBottomWidth: 1,
        borderColor: '#eee',
        color: 'black',
        flexDirection: 'row',
        alignItems: 'center',
    },
    inputContainer: {
        flex: 1,
        fontSize: 16,
    },
});

interface InputProps extends TextInputProps {
    rightContent?: React.ReactNode | React.ReactNode[];
    inputStyle?: Record<string, unknown>;
    flex?: 1 | 0;
    type?: 'text' | 'date';
    onPress?: () => void;
    // onCancel: any;
    // onConfirm: any;
    // onChange: any;
}

const Input: React.FC<InputProps> = ({
    style,
    inputStyle,
    value,
    placeholder,
    onPress,
    flex,
    rightContent,
    type,
    placeholderTextColor,
    ...props
}: InputProps) => {
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <View style={[styles.container, style, { flex }]}>
                {type === 'text' && (
                    <TextInput
                        style={[styles.inputContainer, inputStyle]}
                        value={value}
                        placeholder={placeholder}
                        placeholderTextColor={placeholderTextColor || '#614384'}
                        {...props}
                    />
                )}
                {type === 'date' && <DateTimePickerModal {...props} />}
                {type === 'date' && value !== undefined && value !== '' && (
                    <Text style={[styles.inputContainer, { color: 'black' }]}>{value}</Text>
                )}
                {type === 'date' && !value && (
                    <Text style={[styles.inputContainer, { color: placeholderTextColor || 'lightgray' }]}>
                        {placeholder}
                    </Text>
                )}
                {rightContent}
            </View>
        </TouchableWithoutFeedback>
    );
};

Input.defaultProps = {
    type: 'text',
};

export default Input;
