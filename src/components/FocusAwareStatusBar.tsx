import * as React from 'react';
import { StatusBar, StatusBarProps } from 'react-native';
import { useIsFocused } from '@react-navigation/native';

interface FocusAwareStatusBarProps extends StatusBarProps {
    backgroundColor?: any;
}

const FocusAwareStatusBar: React.FC<StatusBarProps> = ({ backgroundColor, ...props }: FocusAwareStatusBarProps) => {
    const isFocused = useIsFocused();

    return isFocused ? <StatusBar backgroundColor={backgroundColor || '#232E63'} {...props} /> : null;
};

export default FocusAwareStatusBar;
