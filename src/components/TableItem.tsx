import React from "react";
import { StyleSheet, Text, View, TouchableHighlight, TouchableHighlightProps } from "react-native";
import EntypoIcon from 'react-native-vector-icons/Entypo';
import Ionicon from 'react-native-vector-icons/Ionicons';
import { HStack, VStack } from "./";

interface TableItemProps extends TouchableHighlightProps {
	icon?: React.ReactNode,
	title: string,
	subtitle?: string,
	bottomDivider?: boolean,
	backgroundColor?: string,
	color?: string,
	rightContent?: React.ReactNode,
	leftContent?: React.ReactNode
}

const TableItem: React.FC<TableItemProps> = ({style, icon, title, backgroundColor, color, subtitle, bottomDivider, rightContent, leftContent, ...props}) => {
    return (
    	<TouchableHighlight {...props}>
    		<View style={[styles.container, style, {borderBottomWidth: bottomDivider ? 1 : 0, backgroundColor}]}>
		    	{leftContent}
		    	<HStack flex={1} justifyContent="flex-start" style={{marginLeft: leftContent ? 5 : 0, marginRight: rightContent ? 5 : 0}}>
			    	{icon && <View style={styles.iconContainer}>
			    		{icon}
			    	</View>}
			    	<View>
				    	<Text style={[styles.titleText, {color}]}>
				    		{title}
				    	</Text>
				    	{subtitle && <Text style={[styles.subtitleText, {color}]}>
				    		{subtitle}
				    	</Text>}
			    	</View>
		    	</HStack>
		    	{rightContent}
	    	</View>
	    </TouchableHighlight>
    );
};

TableItem.defaultProps = {
	backgroundColor: "white",
	color: "black"
}

const styles = StyleSheet.create({
	container: {
		padding: 20,
		minHeight: 60,
		alignItems: "center",
		flexDirection: "row",
		backgroundColor: 'white',
		borderColor: "#EEEEEE"
	},
	iconContainer: {
		marginRight: 10
	},
	titleText: {
		fontSize: 18
	},
	subtitleText: {
		fontSize: 12,
		opacity: 0.5
	}
});

export default TableItem;
