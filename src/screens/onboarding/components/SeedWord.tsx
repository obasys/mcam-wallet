import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const styles = StyleSheet.create({
    container: {
        borderRadius: 2,
        borderColor: '#614384',
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 5,
        minHeight: 50,
    },
    numberText: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#614384',
        opacity: 0.5,
    },
    wordText: {
        flex: 1,
        textAlign: 'center',
        color: '#614384',
        fontSize: 14,
        fontWeight: 'bold',
    },
});

interface SeedWordProps {
    number: number;
    word?: string;
    style?: any;
}

const SeedWord: React.FC<SeedWordProps> = ({ number, word, style }: SeedWordProps) => {
    return (
        <View style={[styles.container, { opacity: word === '' || word === undefined ? 0.5 : 1 }, style]}>
            <Text style={styles.numberText}>{number}</Text>
            <Text style={styles.wordText} selectable numberOfLines={1}>
                {word}
            </Text>
        </View>
    );
};

export default SeedWord;
