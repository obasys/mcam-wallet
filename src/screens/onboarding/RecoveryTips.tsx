import React, { useRef, useState } from 'react';
import { Dimensions, Image, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { useTranslation } from 'react-i18next';
import { Button, FocusAwareStatusBar, HStack, VStack } from '../../components';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginBottom: 20,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    buttonText: {
        textDecorationLine: 'underline',
    },
    carouselItemContainer: {
        height: '100%',
    },
    carouselItemImage: {
        height: '70%',
    },
    imageContainer: {
        width: '100%',
    },
    carouselItemTitleContainer: {
        marginBottom: 15,
    },
    carouselItemTitleText: {
        fontSize: 21,
        fontWeight: 'bold',
        color: '#614384',
    },
    carouselItemSubtitleContainer: {
        marginBottom: 15,
    },
    carouselItemSubtitleText: {
        fontSize: 16,
        color: '#614384',
    },
    carouselItemDescriptionText: {
        fontSize: 14,
        opacity: 0.5,
        color: '#614384',
    },
    dotContainer: {
        width: 10,
        height: 10,
        borderRadius: 5,
        marginHorizontal: 8,
        backgroundColor: 'rgba(255, 255, 255, 0.92)',
    },
    dividerContainer: {
        padding: 20,
    },
    divider: {
        width: 50,
        height: 6,
        borderRadius: 10,
        backgroundColor: '#232E63',
    },
});

interface RecoveryTipsProps {
    navigation: any;
}

const RecoveryTips: React.FC<RecoveryTipsProps> = ({ navigation }: RecoveryTipsProps) => {
    const { t } = useTranslation('recoveryTips');
    const carousel = useRef<any>();
    const [activeSlide, setActiveSlide] = useState(0);

    const DATA = [
        {
            title: t('slides.first.title'),
            subtitle: t('slides.first.subtitle'),
            text: t('slides.first.description'),
            image: require('../../assets/recoverTip-test.png'),
        },
        // {
        //     title: t('slides.first.title'),
        //     subtitle: t('slides.first.subtitle'),
        //     text: t('slides.first.description'),
        //     image: require('../../assets/recoveryTip1.png'),
        // },
        // {
        //     title: t('slides.second.title'),
        //     subtitle: t('slides.second.subtitle'),
        //     text: t('slides.second.description'),
        //     image: require('../../assets/recoveryTip2.png'),
        // },
        // {
        //     title: t('slides.third.title'),
        //     subtitle: t('slides.third.subtitle'),
        //     text: t('slides.third.description'),
        //     image: require('../../assets/recoveryTip3.png'),
        // },
        // {
        //     title: t('slides.fourth.title'),
        //     subtitle: t('slides.fourth.subtitle'),
        //     text: t('slides.fourth.description'),
        //     image: require('../../assets/recoveryTip4.png'),
        // },
    ];

    const renderItem = ({ item, index }) => {
        return (
            <VStack style={[styles.alignment, styles.carouselItemContainer]} alignItems="flex-start">
                <View style={styles.carouselItemTitleContainer}>
                    <Text style={styles.carouselItemTitleText}>{item.subtitle}</Text>
                </View>
                {item.text !== '' && (
                    <View style={styles.carouselItemSubtitleContainer}>
                        <Text style={styles.carouselItemSubtitleText}>{item.text}</Text>
                    </View>
                )}
                <VStack flex={1} style={styles.imageContainer}>
                    <Image source={item.image} resizeMode="contain" style={styles.carouselItemImage} />
                </VStack>
            </VStack>
        );
    };

    return (
        <LinearGradient colors={['#FFF', '#E2DEE7']} useAngle angle={180} style={{ flex: 1 }}>
            <FocusAwareStatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.container}>
                <HStack style={styles.dividerContainer}>
                    <View style={styles.divider} />
                </HStack>
                <VStack flex={1}>
                    <Carousel
                        ref={(c) => {
                            carousel.current = c;
                        }}
                        inactiveSlideOpacity={0}
                        inactiveSlideScale={1}
                        hasParallaxImages
                        lockScrollWhileSnapping
                        data={DATA}
                        renderItem={renderItem}
                        sliderWidth={Dimensions.get('window').width}
                        itemWidth={Dimensions.get('window').width}
                        onSnapToItem={(index) => setActiveSlide(index)}
                    />
                    <Pagination
                        dotsLength={DATA.length}
                        activeDotIndex={activeSlide}
                        dotStyle={styles.dotContainer}
                        inactiveDotOpacity={0.4}
                        inactiveDotScale={0.6}
                    />
                </VStack>
                <HStack style={styles.alignment}>
                    <Button
                        title={activeSlide === DATA.length - 1 ? t('confirmButton.gotIt') : t('confirmButton.next')}
                        backgroundColor="#614384"
                        color="white"
                        flex={1}
                        mt={40}
                        mb={20}
                        onPress={() =>
                            activeSlide === DATA.length - 1 ? navigation.goBack() : carousel.current.snapToNext()
                        }
                    />
                </HStack>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default RecoveryTips;
