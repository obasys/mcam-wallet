import React, { useContext, useEffect, useState } from 'react';
import { ActivityIndicator, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import { showMessage } from 'react-native-flash-message';
import { CommonActions } from '@react-navigation/native';
import { v4 as uuidv4 } from 'uuid';
import { useTranslation } from 'react-i18next';
import Config from 'react-native-config';
import { encryptData } from '../../services/commonService';
import { generateAddresses } from '../../services/addressService';
import { OnboardingContext, PasswordContext, SocketContext } from '../../providers';
import { setNewWallet } from '../../redux/actions/appActions';
import { Address, RootState } from '../../types';
import { FocusAwareStatusBar, VStack } from '../../components';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginBottom: 20,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    infoContainer: {
        marginTop: 30,
    },
    titleContainer: {
        marginBottom: 15,
    },
    titleText: {
        color: '#614384',
        fontSize: 21,
        fontWeight: 'bold',
    },
    subtitleText: {
        color: '#614384',
        fontSize: 14,
        // opacity: 0.5,
    },
});

interface GenerateWalletProps {
    navigation: any;
}

const GenerateWallet: React.FC<GenerateWalletProps> = ({ navigation }: GenerateWalletProps) => {
    const { t } = useTranslation('generateWallet');
    const appReducer = useSelector((state: RootState) => state.app);
    const { onboarding, dispatchOnboardingAction } = useContext(OnboardingContext);
    const { socketRequest } = useContext(SocketContext);
    const { unlockedPassword } = useContext(PasswordContext);
    const [walletCount, setWalletCount] = useState<number>(appReducer.wallets.length);
    const dispatch = useDispatch();

    const [network, setNetwork] = useState({
        bip32: {
            public: 76066276,
            private: 76067358,
        },
        pubKeyHash: appReducer.network ? parseInt(appReducer.network.publicKeyHash) : parseInt(Config.PUB_KEY_HASH),
        scriptHash: appReducer.network ? parseInt(appReducer.network.scriptHash) : parseInt(Config.SCRIPT_HASH),
        wif: appReducer.network ? parseInt(appReducer.network.wif) : parseInt(Config.WIF),
        messagePrefix: '\x18Bitcoin Signed Message:\n',
        bech32: 'bc',
    });

    const findAddresses = async () => {
        let addressesWithHistory: Address[] = [];
        let synced = false;
        const searchRange = [0, 20];

        while (!synced) {
            const addresses = generateAddresses(
                onboarding?.wallet.seedPhrase!,
                searchRange[0],
                searchRange[1],
                0,
                network,
            );
            addressesWithHistory = [...addressesWithHistory, addresses[0]];

            const filteredAddresses = await socketRequest!('address.check', [
                addresses.map((address) => address.address),
            ]);

            if (filteredAddresses.length > 0) {
                addressesWithHistory = [
                    ...addressesWithHistory,
                    ...addresses.filter((address) => filteredAddresses.includes(address.address)),
                ];
                searchRange[0] += 20;
                searchRange[1] += 20;
            } else {
                synced = true;
            }
        }

        return [...new Set(addressesWithHistory)];
    };

    const createWallet = async () => {
        if (onboarding?.wallet.seedPhrase === undefined) {
            throw new Error(t('alerts.error.notFound.seedPhrase'));
        }

        if (onboarding?.wallet.title === undefined) {
            throw new Error(t('alerts.error.notFound.title'));
        }

        if (unlockedPassword === undefined) {
            throw new Error(t('alerts.error.notFound.unlockedPassword'));
        }

        try {
            let addresses =
                onboarding!.processType === 'create'
                    ? generateAddresses(onboarding?.wallet.seedPhrase!, 0, 1, 0, network)
                    : await findAddresses();

            addresses = addresses.map((address) => {
                return { ...address, wif: encryptData(address.wif, unlockedPassword) };
            });

            dispatchOnboardingAction({
                type: 'setWalletValues',
                wallet: {
                    uuid: uuidv4(),
                    seedPhrase: encryptData(onboarding?.wallet.seedPhrase!, unlockedPassword),
                    addresses,
                    depositAddress: addresses[0].address,
                },
            });
        } catch (e) {
            throw e;
        }
    };

    useEffect(() => {
        try {
            createWallet();
        } catch (e) {
            showMessage({
                message: 'Wallet',
                description: e,
                type: 'danger',
            });

            navigation.goBack();
        }
    }, []);

    useEffect(() => {
        if (
            onboarding?.wallet.seedPhrase !== undefined &&
            onboarding?.wallet.addresses !== undefined &&
            onboarding?.wallet.depositAddress !== undefined &&
            onboarding?.wallet.title !== undefined
        ) {
            dispatch(setNewWallet(onboarding.wallet));
            dispatchOnboardingAction({ type: 'resetWalletValues' });
        }
    }, [onboarding]);

    useEffect(() => {
        if (appReducer.wallets.length > walletCount) {
            navigation.dispatch(
                CommonActions.reset({
                    index: 0,
                    routes: [{ name: 'HomeStack', params: {} }],
                }),
            );
        }
    }, [appReducer.wallets]);

    return (
        <LinearGradient colors={['#FFF', '#E2DEE7']} useAngle angle={90} style={{ flex: 1 }}>
            <FocusAwareStatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.container}>
                <VStack flex={1} style={styles.alignment}>
                    <ActivityIndicator color="#614384" size="large" />
                    <VStack style={styles.infoContainer}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.titleText}>{t('title')}</Text>
                        </View>
                        <Text style={styles.subtitleText}>{t('description')}</Text>
                    </VStack>
                </VStack>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default GenerateWallet;
