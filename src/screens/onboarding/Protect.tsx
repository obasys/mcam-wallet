import React from 'react';
import { Image, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { useTranslation } from 'react-i18next';
import { Button, FocusAwareStatusBar, HStack, VStack } from '../../components';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 110,
        marginBottom: 20,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    itemContainer: {
        height: '100%',
    },
    itemImage: {
        height: '50%',
    },
    imageContainer: {
        width: '100%',
    },
    itemTitleContainer: {
        marginBottom: 15,
    },
    itemTitleText: {
        fontSize: 21,
        fontWeight: 'bold',
        color: '#614384',
    },
    itemDescriptionText: {
        fontSize: 14,
        opacity: 0.5,
        color: '#614384',
    },
});

interface ProtectProps {
    navigation: any;
    route: any;
}

const Protect: React.FC<ProtectProps> = ({ navigation, route }: ProtectProps) => {
    const { t } = useTranslation('protect');

    return (
        <LinearGradient colors={['#FFF', '#E2DEE7']} useAngle angle={90} style={{ flex: 1 }}>
            <FocusAwareStatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.container}>
                <VStack style={[styles.alignment, styles.itemContainer]} flex={1} alignItems="flex-start">
                    <View style={styles.itemTitleContainer}>
                        <Text style={styles.itemTitleText}>{t('title')}</Text>
                    </View>
                    <Text style={styles.itemDescriptionText}>{t('description')}</Text>
                    <VStack flex={1} style={styles.imageContainer}>
                        <Image
                            source={require('../../assets/protect.png')}
                            resizeMode="contain"
                            style={styles.itemImage}
                        />
                    </VStack>
                </VStack>
                <HStack style={styles.alignment}>
                    <Button
                        title={t('confirmButton')}
                        backgroundColor="#614384"
                        color="white"
                        flex={1}
                        mt={40}
                        mb={20}
                        onPress={() =>
                            navigation.replace('PasswordStack', {
                                screen: 'Password',
                                params: { type: 'new-password' },
                            })
                        }
                    />
                </HStack>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default Protect;
