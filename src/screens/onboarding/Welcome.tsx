import React, { useContext, useEffect, useState } from 'react';
import { Dimensions, Image, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { OnboardingContext } from '../../providers';
import { Button, FocusAwareStatusBar, VStack } from '../../components';
import { RootState } from '../../types';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 110,
        marginBottom: 20,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    buttonText: {
        textDecorationLine: 'underline',
    },
    carouselItemContainer: {
        height: '100%',
    },
    carouselItemImage: {
        height: '70%',
    },
    imageContainer: {
        width: '100%',
    },
    carouselItemTitleContainer: {
        marginBottom: 15,
    },
    carouselItemTitleText: {
        fontSize: 21,
        fontWeight: 'bold',
        color: '#614384',
    },
    carouselItemDescriptionText: {
        fontSize: 14,
        opacity: 0.5,
        color: '#614384',
    },
    dotContainer: {
        width: 10,
        height: 10,
        borderRadius: 5,
        marginHorizontal: 8,
        backgroundColor: 'rgba(255, 255, 255, 0.92)',
    },
    buttonContainer: {
        paddingHorizontal: 80,
        paddingVertical: 10
    },
});

interface WelcomeProps {
    navigation: any;
}

const Welcome: React.FC<WelcomeProps> = ({ navigation }: WelcomeProps) => {
    const { t } = useTranslation('welcome');
    const [activeSlide, setActiveSlide] = useState(0);
    const appReducer = useSelector((state: RootState) => state.app);
    const { onboarding, dispatchOnboardingAction } = useContext(OnboardingContext);

    const DATA = [

        {
            title: "Welcome",
            text: "MAC Coin is finance, real estate, distribution, culture, etc. It's a cryptocurrency specialized in real-life payment.",
            image: require('../../assets/intro-test.png'),
        },
        // {
        //     title: t('slides.first.title'),
        //     text: t('slides.first.description'),
        //     image: require('../../assets/intro1.png'),
        // },
        // {
        //     title: t('slides.second.title'),
        //     text: t('slides.second.description'),
        //     image: require('../../assets/intro2.png'),
        // },
        // {
        //     title: t('slides.third.title'),
        //     text: t('slides.third.description'),
        //     image: require('../../assets/intro3.png'),
        // },
    ];

    const renderItem = ({ item, index }: any) => {
        return (
            <VStack style={[styles.alignment, styles.carouselItemContainer]} alignItems="flex-start">
                <View style={styles.carouselItemTitleContainer}>
                    <Text style={styles.carouselItemTitleText}>{item.title}</Text>
                </View>
                <Text style={styles.carouselItemDescriptionText}>{item.text}</Text>
                <VStack flex={1} style={styles.imageContainer}>
                    <Image source={item.image} resizeMode="contain" style={styles.carouselItemImage} />
                </VStack>
            </VStack>
        );
    };

    const handleCreateWallet = () => {
        dispatchOnboardingAction({ type: 'setProcessType', processType: 'create' });

        if (!appReducer.password.value) {
            navigation.navigate('Protect');
        } else {
            navigation.navigate('RecoveryPhrase');
        }
    };

    const handleImportWallet = () => {
        dispatchOnboardingAction({ type: 'setProcessType', processType: 'import' });

        if (!appReducer.password.value) {
            navigation.navigate('Protect');
        } else {
            navigation.navigate('RecoveryPhrase');
        }
    };

    useEffect(() => {
        if (appReducer.wallets.length === 0) {
            navigation.setOptions({
                headerLeft: () => null,
            });
        }
    }, []);

    return (
        <LinearGradient colors={['#FFF', '#E2DEE7']} useAngle angle={180} style={{ flex: 1 }}>
            <FocusAwareStatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.container}>
                <VStack flex={1}>
                    <Carousel
                        loop
                        autoplay
                        autoplayInterval={3000}
                        autoplayDelay={1000}
                        inactiveSlideOpacity={0}
                        inactiveSlideScale={1}
                        hasParallaxImages
                        lockScrollWhileSnapping
                        data={DATA}
                        renderItem={renderItem}
                        sliderWidth={Dimensions.get('window').width}
                        itemWidth={Dimensions.get('window').width}
                        onSnapToItem={(index) => setActiveSlide(index)}
                    />
                    <Pagination
                        dotsLength={DATA.length}
                        activeDotIndex={activeSlide}
                        dotStyle={styles.dotContainer}
                        inactiveDotOpacity={0.4}
                        inactiveDotScale={0.6}
                    />
                </VStack>
                <VStack justifyContent="flex-end" alignItems="center" style={styles.alignment}>
                    <Button
                        title={t('create')}
                        style={styles.buttonContainer}
                        onPress={handleCreateWallet}
                    />
                    <Button
                        title={t('import')}
                        textStyle={styles.buttonText}
                        color="#614384"
                        backgroundColor="transparent"
                        mb={20}
                        onPress={handleImportWallet}
                    />
                </VStack>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default Welcome;
