import React, { useContext, useState } from 'react';
import { Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { useTranslation } from 'react-i18next';
import IoniconsIcon from 'react-native-vector-icons/Ionicons';
import { showMessage } from 'react-native-flash-message';
import { OnboardingContext } from '../../providers';
import { Button, FocusAwareStatusBar, HStack, VStack } from '../../components';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 110,
        marginBottom: 20,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    agreementButton: {
        marginTop: 30,
    },
    agreementContainer: {
        marginTop: 10,
    },
    agreementText: {
        fontSize: 14,
        color: '#614384',
    },
    itemContainer: {
        height: '100%',
    },
    itemImage: {
        height: '70%',
    },
    imageContainer: {
        width: '100%',
    },
    titleContainer: {
        marginBottom: 15,
    },
    titleText: {
        fontSize: 21,
        fontWeight: 'bold',
        color: '#614384',
    },
    descriptionText: {
        fontSize: 14,
        color: '#614384',
    },
});

interface FinishedProps {
    navigation: any;
}

const Finished: React.FC<FinishedProps> = ({ navigation }: FinishedProps) => {
    const { t } = useTranslation('finished');
    const [recoveryAgreement, setRecoveryAgreement] = useState(false);
    const { onboarding, dispatchOnboardingAction } = useContext(OnboardingContext);

    const handleContinue = () => {
        if (recoveryAgreement) {
            navigation.navigate('GenerateWallet');
        } else {
            showMessage({
                message: t('alerts.errorAgreement.message'),
                description: t('alerts.errorAgreement.description'),
                type: 'danger',
            });
        }
    };

    return (
        <LinearGradient colors={['#FFF', '#E2DEE7']} useAngle angle={180} style={{ flex: 1 }}>
            <FocusAwareStatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.container}>
                <VStack style={[styles.alignment, styles.itemContainer]} flex={1} alignItems="flex-start">
                    <View style={styles.titleContainer}>
                        <Text style={styles.titleText}>{t('title')}</Text>
                    </View>
                    <Text style={styles.descriptionText}>{t('description')}</Text>
                    <VStack flex={1} style={styles.imageContainer}>
                        <Image
                            source={require('../../assets/confirmation-test.png')}
                            resizeMode="contain"
                            style={styles.itemImage}
                        />
                    </VStack>
                    <TouchableOpacity
                        style={styles.agreementButton}
                        onPress={() => setRecoveryAgreement(!recoveryAgreement)}
                    >
                        <IoniconsIcon
                            name={recoveryAgreement ? 'checkmark-circle' : 'ellipse-outline'}
                            size={28}
                            color="#614384"
                        />
                        <View style={styles.agreementContainer}>
                            <Text style={styles.agreementText}>{t('agreement')}</Text>
                        </View>
                    </TouchableOpacity>
                </VStack>
                <HStack style={styles.alignment}>
                    <Button
                        title={t('confirmButton')}
                        backgroundColor="#614384"
                        color="white"
                        flex={1}
                        mb={20}
                        mt={40}
                        onPress={handleContinue}
                    />
                </HStack>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default Finished;
