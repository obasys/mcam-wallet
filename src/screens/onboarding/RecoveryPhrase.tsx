import React, { useContext, useEffect, useState } from 'react';
import { KeyboardAvoidingView, Platform, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import { showMessage } from 'react-native-flash-message';
import { useTranslation } from 'react-i18next';
import { Button, DismissKeyboard, FocusAwareStatusBar, HStack, Input, VStack } from '../../components';
import { SeedWord } from './components';
import { OnboardingContext } from '../../providers';
import { generateSeedPhrase } from '../../services/addressService';
import { RootState } from '../../types';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 110,
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 20,
        height: 90,
    },
    titleText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#e2dee7',
    },
    backButton: {
        marginRight: 10,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    seedPhraseContainer: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 30,
    },
    seedWordContainer: {
        marginBottom: 15,
        width: '30%',
    },
    importInput: {
        color: '#614384',
    },
    caption: {
        fontSize: 12,
        color: '#b0a1c1',
    },
    inputContainer: {
        width: '100%',
        marginBottom: 30,
    },
});

interface RecoveryPhraseProps {
    navigation: any;
    route: any;
}

const RecoveryPhrase: React.FC<RecoveryPhraseProps> = ({ navigation, route }: RecoveryPhraseProps) => {
    const { t } = useTranslation('recoveryPhrase');
    const appReducer = useSelector((state: RootState) => state.app);
    const [seedPhrase, setSeedPhrase] = useState<string[]>(['', '', '', '', '', '', '', '', '', '', '', '']);
    const [title, setTitle] = useState<string>(`${t('initialTitle')} #${appReducer.wallets.length + 1}`);
    const [seedPhraseText, setSeedPhraseText] = useState<string>('');
    const { onboarding, dispatchOnboardingAction } = useContext(OnboardingContext);
    const dispatch = useDispatch();

    const handleContinue = () => {
        if (seedPhrase.length === 12) {
            if (onboarding!.processType === 'create') {
                dispatchOnboardingAction({
                    type: 'setWalletValues',
                    wallet: { seedPhrase: seedPhrase.join(' '), title },
                });
                navigation.navigate('Finished');
            } else if (onboarding!.processType === 'import') {
                if (seedPhraseText.split(' ').length === 12) {
                    dispatchOnboardingAction({
                        type: 'setWalletValues',
                        wallet: { seedPhrase: seedPhrase.join(' '), title },
                    });
                    navigation.navigate('Finished');
                } else {
                    showMessage({
                        message: t('alerts.fillRecoveryPhrase.message'),
                        description: t('alerts.fillRecoveryPhrase.description'),
                        type: 'danger',
                    });
                }
            }
        }
    };

    useEffect(() => {
        navigation.navigate('RecoveryTips');

        if (onboarding!.processType === 'create') {
            setSeedPhrase(generateSeedPhrase());
        }
    }, []);

    useEffect(() => {
        if (onboarding!.processType === 'import') {
            const splited = seedPhraseText.split(' ');
            if (splited.length <= 12) {
                setSeedPhrase([...splited, ...new Array(12 - splited.length)]);
            }
        }
    }, [seedPhraseText]);

    return (
        <DismissKeyboard>
            <LinearGradient colors={['#FFF', '#E2DEE7']} useAngle angle={180} style={{ flex: 1 }}>
                <SafeAreaView style={styles.container}>
                    <FocusAwareStatusBar barStyle="dark-content" />
                    <KeyboardAvoidingView
                        style={{ flex: 1 }}
                        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                        keyboardVerticalOffset={100}
                    >
                        <ScrollView>
                            <VStack
                                justifyContent="flex-start"
                                alignItems="flex-start"
                                flex={1}
                                style={styles.alignment}
                            >
                                <View style={[styles.seedPhraseContainer]}>
                                    {seedPhrase.map((seedWord, index) => {
                                        return (
                                            <SeedWord
                                                number={index + 1}
                                                word={seedWord}
                                                style={styles.seedWordContainer}
                                                key={index}
                                            />
                                        );
                                    })}
                                </View>
                                <VStack flex={1} justifyContent="flex-start">
                                    {onboarding!.processType === 'import' && (
                                        <View style={styles.inputContainer}>
                                            <Text style={styles.caption}>{t('recoveryPhrase.title')}</Text>
                                            <Input
                                                inputStyle={styles.importInput}
                                                placeholder={t('recoveryPhrase.placeholder')}
                                                onChangeText={setSeedPhraseText}
                                                value={seedPhraseText}
                                            />
                                        </View>
                                    )}
                                    <View style={styles.inputContainer}>
                                        <Text style={styles.caption}>Wallet title</Text>
                                        <Input
                                            inputStyle={styles.importInput}
                                            placeholder={t('walletTitle.placeholder')}
                                            onChangeText={setTitle}
                                            value={title}
                                        />
                                    </View>
                                </VStack>
                            </VStack>
                        </ScrollView>
                        <HStack style={styles.alignment}>
                            <Button
                                title={t('confirmButton')}
                                backgroundColor="#614384"
                                color="white"
                                flex={1}
                                onPress={handleContinue}
                                mb={20}
                            />
                        </HStack>
                    </KeyboardAvoidingView>
                </SafeAreaView>
            </LinearGradient>
        </DismissKeyboard>
    );
};

export default RecoveryPhrase;
