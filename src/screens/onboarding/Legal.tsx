import React, { useState } from 'react';
import { Linking, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import IoniconsIcon from 'react-native-vector-icons/Ionicons';
import { showMessage } from 'react-native-flash-message';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import Config from 'react-native-config';
import { Button, FocusAwareStatusBar, HStack, TableItem, VStack } from '../../components';
import { setLegal } from '../../redux/actions/appActions';
import { RootState } from '../../types';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 110,
        marginBottom: 20,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    contentContainer: {
        width: '100%',
    },
    descriptionText: {
        color: '#614384',
        opacity: 0.5,
    },
    descriptionContainer: {
        marginBottom: 15,
    },
    agreementContainer: {
        marginTop: 10,
    },
    agreementText: {
        fontSize: 14,
        color: '#614384',
    },
});

interface LegalProps {
    navigation: any;
    route: any;
}

const Legal: React.FC<LegalProps> = ({ navigation, route }: LegalProps) => {
    const { t } = useTranslation('legal');
    const [legalAgreement, setLegalAgreement] = useState(false);
    const appReducer = useSelector((state: RootState) => state.app);
    const dispatch = useDispatch();
    const { processType } = route.params ?? {};

    const handleContinue = () => {
        if (legalAgreement) {
            dispatch(setLegal({ isLegalAgreed: legalAgreement }));

            if (!appReducer.password.value) {
                navigation.navigate('Protect');
            } else {
                navigation.navigate('RecoveryPhrase');
            }
        } else {
            showMessage({
                message: t('alerts.errorAgreement.message'),
                description: t('alerts.errorAgreement.description'),
                type: 'danger',
            });
        }
    };

    return (
        <LinearGradient colors={['#FFF', '#E2DEE7']} useAngle angle={180} style={{ flex: 1 }}>
            <FocusAwareStatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.container}>
                <VStack justifyContent="space-between" alignItems="flex-start" flex={1}>
                    <View style={styles.contentContainer}>
                        <View style={[styles.alignment, styles.descriptionContainer]}>
                            <Text style={styles.descriptionText}>{t('description')}</Text>
                        </View>
                        <TableItem
                            title={t('termsOfService')}
                            backgroundColor="transparent"
                            // onPress={() => null}
                            underlayColor="#614384"
                            color="#614384"
                            onPress={() => Linking.openURL(Config.TERMS_OF_SERVICE)}
                            rightContent={<EntypoIcon name="chevron-thin-right" size={20} color="#614384" />}
                        />
                        <TableItem
                            title={t('privacyPolicy')}
                            backgroundColor="transparent"
                            // onPress={() => null}
                            underlayColor="#614384"
                            color="#614384"
                            onPress={() => Linking.openURL(Config.PRIVACY_POLICY)}
                            rightContent={<EntypoIcon name="chevron-thin-right" size={20} color="#614384" />}
                        />
                    </View>
                    <TouchableOpacity onPress={() => setLegalAgreement(!legalAgreement)}>
                        <View style={styles.alignment}>
                            <IoniconsIcon
                                name={legalAgreement ? 'checkmark-circle' : 'ellipse-outline'}
                                size={28}
                                color="#614384"
                            />
                            <View style={styles.agreementContainer}>
                                <Text style={styles.agreementText}>{t('agreement')}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </VStack>
                <HStack style={styles.alignment}>
                    <Button
                        title={t('confirmButton')}
                        backgroundColor="#614384"
                        color="white"
                        flex={1}
                        mt={40}
                        mb={20}
                        onPress={handleContinue}
                    />
                </HStack>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default Legal;
