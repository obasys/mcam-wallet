import React, { useContext } from 'react';
import { Dimensions, StyleSheet, View } from 'react-native';
import QRCodeScannerComponent from 'react-native-qrcode-scanner';
import { RNCamera as Camera } from 'react-native-camera';
import Config from 'react-native-config';
import { useTranslation } from 'react-i18next';
import { ActionSheet } from 'react-native-cross-actionsheet';
import { isAddress } from '../../services/addressService';
import { WalletContext } from '../../providers';
import { FocusAwareStatusBar, HStack } from '../../components';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'black',
        flex: 1,
    },
    cornersContainer: {
        height: 400,
        width: '100%',
        padding: 20,
        justifyContent: 'space-between',
    },
    cornerContainer: {
        width: 80,
        height: 80,
        borderColor: 'white',
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 40,
        paddingHorizontal: 20,
        height: 110,
        position: 'absolute',
        zIndex: 1,
    },
    titleText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'white',
    },
    backButton: {
        marginRight: 10,
    },
});

interface QRCodeScannerProps {
    navigation: any;
    route: any;
}

const QRCodeScanner: React.FC<QRCodeScannerProps> = ({ navigation, route }: QRCodeScannerProps) => {
    const { t } = useTranslation('qrCodeScanner');
    const { type, callback } = route.params ?? {};
    const { uuid } = useContext(WalletContext);

    const processAddressBook = (address: string, actionType: 'replace' | 'navigate' = 'navigate') => {
        if (actionType === 'navigate') {
            navigation.navigate('ManageAddressBookItem', { address });
        } else {
            navigation.replace('AddressBookStack', { screen: 'ManageAddressBookItem', params: { address } });
        }
    };

    const processWithdraw = (address: string, actionType: 'replace' | 'navigate' = 'navigate') => {
        if (actionType === 'navigate') {
            navigation.navigate('Withdraw', { address });
        } else {
            navigation.replace('WalletStack', { screen: 'Withdraw', params: { address } });
        }
    };

    const showActionSheet = (address: string) => {
        ActionSheet.options({
            options: [
                { text: t('addToAddressBook'), onPress: () => processAddressBook(address, 'replace') },
                { text: t('withdraw'), onPress: () => processWithdraw(address, 'replace') },
            ],
            cancel: { onPress: () => navigation.goBack() },
        });
    };

    const processData = (address: string) => {
        switch (type) {
            case 'address-book':
                processAddressBook(address);
                break;
            case 'withdraw':
                processWithdraw(address);
                break;
            default:
                if (uuid) {
                    showActionSheet(address);
                } else {
                    processAddressBook(address, 'replace');
                }

                break;
        }
    };

    const onSuccess = (e: any) => {
        const data = e.data.replace(/ /g, '');

        if (data.startsWith(`${Config.COIN_NAME.toLowerCase()}:`)) {
            const splited = data.split(':');

            if (isAddress(splited[1])) {
                processData(splited[1]);
            }
        } else if (isAddress(data)) {
            processData(data);
        }
    };

    return (
        <View style={styles.container}>
            <FocusAwareStatusBar barStyle="light-content" />
            <QRCodeScannerComponent
                showMarker
                onRead={onSuccess}
                reactivateTimeout={2000}
                customMarker={
                    <View style={styles.cornersContainer}>
                        <HStack justifyContent="space-between">
                            <View
                                style={[
                                    styles.cornerContainer,
                                    { borderTopLeftRadius: 20, borderLeftWidth: 4, borderTopWidth: 4 },
                                ]}
                            />
                            <View
                                style={[
                                    styles.cornerContainer,
                                    {
                                        borderTopRightRadius: 20,
                                        borderRightWidth: 4,
                                        borderTopWidth: 4,
                                    },
                                ]}
                            />
                        </HStack>
                        <HStack justifyContent="space-between">
                            <View
                                style={[
                                    styles.cornerContainer,
                                    {
                                        borderBottomLeftRadius: 20,
                                        borderLeftWidth: 4,
                                        borderBottomWidth: 4,
                                    },
                                ]}
                            />
                            <View
                                style={[
                                    styles.cornerContainer,
                                    {
                                        borderBottomRightRadius: 20,
                                        borderRightWidth: 4,
                                        borderBottomWidth: 4,
                                    },
                                ]}
                            />
                        </HStack>
                    </View>
                }
                cameraStyle={{ height: Dimensions.get('window').height }}
                topViewStyle={{ flex: 0 }}
                bottomViewStyle={{ flex: 0 }}
                cameraProps={{ flashMode: Camera.Constants.FlashMode.off }}
            />
        </View>
    );
};

export default QRCodeScanner;
