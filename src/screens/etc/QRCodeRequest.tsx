import React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import QRCode from 'react-native-qrcode-svg';
import { useTranslation } from 'react-i18next';
import Config from 'react-native-config';
import moment from 'moment';
import { FocusAwareStatusBar, HStack, VStack } from '../../components';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    dividerContainer: {
        padding: 20,
    },
    divider: {
        width: 50,
        height: 6,
        borderRadius: 10,
        backgroundColor: '#232E63',
    },
    titleContainer: {
        marginBottom: 15,
    },
    titleText: {
        fontSize: 18,
        color: 'white',
        fontWeight: 'bold',
    },
    descriptionText: {
        fontSize: 14,
        color: 'white',
        opacity: 0.5,
    },
    alignment: {
        paddingHorizontal: 20,
        width: '100%',
    },
    inputContainer: {
        marginBottom: 15,
        width: '100%',
    },
    caption: {
        fontSize: 12,
        color: '#232E63',
    },
    content: {
        marginTop: 10,
    },
    contentText: {
        fontSize: 16,
    },
});

interface QRCodeRequestProps {
    navigation: any;
    route: any;
}

const QRCodeRequest: React.FC<QRCodeRequestProps> = ({ navigation, route }: QRCodeRequestProps) => {
    const { t } = useTranslation('qrCodeRequest');
    const params = route.params ?? { address: '', amount: '', timelock: undefined, token: undefined };

    return (
        <LinearGradient colors={['#FFF', '#E2DEE7']} useAngle angle={180} style={{ flex: 1 }}>
            <FocusAwareStatusBar barStyle="light-content" />
            <View style={[styles.alignment, { paddingBottom: 20 }]}>
                <HStack style={styles.dividerContainer}>
                    <View style={styles.divider} />
                </HStack>
                <View style={styles.titleContainer}>
                    <Text style={styles.titleText}>QR Code Request</Text>
                </View>
                <Text style={styles.descriptionText}>Share QR Code for another user to perform Deposit Request</Text>
            </View>
            <ScrollView style={styles.container}>
                <VStack flex={1} justifyContent="flex-start" alignItems="center">
                    <View>
                        <View
                            style={{
                                borderRadius: 5,
                                borderWidth: 1,
                                borderColor: '#ECECEC',
                                width: '100%',
                                padding: 20,
                                marginVertical: 20,
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}
                        >
                            <QRCode
                                value={`aok://send?address=${params.address}&amount=${params.amount}&token=${
                                    params.token === undefined ? '' : params.token.name
                                }`}
                                size={250}
                            />
                        </View>
                    </View>
                    <View style={styles.alignment}>
                        <View style={styles.inputContainer}>
                            <Text style={styles.caption}>{t('address')}</Text>
                            <View style={styles.content}>
                                <Text style={styles.contentText} selectable>
                                    {params.address}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.inputContainer}>
                            <Text style={styles.caption}>{t('amount')}</Text>
                            <View style={styles.content}>
                                <Text style={styles.contentText} selectable>
                                    {params.amount} {params.token ? params.token.name : Config.COIN_NAME}
                                </Text>
                            </View>
                        </View>
                        {params.token && (
                            <View style={styles.inputContainer}>
                                <Text style={styles.caption}>{t('token')}</Text>
                                <View style={styles.content}>
                                    <Text style={styles.contentText} selectable>
                                        {params.token.name}
                                    </Text>
                                </View>
                            </View>
                        )}
                        {params.timelock && (
                            <View style={styles.inputContainer}>
                                <Text style={styles.caption}>{t('timelock')}</Text>
                                <View style={styles.content}>
                                    <Text style={styles.contentText} selectable>
                                        {moment(params.timelock * 1000).format('LLL')}
                                    </Text>
                                </View>
                            </View>
                        )}
                    </View>
                </VStack>
            </ScrollView>
        </LinearGradient>
    );
};


export default QRCodeRequest;
