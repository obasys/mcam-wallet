import React, { useEffect } from 'react';
import { ActivityIndicator, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { useDispatch } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import { useTranslation } from 'react-i18next';
import { CommonActions } from '@react-navigation/native';
import { reset } from '../../redux/actions/appActions';
import { FocusAwareStatusBar, VStack } from '../../components';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginBottom: 20,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    infoContainer: {
        marginTop: 30,
    },
    titleContainer: {
        marginBottom: 15,
    },
    titleText: {
        color: '#614384',
        fontSize: 21,
        fontWeight: 'bold',
    },
    subtitleText: {
        color: '#614384',
        fontSize: 14,
        opacity: 0.5,
    },
});

interface FactoryResetProps {
    navigation: any;
    route: any;
}

const FactoryReset: React.FC<FactoryResetProps> = ({ navigation, route }: FactoryResetProps) => {
    const { t } = useTranslation('factoryReset');
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(reset());

        navigation.dispatch(
            CommonActions.reset({
                index: 0,
                routes: [{ name: 'OnboardingStack' }],
            }),
        );
    }, []);

    return (
        <LinearGradient colors={['#FFF', '#E2DEE7']} useAngle angle={180} style={{ flex: 1 }}>
            <FocusAwareStatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.container}>
                <VStack flex={1} style={styles.alignment}>
                    <ActivityIndicator color="#614384" size="large" />
                    <VStack style={styles.infoContainer}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.titleText}>{t('erasingData')}</Text>
                        </View>
                    </VStack>
                </VStack>
            </SafeAreaView>
        </LinearGradient>
    );
};


export default FactoryReset;
