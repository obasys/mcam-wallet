import React, { useContext, useEffect } from 'react';
import { Animated, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { useDispatch, useSelector } from 'react-redux';
import { FocusAwareStatusBar } from '../../components';
import { PasswordContext } from '../../providers';
import { RootState } from '../../types';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
        padding: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

interface SplashProps {
    navigation: any;
}

const Splash: React.FC<SplashProps> = ({ navigation }: SplashProps) => {
    const { unlockedPassword } = useContext(PasswordContext);
    const opacity = new Animated.Value(0);
    const dispatch = useDispatch();
    const appReducer = useSelector((state: RootState) => state.app);

    const animateOpacity = () => {
        Animated.sequence([
            Animated.delay(100),
            Animated.timing(opacity, {
                toValue: 1,
                duration: 1000,
                useNativeDriver: true,
            }),
            Animated.timing(opacity, {
                toValue: 0.5,
                duration: 1000,
                useNativeDriver: true,
            }),
        ]).start(animateOpacity);
    };

    useEffect(() => {
        animateOpacity();
    }, []);

    useEffect(() => {
        if (appReducer.isMigrated) {
            if (appReducer.password.value !== undefined && appReducer.wallets.length > 0) {
                navigation.replace('PasswordStack', {
                    screen: 'Password',
                    params: { type: 'unlock', next: { stack: 'HomeStack', screen: 'Home' } },
                });
            } else if (appReducer.password.value !== undefined && unlockedPassword === undefined) {
                navigation.replace('PasswordStack', {
                    screen: 'Password',
                    params: { type: 'unlock', next: { stack: 'OnboardingStack', screen: 'Welcome' } },
                });
            } else {
                navigation.replace('OnboardingStack', { screen: 'Welcome' });
            }
        }
    }, [appReducer.isMigrated]);

    return (
        <LinearGradient colors={['#FFF', '#E2DEE7']} useAngle angle={180} style={styles.container}>
            <FocusAwareStatusBar barStyle="dark-content" />
            <Animated.Image
                source={require('../../assets/logo_splash.png')}
                style={{ height: '15%', opacity }}
                resizeMode="contain"
            />
        </LinearGradient>
    );
};

export default Splash;
