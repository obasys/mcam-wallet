import React from 'react';
import { StyleSheet, TouchableHighlight, TouchableHighlightProps, View } from 'react-native';

const styles = StyleSheet.create({
    root: {
        width: '33%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        width: 80,
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
    },
});

interface PadProps extends TouchableHighlightProps {
    hidden?: boolean;
    children?: any;
}

const Pad: React.FC<PadProps> = ({ style, children, hidden, ...props }: PadProps) => {
    return (
        <View style={styles.root}>
            <TouchableHighlight
                underlayColor="#614384"
                style={[styles.container, style, { display: hidden ? 'none' : 'flex' }]}
                {...props}
            >
                {children}
            </TouchableHighlight>
        </View>
    );
};

export default Pad;
