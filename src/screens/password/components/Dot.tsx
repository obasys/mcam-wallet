import React from 'react';
import { StyleSheet, View, ViewProps } from 'react-native';

const styles = StyleSheet.create({
    container: {
        borderRadius: 15,
        height: 15,
        width: 15,
    },
});

interface DotProps extends ViewProps {
    filled?: boolean;
}

const Dot: React.FC<DotProps> = ({ filled, style, ...props }: DotProps) => {
    return <View style={[styles.container, { backgroundColor: filled ? '#614384' : 'transparent' }, style]} />;
};

export default Dot;
