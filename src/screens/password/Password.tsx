import React, { useContext, useEffect, useState } from 'react';
import { Alert, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';
import { useDispatch, useSelector } from 'react-redux';
import { showMessage } from 'react-native-flash-message';
import { useTranslation } from 'react-i18next';
import SInfo from 'react-native-sensitive-info';
import { Button, DismissKeyboard, FocusAwareStatusBar, HStack, Input, VStack } from '../../components';
import { PasswordContext } from '../../providers';
import { decryptData, encryptData } from '../../services/commonService';
import { Dot, Pad } from './components';
import { setPassword, updateWallets } from '../../redux/actions/appActions';
import { RootState } from '../../types';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 110,
        marginBottom: 20,
    },
    alignment: {
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
        flex: 1,
    },
    numberText: {
        fontSize: 36,
        color: '#614384',
    },
    dotContainer: {
        marginHorizontal: 10,
        paddingVertical: 15,
        paddingHorizontal: 10,
        borderBottomWidth: 1,
        borderColor: '#614384',
    },
    numbersContainer: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    headerContainer: {
        marginBottom: 20,
    },
    titleContainer: {
        marginBottom: 15,
    },
    titleText: {
        fontSize: 21,
        fontWeight: 'bold',
        color: '#614384',
    },
    subtitleText: {
        fontSize: 14,
        color: '#999999',
    },
    passwordInput: {
        color: '#614384',
        fontSize: 21,
    },
    authButtonText: {
        textDecorationLine: 'underline',
    },
});

interface PasswordProps {
    navigation: any;
    route: any;
}

const Password: React.FC<PasswordProps> = ({ navigation, route }: PasswordProps) => {
    const { t } = useTranslation('password');
    const appReducer = useSelector((state: RootState) => state.app);
    const dispatch = useDispatch();
    const { setUnlockedPassword, unlockedPassword } = useContext(PasswordContext);
    const [passwordValue, setPasswordValue] = useState<string>('');
    const [newPasswordValue, setNewPasswordValue] = useState<string>('');
    const [step, setStep] = useState<'type-password' | 'repeat-password'>('type-password');
    const { type, unlock, next, goBack } = route.params ?? { goBack: false };

    const handlePin = (number: number) => {
        passwordValue.length < 6 && setPasswordValue(`${passwordValue}${number}`);
    };

    const deleteNum = () => {
        passwordValue.length > 0 && setPasswordValue(passwordValue.slice(0, -1));
    };

    const reset = () => {
        setStep('type-password');
        setPasswordValue('');
        setNewPasswordValue('');
    };

    const setNewPassword = async (password: string, useBiometric: boolean, isSensorAvailable = true) => {
        if (isSensorAvailable) {
            try {
                const settedItem = await SInfo.setItem('password', password, {
                    sharedPreferencesName: 'mySharedPrefs',
                    keychainService: 'myKeychain',
                    touchID: true,
                    showModal: true,
                    kSecAccessControl: 'kSecAccessControlBiometryAny',
                });
            } catch (e) {
                useBiometric = false;
                console.warn('Error while setting keychain password. ', e);
            }
        }

        if (appReducer.password.value) {
            if (!unlockedPassword) {
                showMessage({
                    message: t('appNotUnlocked.message'),
                    description: t('appNotUnlocked.description'),
                    type: 'danger',
                });

                return;
            }

            if (appReducer.wallets.length > 0) {
                const decryptedWallets = [];
                const newEncryptedWallets = [];

                for (var i = 0; i < appReducer.wallets.length; ++i) {
                    decryptedWallets.push({
                        ...appReducer.wallets[i],
                        seedPhrase: decryptData(appReducer.wallets[i].seedPhrase, unlockedPassword),
                        addresses: appReducer.wallets[i].addresses.map((address: any) => {
                            return { ...address, wif: decryptData(address.wif, unlockedPassword) };
                        }),
                    });
                }

                for (var i = 0; i < decryptedWallets.length; ++i) {
                    newEncryptedWallets.push({
                        ...decryptedWallets[i],
                        seedPhrase: encryptData(decryptedWallets[i].seedPhrase, password),
                        addresses: decryptedWallets[i].addresses.map((address: any) => {
                            return { ...address, wif: encryptData(address.wif, password) };
                        }),
                    });
                }

                dispatch(updateWallets(newEncryptedWallets));
            }
        }

        dispatch(setPassword({ value: encryptData(password, password), useBiometric }));
        setUnlockedPassword(password);

        showMessage({
            message: t('alerts.newPasswordCreated.message'),
            description: t('alerts.newPasswordCreated.description'),
            backgroundColor: '#614384',
        });

        if (!next) {
            navigation.replace('OnboardingStack', { screen: 'RecoveryPhrase' });
        } else {
            navigation.replace(next.stack, {
                screen: 'screen' in next ? next.screen : undefined,
                params: 'params' in next ? next.params : {},
            });
        }
    };

    const handleNewPassword = (first: string, second: string) => {
        if (first === second) {
            SInfo.isSensorAvailable().then((sensor) => {
                if (sensor) {
                    Alert.alert(
                        t('alerts.useBiometric.message'),
                        t('alerts.useBiometric.description'),
                        [
                            {
                                text: t('alerts.useBiometric.cancel'),
                                onPress: () => setNewPassword(first, false),
                                style: 'cancel',
                            },
                            { text: t('alerts.useBiometric.ok'), onPress: () => setNewPassword(first, true) },
                        ],
                        { cancelable: false },
                    );
                } else {
                    setNewPassword(first, false, false);
                }
            });
        } else {
            showMessage({
                message: t('alerts.passwordsNotMatch.message'),
                description: t('alerts.passwordsNotMatch.description'),
                type: 'danger',
            });
        }

        reset();
    };

    const handleUnlock = (password: string) => {
        if (password === decryptData(appReducer.password.value, password)) {
            setUnlockedPassword(password);
            if (next) {
                navigation.replace(next.stack, { screen: next.screen, params: 'params' in next ? next.params : {} });
            } else {
                navigation.goBack();
            }
        } else {
            setPasswordValue('');

            showMessage({
                message: t('alerts.passwordIncorrect.message'),
                description: t('alerts.passwordIncorrect.description'),
                type: 'danger',
            });
        }
    };

    const handleProcessTypes = () => {
        if (type === 'new-password') {
            if (step === 'type-password') {
                setStep('repeat-password');
                setNewPasswordValue(passwordValue);
                setPasswordValue('');
            } else if (step === 'repeat-password') {
                handleNewPassword(newPasswordValue, passwordValue);
            }
        }

        if (type === 'unlock') {
            handleUnlock(passwordValue);
        }
    };

    const handleEnterPassword = () => {
        if (passwordValue.length >= 4) {
            handleProcessTypes();
        } else {
            showMessage({
                message: t('alerts.passwordTooShort.message'),
                description: t('alerts.passwordTooShort.description'),
                type: 'danger',
            });
        }
    };

    const handleChangeAuthMethod = () => {
        navigation.navigate('ChangePasswordMethod');
        reset();
    };

    const unlockByBiometric = () => {
        SInfo.isSensorAvailable().then((sensor) => {
            if (sensor) {
                SInfo.getItem('password', {
                    sharedPreferencesName: 'mySharedPrefs',
                    keychainService: 'myKeychain',
                    touchID: true,
                    showModal: true,
                    strings: {
                        description: t('androidBiometric.description'),
                        header: t('androidBiometric.header'),
                    },
                    kSecUseOperationPrompt: t('iosBiometric.prompt'),
                }).then((data) => {
                    handleUnlock(data);
                });
            }
        });
    };

    useEffect(() => {
        if (appReducer.password.type === 'pin' && passwordValue.length === 6) {
            handleProcessTypes();
        }
    }, [passwordValue]);

    useEffect(() => {
        if (type === 'unlock' && appReducer.password.useBiometric) {
            unlockByBiometric();
        }
        if (!goBack) {
            navigation.setOptions({
                headerLeft: () => null,
                gestureEnabled: false,
            });
        }
    }, []);

    return (
        <DismissKeyboard>
            <LinearGradient colors={['#FFF', '#614384']} useAngle angle={180} style={{ flex: 1 }}>
                <SafeAreaView style={styles.container}>
                    <FocusAwareStatusBar barStyle="dark-content" />
                    <View style={styles.alignment}>
                        <VStack>
                            <VStack style={styles.headerContainer}>
                                <View style={styles.titleContainer}>
                                    <Text style={styles.titleText}>
                                        {type === 'new-password' &&
                                            (appReducer.password.type === 'pin'
                                                ? t('title.newPassword.pin')
                                                : t('title.newPassword.password'))}
                                        {type === 'unlock' && t('title.unlock')}
                                    </Text>
                                </View>
                                <Text style={styles.subtitleText}>
                                    {type === 'new-password' &&
                                        step === 'type-password' &&
                                        (appReducer.password.type === 'pin'
                                            ? t('description.newPassword.pin')
                                            : t('description.newPassword.password'))}
                                    {type === 'new-password' &&
                                        step === 'repeat-password' &&
                                        (appReducer.password.type === 'pin'
                                            ? t('description.newPassword.repeat.pin')
                                            : t('description.newPassword.repeat.password'))}
                                    {type === 'unlock' &&
                                        (appReducer.password.type === 'pin'
                                            ? t('description.unlock.pin')
                                            : t('description.unlock.password'))}
                                </Text>
                            </VStack>
                            {appReducer.password.type === 'pin' && (
                                <HStack>
                                    <View style={styles.dotContainer}>
                                        <Dot filled={passwordValue[0] !== undefined} />
                                    </View>
                                    <View style={styles.dotContainer}>
                                        <Dot filled={passwordValue[1] !== undefined} />
                                    </View>
                                    <View style={styles.dotContainer}>
                                        <Dot filled={passwordValue[2] !== undefined} />
                                    </View>
                                    <View style={styles.dotContainer}>
                                        <Dot filled={passwordValue[3] !== undefined} />
                                    </View>
                                    <View style={styles.dotContainer}>
                                        <Dot filled={passwordValue[4] !== undefined} />
                                    </View>
                                    <View style={styles.dotContainer}>
                                        <Dot filled={passwordValue[5] !== undefined} />
                                    </View>
                                </HStack>
                            )}
                            {appReducer.password.type === 'password' && (
                                <Input
                                    placeholder="*********"
                                    placeholderTextColor="#cccccc"
                                    autoFocus
                                    autoCompleteType="password"
                                    secureTextEntry
                                    value={passwordValue}
                                    onChangeText={(text) => setPasswordValue(text)}
                                    inputStyle={styles.passwordInput}
                                    rightContent={
                                        <Button
                                            title={t('enterButton')}
                                            backgroundColor="transparent"
                                            onPress={handleEnterPassword}
                                        />
                                    }
                                />
                            )}
                            {type === 'new-password' && (
                                <Button
                                    my={20}
                                    title={t('changeAuthMethod')}
                                    backgroundColor="transparent"
                                    textStyle={styles.authButtonText}
                                    onPress={handleChangeAuthMethod}
                                />
                            )}
                        </VStack>

                        {appReducer.password.type === 'pin' ? (
                            <View style={styles.numbersContainer}>
                                <Pad onPress={() => handlePin(1)}>
                                    <Text style={styles.numberText}>1</Text>
                                </Pad>
                                <Pad onPress={() => handlePin(2)}>
                                    <Text style={styles.numberText}>2</Text>
                                </Pad>
                                <Pad onPress={() => handlePin(3)}>
                                    <Text style={styles.numberText}>3</Text>
                                </Pad>
                                <Pad onPress={() => handlePin(4)}>
                                    <Text style={styles.numberText}>4</Text>
                                </Pad>
                                <Pad onPress={() => handlePin(5)}>
                                    <Text style={styles.numberText}>5</Text>
                                </Pad>
                                <Pad onPress={() => handlePin(6)}>
                                    <Text style={styles.numberText}>6</Text>
                                </Pad>
                                <Pad onPress={() => handlePin(7)}>
                                    <Text style={styles.numberText}>7</Text>
                                </Pad>
                                <Pad onPress={() => handlePin(8)}>
                                    <Text style={styles.numberText}>8</Text>
                                </Pad>
                                <Pad onPress={() => handlePin(9)}>
                                    <Text style={styles.numberText}>9</Text>
                                </Pad>
                                <Pad
                                    hidden={type !== 'unlock' || appReducer.password.useBiometric === false}
                                    onPress={unlockByBiometric}
                                >
                                    <Ionicon name="finger-print-outline" size={40} color="#614384" />
                                </Pad>
                                <Pad onPress={() => handlePin(0)}>
                                    <Text style={styles.numberText}>0</Text>
                                </Pad>
                                <Pad onPress={deleteNum}>
                                    <Ionicon name="backspace-outline" size={40} color="#614384" />
                                </Pad>
                            </View>
                        ) : (
                            <HStack flex={1} />
                        )}
                    </View>
                </SafeAreaView>
            </LinearGradient>
        </DismissKeyboard>
    );
};

export default Password;
