import React from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import IoniconsIcon from 'react-native-vector-icons/Ionicons';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { setPassword } from '../../redux/actions/appActions';
import { FocusAwareStatusBar, TableItem } from '../../components';
import { RootState } from '../../types';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 110,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    descriptionText: {
        color: 'white',
        opacity: 0.5,
    },
    descriptionContainer: {
        marginBottom: 15,
    },
});

interface ChangePasswordMethodProps {
    navigation: any;
}

const ChangePasswordMethod: React.FC<ChangePasswordMethodProps> = ({ navigation }: ChangePasswordMethodProps) => {
    const { t } = useTranslation('changePasswordMethod');
    const appReducer = useSelector((state: RootState) => state.app);
    const dispatch = useDispatch();

    const handleAuthMethodChange = (type: 'password' | 'pin') => {
        dispatch(setPassword({ type }));
        navigation.goBack();
    };

    return (
        <LinearGradient colors={['#FFF', '#E2DEE7']} useAngle angle={180} style={{ flex: 1 }}>
            <FocusAwareStatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.container}>
                <View style={[styles.alignment, styles.descriptionContainer]}>
                    <Text style={styles.descriptionText}>{t('description')}</Text>
                </View>
                <TableItem
                    title={t('pinTitle')}
                    backgroundColor="transparent"
                    underlayColor="#232E63"
                    color="white"
                    leftContent={
                        <IoniconsIcon
                            name={appReducer.password.type === 'pin' ? 'checkmark-circle' : 'ellipse-outline'}
                            size={28}
                            color="white"
                        />
                    }
                    rightContent={<EntypoIcon name="chevron-thin-right" size={20} color="white" />}
                    onPress={() => handleAuthMethodChange('pin')}
                />
                <TableItem
                    title={t('passwordTitle')}
                    backgroundColor="transparent"
                    underlayColor="#232E63"
                    color="white"
                    leftContent={
                        <IoniconsIcon
                            name={appReducer.password.type === 'password' ? 'checkmark-circle' : 'ellipse-outline'}
                            size={28}
                            color="white"
                        />
                    }
                    rightContent={<EntypoIcon name="chevron-thin-right" size={20} color="white" />}
                    onPress={() => handleAuthMethodChange('password')}
                />
            </SafeAreaView>
        </LinearGradient>
    );
};

export default ChangePasswordMethod;
