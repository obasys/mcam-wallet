import React, { useContext } from 'react';
import { SafeAreaView, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import QRCode from 'react-native-qrcode-svg';
import Clipboard from '@react-native-community/clipboard';
import { showMessage } from 'react-native-flash-message';
import { useTranslation } from 'react-i18next';
import Share from 'react-native-share';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import { WalletContext } from '../../providers';
import { FocusAwareStatusBar, HStack, Button } from '../../components';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    buttonsContainer: {
        marginTop: 20,
    },
    alignment: {
        paddingHorizontal: 20,
        alignItems: 'center',
        flex: 1,
    },
    numberText: {
        fontSize: 36,
        fontWeight: '200',
    },
    dotContainer: {
        marginHorizontal: 15,
    },
    numbersContainer: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
});

interface ReceiveProps {
    navigation: any;
}

const hapticOptions = {
    enableVibrateFallback: true,
    ignoreAndroidSystemSettings: false,
};

const Receive: React.FC<ReceiveProps> = ({ navigation }: ReceiveProps) => {
    const { t } = useTranslation('deposit');
    const { wallet } = useContext(WalletContext);

    if (!wallet) {
        return <View />;
    }

    const copyToClipboard = () => {
        Clipboard.setString(wallet!.depositAddress);
        ReactNativeHapticFeedback.trigger('impactHeavy', hapticOptions);
        showMessage({
            message: t('alerts.copiedAddress.message'),
            description: t('alerts.copiedAddress.description'),
            backgroundColor: '#614384',
        });
    };

    const share = async () => {
        await Share.open({
            message: wallet!.depositAddress,
        });
    };

    return (
        <SafeAreaView style={styles.container}>
            <FocusAwareStatusBar barStyle="dark-content" backgroundColor="white" />
            <View style={styles.alignment}>
                <TouchableWithoutFeedback onLongPress={copyToClipboard}>
                    <View
                        style={{
                            borderRadius: 5,
                            borderWidth: 1,
                            borderColor: '#ECECEC',
                            padding: 20,
                            marginBottom: 20,
                        }}
                    >
                        <QRCode value={wallet!.depositAddress} size={250} />
                    </View>
                </TouchableWithoutFeedback>
                <Text style={{ fontSize: 14, color: '#614384', marginBottom: 10 }}>
                    Your wallet Address
                </Text>
                <Text selectable style={{ fontSize: 12, color: '#999999' }}>
                    {wallet!.depositAddress}
                </Text>
                <HStack style={styles.buttonsContainer}>
                    <Button
                        title={t('copy')}
                        backgroundColor="transparent"
                        color="#614384"
                        border='1'
                        borderColor="#c8b4df"
                        onPress={copyToClipboard}
                    />
                </HStack>
            </View>
        </SafeAreaView>
    );
};

export default Receive;
