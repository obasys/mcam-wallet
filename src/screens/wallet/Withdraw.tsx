import React, { useContext, useEffect, useState } from 'react';
import {
    Alert,
    FlatList,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Switch,
    Text,
    View,
} from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import Clipboard from '@react-native-community/clipboard';
import { useTranslation } from 'react-i18next';
import Config from 'react-native-config';
import moment from 'moment';
import { WalletContext } from '../../providers';
import { Button, Coin, DismissKeyboard, FocusAwareStatusBar, HStack, Input, VStack } from '../../components';
import { AddressBook, Token } from '../../types';
import { isAddress } from '../../services/addressService';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    caption: {
        fontSize: 12,
        color: '#614384',
    },
    inputContainer: {
        width: '100%',
        marginBottom: 15,
    },
    totalText: {
        fontSize: 21,
        fontWeight: 'bold',
        color: '#614384'
    },
    balanceText: {
        fontSize: 12,
        opacity: 0.5,
    },
    totalContainer: {
        marginTop: 10,
    },
    balanceContainer: {
        alignItems: 'flex-end',
        marginTop: 5,
    },
});

interface SendProps {
    navigation: any;
    route: any;
}

const Send: React.FC<SendProps> = ({ navigation, route }: SendProps) => {
    const { t, i18n } = useTranslation('withdraw');
    const defaultParams = { address: '', amount: '', timelock: undefined, fee: '0.1', token: undefined };
    const params = route.params ? { ...defaultParams, ...route.params } : defaultParams;
    const { wallet, sendTransation } = useContext(WalletContext);
    const [address, setAddress] = useState<string>(params.address);
    const [amount, setAmount] = useState<string>(params.amount);
    const [fee, setFee] = useState<string>(params.fee);
    const [customFeeEnabled, setCustomFeeEnabled] = useState(false);
    const [timelockEnabled, setTimelockEnabled] = useState(false);
    const [token, setToken] = useState<Token | undefined>(
        wallet!.tokens.find((item) => item.tokenName === params.token),
    );
    const [timelock, setTimelock] = useState<Date>(params.timelock);
    const [isClipboardActive, setIsClipboardActive] = useState(false);
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const formattedTotalSend = (
        parseFloat(!amount || amount === '' || amount === '.' || amount === ',' ? '0' : amount) +
        (token ? 0 : fee === '' ? 0 : parseFloat(fee))
    )
        .toFixed(4)
        .toString();
    const formattedBalance = ((token ? token.balance : wallet!.balance.confirmed) / 10 ** 4)
        .toFixed(token ? token.units : 8)
        .toString();

    const checkAmount = (text: string) => {
        const regex = /^[0-9]{0,100}([.,][0-9]{0,4})?$/;
        regex.test(text) && setAmount(text.replace(',', '.'));
    };

    const checkFee = (text: string) => {
        const regex = /^[0-9]{0,100}([.,][0-9]{0,4})?$/;
        regex.test(text) && setFee(text.replace(',', '.'));
    };

    const getFromQRCode = () => {
        navigation.navigate('QRCodeScanner', {
            type: 'withdraw',
        });
    };

    const send = () => {
        const sendConfirmed = () => {
            navigation.replace('SendTransaction', {
                address,
                amount,
                fee,
                token,
                timelock: timelock ? moment(timelock).unix() : undefined,
            });
        };

        Alert.alert(
            t('alerts.withdrawConfirmation.message'),
            t('alerts.withdrawConfirmation.description', { amount, coin: token ? token.tokenName : Config.COIN_NAME }),
            [
                {
                    text: t('alerts.withdrawConfirmation.cancel'),
                    onPress: () => null,
                    style: 'cancel',
                },
                {
                    text: t('alerts.withdrawConfirmation.confirm'),
                    onPress: sendConfirmed,
                    style: 'default',
                },
            ],
            { cancelable: false },
        );
    };

    const getFromClipboard = () => {
        Clipboard.getString().then((string) => {
            const addresses = string.match(new RegExp(Config.ADDRESS_REGEX));

            if (addresses && addresses.length > 0) {
                setAddress(addresses[0]);
            }
        });
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirmTimelock = (date: Date) => {
        setTimelock(date);
        hideDatePicker();
    };

    useEffect(() => {
        if (Clipboard.hasString()) {
            Clipboard.getString().then((string) => {
                const addresses = string.match(new RegExp(Config.ADDRESS_REGEX));

                if (addresses && addresses.length > 0) {
                    setIsClipboardActive(true);
                }
            });
        }
    }, []);

    useEffect(() => {
        if (route.params?.address) {
            setAddress(route.params.address);
        }

        if (route.params?.amount) {
            setAmount(route.params.amount);
        }

        if (route.params?.timelock) {
            setTimelock(route.params.timelock);
        }
    }, [route.params]);

    return (
        <DismissKeyboard>
            <SafeAreaView style={styles.container}>
                <FocusAwareStatusBar barStyle="dark-content" backgroundColor="white" />
                <KeyboardAvoidingView
                    style={styles.container}
                    behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                    keyboardVerticalOffset={100}
                >
                    <ScrollView>
                        {wallet!.tokens.length > 0 && (
                            <View style={styles.inputContainer}>
                                <View style={styles.alignment}>
                                    <Text style={styles.caption}>{t('token')}</Text>
                                </View>
                                <HStack>
                                    <FlatList
                                        style={{ flexGrow: 0, width: '100%', paddingLeft: 20 }}
                                        horizontal
                                        data={wallet!.tokens}
                                        keyExtractor={(item) => item.tokenName}
                                        renderItem={({ item }) => (
                                            <Button
                                                title={item.tokenName}
                                                onPress={() =>
                                                    token && token.tokenName === item.tokenName
                                                        ? setToken(undefined)
                                                        : setToken(item)
                                                }
                                                size="lg"
                                                style={{ minHeight: 40 }}
                                                border={!token || token.tokenName !== item.tokenName}
                                                borderColor="#ECECEC"
                                                backgroundColor={
                                                    token && token.tokenName === item.tokenName ? '#614384' : '#eee6f8'
                                                }
                                                color={token && token.tokenName === item.tokenName ? 'white' : '#614384'}
                                                mt={10}
                                                mr={10}
                                            />
                                        )}
                                    />
                                </HStack>
                            </View>
                        )}
                        <VStack style={styles.alignment} justifyContent="flex-start" alignItems="flex-start" flex={1}>
                            <View style={styles.inputContainer}>
                                <Text style={styles.caption}>{t('withdrawAddress.title')}</Text>
                                <Input
                                    placeholder={t('withdrawAddress.placeholder')}
                                    autoFocus={!address || address === ''}
                                    onChangeText={(text) => setAddress(text)}
                                    value={address}
                                />
                                <HStack justifyContent="space-between">
                                    <Button
                                        onPress={() =>
                                            navigation.navigate('AddressBookStack', {
                                                screen: 'AddressBook',
                                                params: {
                                                    type: 'choose-address',
                                                    callback: (addressBookItem: AddressBook) =>
                                                        setAddress(addressBookItem.address),
                                                },
                                            })
                                        }
                                        title={t('addressBook')}
                                        size="md"
                                        border
                                        borderColor="#ECECEC"
                                        backgroundColor="#614384"
                                        color="white"
                                        mt={10}
                                        mr={10}
                                        flex={1}
                                    />
                                    <Button
                                        onPress={getFromQRCode}
                                        title={t('qrScanner')}
                                        size="md"
                                        border
                                        borderColor="#ECECEC"
                                        backgroundColor="#eee6f8"
                                        color="#614384"
                                        mt={10}
                                        mr={10}
                                        flex={1}
                                    />
                                    <Button
                                        onPress={getFromClipboard}
                                        disabled={!isClipboardActive}
                                        title={t('clipboard')}
                                        size="md"
                                        border
                                        borderColor="#ECECEC"
                                        backgroundColor="#eee6f8"
                                        color="#614384"
                                        mt={10}
                                        flex={1}
                                    />
                                </HStack>
                            </View>
                            <View style={styles.inputContainer}>
                                <Text style={styles.caption}>{t('amount.title')}</Text>
                                <Input
                                    placeholder={t('amount.placeholder')}
                                    autoFocus={address !== undefined && address !== ''}
                                    rightContent={
                                        <View style={{ marginLeft: 5 }}>
                                            <Text style={{ fontWeight: 'bold' }}>
                                                {token ? token.tokenName : Config.COIN_NAME}
                                            </Text>
                                        </View>
                                    }
                                    onChangeText={(text) => checkAmount(text)}
                                    value={amount}
                                    keyboardType="numeric"
                                />
                            </View>
                            <HStack style={styles.inputContainer} justifyContent="space-between">
                                <Text style={{ fontSize: 16 }}>{t('customFee')}</Text>
                                <Switch
                                    onValueChange={() => setCustomFeeEnabled(!customFeeEnabled)}
                                    value={customFeeEnabled}
                                    trackColor={{ false: '#767577', true: '#614384' }}
                                />
                            </HStack>
                            {customFeeEnabled && (
                                <View style={styles.inputContainer}>
                                    <Text style={styles.caption}>{t('fee.title')}</Text>
                                    <Input
                                        placeholder={t('fee.placeholder')}
                                        rightContent={
                                            <View style={{ marginLeft: 5 }}>
                                                <Text style={{ fontWeight: 'bold' }}>{Config.COIN_NAME}</Text>
                                            </View>
                                        }
                                        onChangeText={(text) => checkFee(text)}
                                        value={fee}
                                        keyboardType="numeric"
                                    />
                                </View>
                            )}
                            <HStack style={styles.inputContainer} justifyContent="space-between">
                                <Text style={{ fontSize: 16 }}>Time lock date</Text>
                                <Switch
                                    onValueChange={() => setTimelockEnabled(!timelockEnabled)}
                                    value={timelockEnabled}
                                    trackColor={{ false: '#767577', true: '#614384' }}
                                />
                            </HStack>
                            {timelockEnabled && (
                                <View style={styles.inputContainer}>
                                    <Text style={styles.caption}>{t('timelockDate.title')}</Text>
                                    <Input
                                        placeholder={t('timelockDate.placeholder')}
                                        type="date"
                                        onPress={() => setDatePickerVisibility(true)}
                                        value={timelock && moment(timelock).format('LLL')}
                                        rightContent={
                                            <View style={{ marginLeft: 5 }}>
                                                <Ionicon name="ios-calendar" size={20} color="#614384" />
                                            </View>
                                        }
                                        isVisible={isDatePickerVisible}
                                        mode="datetime"
                                        locale={i18n.language}
                                        cancelTextIOS={t('datepicker.cancelIOS')}
                                        confirmTextIOS={t('datepicker.confirmIOS')}
                                        headerTextIOS={t('datepicker.headerIOS')}
                                        onConfirm={handleConfirmTimelock}
                                        onCancel={hideDatePicker}
                                    />
                                </View>
                            )}
                            <View style={styles.inputContainer}>
                                <HStack style={styles.totalContainer} justifyContent="flex-end" alignItems="flex-end">
                                    <Text style={styles.totalText}>
                                        {formattedTotalSend} {token ? token.tokenName : null}
                                        <Text style={{ opacity: 0.5, fontSize: 18 }}>
                                            {token &&
                                                ` + ${(fee === '' ? 0 : parseFloat(fee)).toFixed(4)} ${
                                                    Config.COIN_NAME
                                                }`}
                                        </Text>
                                    </Text>
                                    {!token && <Coin tintColor="#614384" />}
                                </HStack>
                                <View style={styles.balanceContainer}>
                                    <Text style={styles.balanceText}>
                                        {t('balance', {
                                            coin: token ? token.tokenName : Config.COIN_NAME,
                                            balance: formattedBalance,
                                        })}
                                    </Text>
                                </View>
                            </View>
                        </VStack>
                    </ScrollView>
                    <HStack style={styles.alignment}>
                        <Button
                            title={t('confirmButton')}
                            flex={1}
                            disabled={!isAddress(address) || !amount || amount === ''}
                            onPress={send}
                            mb={20}
                        />
                    </HStack>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </DismissKeyboard>
    );
};

export default Send;
