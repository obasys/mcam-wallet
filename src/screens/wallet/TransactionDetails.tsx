import React from 'react';
import { Linking, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { useTranslation } from 'react-i18next';
import Ionicon from 'react-native-vector-icons/Ionicons';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import { useSelector } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import Config from 'react-native-config';
import moment from 'moment';
import { AddressBook, RootState } from '../../types';
import { Button, Coin, FocusAwareStatusBar, HStack, IconButton, VStack } from '../../components';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    dividerContainer: {
        padding: 20,
    },
    divider: {
        width: 50,
        height: 6,
        borderRadius: 10,
        backgroundColor: '#614384',
    },
    titleContainer: {
        marginBottom: 15,
    },
    titleText: {
        fontSize: 18,
        color: '#614384',
        fontWeight: 'bold',
    },
    balanceContainer: {
        marginBottom: 5,
    },
    descriptionText: {
        fontSize: 16,
        color: '#614384',
        opacity: 0.5,
    },
    balanceText: {
        fontWeight: 'bold',
        textAlign: 'left',
        color: '#614384',
    },
    inputContainer: {
        marginBottom: 15,
    },
    caption: {
        fontSize: 12,
        color: '#614384',
    },
    content: {
        marginTop: 10,
    },
    contentText: {
        fontSize: 16,
    },
});

interface TransactionDetailsProps {
    navigation: any;
    route: any;
}

const TransactionDetails: React.FC<TransactionDetailsProps> = ({ navigation, route }: TransactionDetailsProps) => {
    const { t } = useTranslation('transactionDetails');
    const { transaction } = route.params ?? {};
    const appReducer = useSelector((state: RootState) => state.app);
    const formattedAmount = (transaction.amount / 10 ** 8).toFixed(4).toString().split('.');
    const formattedTokenAmount = transaction.tokens.length
        ? transaction.tokens[0].amount.toFixed(4).toString().split('.')
        : undefined;

    const getAddressItemFromAdressBook = () => {
        const addressItem = appReducer.addressBook.find((addressItemToFind: AddressBook) =>
            transaction.type === 'sent'
                ? addressItemToFind.address === transaction.to
                : addressItemToFind.address === transaction.from,
        );
        return addressItem || undefined;
    };

    const addressItem = getAddressItemFromAdressBook();

    return (
        <LinearGradient colors={['#FFF', '#E2DEE7']} useAngle angle={180} style={{ flex: 1 }}>
            <FocusAwareStatusBar barStyle="dark-content" />
            <View style={[styles.alignment, { paddingBottom: 20 }]}>
                <HStack style={styles.dividerContainer}>
                    <View style={styles.divider} />
                </HStack>
                <HStack justifyContent="space-between" alignItems="flex-end">
                    <View>
                        <View style={styles.titleContainer}>
                            <Text style={styles.titleText}>
                                {transaction.type === 'sent' && t('withdraw')}
                                {transaction.type === 'received' && t('deposit')}
                            </Text>
                        </View>
                        <HStack style={styles.balanceContainer} justifyContent="flex-start" alignItems="flex-end">
                            <Text style={styles.balanceText}>
                                <Text style={{ fontSize: 32 }}>
                                    {transaction.type === 'sent' ? '-' : '+'}
                                    {formattedTokenAmount
                                        ? formattedTokenAmount
                                            ? formattedTokenAmount[0]
                                            : '0'
                                        : formattedAmount[0]
                                        ? formattedAmount[0]
                                        : '0'}
                                </Text>
                                <Text style={{ fontSize: 18 }}>
                                    .
                                    {formattedTokenAmount
                                        ? formattedTokenAmount
                                            ? formattedTokenAmount[1]
                                            : '0000'
                                        : formattedAmount[1]
                                        ? formattedAmount[1]
                                        : '0000'}{' '}
                                    {transaction.tokens.length ? transaction.tokens[0].name : null}
                                </Text>
                            </Text>
                            {transaction.tokens.length === 0 && <Coin tintColor="#614384" />}
                        </HStack>
                        <Text style={styles.descriptionText}>{moment(transaction.time * 1000).format('LLL')}</Text>
                    </View>
                    <IconButton
                        backgroundColor="transparent"
                        onPress={() => Linking.openURL(`${Config.EXPLORER_URL}/#/transaction/${transaction.hash}`)}
                    >
                        <Ionicon name="link" size={30} color="#614384" />
                    </IconButton>
                </HStack>
            </View>
            <SafeAreaView style={styles.container}>
                <VStack
                    flex={1}
                    style={[styles.alignment, { paddingTop: 20 }]}
                    justifyContent="flex-start"
                    alignItems="flex-start"
                >
                    <View style={styles.inputContainer}>
                        <Text style={styles.caption}>{t('toAddress')}</Text>
                        <View style={styles.content}>
                            <Text style={styles.contentText} selectable>
                                {transaction.to}
                            </Text>
                        </View>
                    </View>
                    {transaction.type === 'sent' && (
                        <Button
                            leftContent={<EntypoIcon name="plus" size={20} color="#614384" />}
                            title={t('addToAddressBook')}
                            size="md"
                            backgroundColor="transparent"
                            color="black"
                            border
                            mb={15}
                            disabled={addressItem !== undefined}
                            onPress={() =>
                                navigation.navigate('AddressBookStack', {
                                    screen: 'ManageAddressBookItem',
                                    params: { address: transaction.to },
                                })
                            }
                        />
                    )}
                    <View style={styles.inputContainer}>
                        <Text style={styles.caption}>{t('fromAddress')}</Text>
                        <View style={styles.content}>
                            <Text style={styles.contentText} selectable>
                                {transaction.from}
                            </Text>
                        </View>
                    </View>
                    {transaction.type === 'received' && (
                        <Button
                            leftContent={<EntypoIcon name="plus" size={20} color="black" />}
                            title={t('addToAddressBook')}
                            size="md"
                            backgroundColor="transparent"
                            color="black"
                            border
                            mb={15}
                            disabled={addressItem !== undefined}
                            onPress={() =>
                                navigation.navigate('AddressBookStack', {
                                    screen: 'ManageAddressBookItem',
                                    params: { address: transaction.from },
                                })
                            }
                        />
                    )}
                    <View style={styles.inputContainer}>
                        <Text style={styles.caption}>{t('transactionHash')}</Text>
                        <View style={styles.content}>
                            <Text style={styles.contentText} selectable>
                                {transaction.hash}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.inputContainer}>
                        <Text style={styles.caption}>{t('confirmations')}</Text>
                        <View style={styles.content}>
                            <Text style={styles.contentText} selectable>
                                {transaction.confirmations}
                            </Text>
                        </View>
                    </View>
                    {transaction.timelock && transaction.timelock.time > 50000000 && (
                        <View style={styles.inputContainer}>
                            <Text style={styles.caption}>{t('timelockDate')}</Text>
                            <View style={styles.content}>
                                <Text style={styles.contentText} selectable>
                                    {moment(transaction.timelock.time * 1000).format('LLL')}
                                </Text>
                            </View>
                        </View>
                    )}
                    {transaction.timelock && transaction.timelock.time <= 50000000 && (
                        <View style={styles.inputContainer}>
                            <Text style={styles.caption}>{t('timelockHeight')}</Text>
                            <View style={styles.content}>
                                <Text style={styles.contentText} selectable>
                                    {transaction.timelock.time}
                                </Text>
                            </View>
                        </View>
                    )}
                    <View style={styles.inputContainer}>
                        <Text style={styles.caption}>{t('fee')}</Text>
                        <HStack style={styles.content} justifyContent="flex-start" alignItems="flex-start">
                            <Text style={styles.contentText} selectable>
                                {(transaction.fee / 10 ** 8).toFixed(4).toString()}
                            </Text>
                            <Coin />
                        </HStack>
                    </View>
                </VStack>
                <HStack style={styles.alignment}>
                    <Button title={t('backToList')} flex={1} mb={20} onPress={() => navigation.goBack()} />
                </HStack>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default TransactionDetails;
