import React, { useContext, useState } from 'react';
import { Alert, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useTranslation } from 'react-i18next';
import Clipboard from '@react-native-community/clipboard';
import { showMessage } from 'react-native-flash-message';
import { decryptData } from '../../services/commonService';
import { PasswordContext, WalletContext } from '../../providers';
import { Button, DismissKeyboard, FocusAwareStatusBar, Input, TableItem } from '../../components';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    caption: {
        fontSize: 12,
        color: '#614384',
    },
    inputContainer: {
        marginBottom: 30,
    },
    textColor: {
        color: '#614384'
    }
});

interface SettingsProps {
    navigation: any;
}

const Settings: React.FC<SettingsProps> = ({ navigation }: SettingsProps) => {
    const { t } = useTranslation('settings');
    const { wallet, deleteTransactions, changeWalletTitle } = useContext(WalletContext);
    const { unlockedPassword } = useContext(PasswordContext);
    const [walletTitle, setWalletTitle] = useState<string>(wallet!.title);

    const handleEnterWalletTitle = () => {
        changeWalletTitle!(walletTitle);
        showMessage({
            message: t('alerts.titleChanged.message'),
            description: t('alerts.titleChanged.description'),
            backgroundColor: '#614384',
        });
    };

    const copySeedPhrase = () => {
        const confirmedCopySeedPhrase = () => {
            if (unlockedPassword) {
                Clipboard.setString(decryptData(wallet!.seedPhrase, unlockedPassword));
                showMessage({
                    message: t('alerts.seedPhraseCopied.message'),
                    description: t('alerts.seedPhraseCopied.description'),
                    backgroundColor: '#614384',
                });
            }
        };

        Alert.alert(
            t('alerts.seedPhraseCopyConfirmation.message'),
            t('alerts.seedPhraseCopyConfirmation.description'),
            [
                {
                    text: t('alerts.seedPhraseCopyConfirmation.cancel'),
                    onPress: () => null,
                    style: 'cancel',
                },
                {
                    text: t('alerts.seedPhraseCopyConfirmation.confirm'),
                    onPress: confirmedCopySeedPhrase,
                    style: 'default',
                },
            ],
            { cancelable: false },
        );
    };

    const deleteWalletCache = () => {
        const confirmedDeleteCache = () => {
            deleteTransactions!();
            showMessage({
                message: t('alerts.cacheDeleted.message'),
                description: t('alerts.cacheDeleted.description'),
                backgroundColor: '#614384',
            });
        };

        Alert.alert(
            t('alerts.cacheDeleteConfirmation.message'),
            t('alerts.cacheDeleteConfirmation.description'),
            [
                {
                    text: t('alerts.cacheDeleteConfirmation.cancel'),
                    onPress: () => null,
                    style: 'cancel',
                },
                {
                    text: t('alerts.cacheDeleteConfirmation.confirm'),
                    onPress: confirmedDeleteCache,
                    style: 'destructive',
                },
            ],
            { cancelable: false },
        );
    };

    const deleteWalletItem = () => {
        const confirmedDeleteWallet = () => {
            navigation.navigate('HomeStack', { screen: 'DeleteWallet', params: { uuid: wallet!.uuid } });
        };

        Alert.alert(
            t('alerts.deleteWalletConfirmation.message'),
            t('alerts.deleteWalletConfirmation.description'),
            [
                {
                    text: t('alerts.deleteWalletConfirmation.cancel'),
                    onPress: () => null,
                    style: 'cancel',
                },
                {
                    text: t('alerts.deleteWalletConfirmation.confirm'),
                    onPress: confirmedDeleteWallet,
                    style: 'destructive',
                },
            ],
            { cancelable: false },
        );
    };

    return (
        <DismissKeyboard>
            <SafeAreaView style={styles.container}>
                <FocusAwareStatusBar barStyle="dark-content" backgroundColor="white" />
                <View style={styles.alignment}>
                    <View style={styles.inputContainer}>
                        <Text style={styles.caption}>{t('walletTitle.title')}</Text>
                        <Input
                            placeholder={t('walletTitle.placeholder')}
                            onChangeText={(text: string) => setWalletTitle(text)}
                            value={walletTitle}
                            rightContent={
                                <Button
                                    title={t('enter')}
                                    backgroundColor="transparent"
                                    color="black"
                                    onPress={handleEnterWalletTitle}
                                />
                            }
                        />
                    </View>
                </View>
                <TableItem
                    title={t('deleteHistoryCache')}
                    color='#614384'
                    onPress={deleteWalletCache}
                    icon={<MaterialIcon name="history" color="#614384" size={30} />}
                    rightContent={<MaterialIcon name="chevron-right" color="#614384" size={30} />}
                />
                <TableItem
                    title={t('copySeedPhrase')}
                    color='#614384'
                    onPress={copySeedPhrase}
                    icon={<MaterialIcon name="shield-outline" color="#614384" size={30} />}
                    rightContent={<MaterialIcon name="chevron-right" color="#614384" size={30} />}
                />
                <TableItem
                    title={t('deleteWallet')}
                    color='#614384'
                    onPress={deleteWalletItem}
                    icon={<MaterialIcon name="delete-variant" color="#614384" size={30} />}
                    rightContent={<MaterialIcon name="chevron-right" color="#614384" size={30} />}
                />
            </SafeAreaView>
        </DismissKeyboard>
    );
};

export default Settings;
