import React, { useContext, useEffect } from 'react';
import { ActivityIndicator, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { showMessage } from 'react-native-flash-message';
import { useTranslation } from 'react-i18next';
import { WalletContext } from '../../providers';
import { FocusAwareStatusBar, VStack } from '../../components';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginBottom: 20,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    infoContainer: {
        marginTop: 30,
    },
    titleContainer: {
        marginBottom: 15,
    },
    titleText: {
        color: '#614384',
        fontSize: 21,
        fontWeight: 'bold',
    },
    subtitleText: {
        color: '#614384',
        fontSize: 14,
        opacity: 0.5,
    },
});

interface SendTransactionProps {
    navigation: any;
    route: any;
}

const SendTransaction: React.FC<SendTransactionProps> = ({ navigation, route }: SendTransactionProps) => {
    const { t } = useTranslation('sendTransation');
    const { wallet, sendTransation, statuses, setStatuses } = useContext(WalletContext);
    const { address, amount, fee, timelock, token } = route.params ?? {};

    useEffect(() => {
        setTimeout(() => {
            sendTransation!(
                address,
                Math.pow(10, 4) * parseFloat(amount),
                Math.pow(10, 4) * fee,
                timelock,
                token,
            ).catch((e) => {
                showMessage({
                    message: t('alerts.error.message'),
                    description: e.message,
                    type: 'danger',
                });
                setStatuses!((prevState: any) => {
                    return { ...prevState, withdraw: 'failed' };
                });
            });
        }, 100);
    }, []);

    useEffect(() => {
        if (statuses!.withdraw === 'sent') {
            setStatuses!((prevState: any) => {
                return { ...prevState, withdraw: 'waiting' };
            });
            showMessage({
                message: t('alerts.transactionSent.message'),
                description: t('alerts.transactionSent.description'),
                backgroundColor: '#614384',
            });
            navigation.replace('WalletStack', { screen: 'History' });
        } else if (statuses!.withdraw === 'failed') {
            setStatuses!((prevState: any) => {
                return { ...prevState, withdraw: 'waiting' };
            });
            navigation.replace('WalletStack', { screen: 'History' });
        }
    }, [statuses]);

    return (
        <LinearGradient colors={['#FFF', '#E2DEE7']} useAngle angle={180} style={{ flex: 1 }}>
            <FocusAwareStatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.container}>
                <VStack flex={1} style={styles.alignment}>
                    <ActivityIndicator color="#614384" size="large" />
                    <VStack style={styles.infoContainer}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.titleText}>{t('sendingTransaction')}</Text>
                        </View>
                    </VStack>
                </VStack>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default SendTransaction;
