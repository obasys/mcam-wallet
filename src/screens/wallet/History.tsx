import React, { useContext, useEffect, useRef, useState } from 'react';
import {
    Animated,
    FlatList,
    Image,
    Platform,
    RefreshControl,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import OcticonsIcon from 'react-native-vector-icons/Octicons';
import MaskedView from '@react-native-community/masked-view';
import LinearGradient from 'react-native-linear-gradient';
import { useTranslation } from 'react-i18next';
import { SocketContext, WalletContext } from '../../providers';
import { Coin, FocusAwareStatusBar, HStack, IconButton, VStack } from '../../components';
import { TransactionItem } from './components';

const HEADER_MAX_HEIGHT = 300;
const HEADER_MIN_HEIGHT = 120;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerContainer: {
        width: '100%',
        zIndex: 1,
        top: 0,
        left: 0,
        right: 0,
        position: 'absolute',
        padding: 20,
        justifyContent: 'flex-end',
        height: HEADER_MAX_HEIGHT,
    },
    minimizedOptionsContainer: {},
    optionsContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
    },
    titleText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#614384',
    },
    caption: {
        color: '#614384',
        fontSize: 12,
        textAlign: 'center'
    },
    balanceContainer: {
        alignItems: 'flex-start',
    },
    balanceText: {
        fontWeight: 'bold',
        color: '#614384',
    },
    lockedBalanceContainer: {
        opacity: 0.5,
    },
    lockedBalanceText: {
        fontSize: 12,
        color: '#ff3d00',
    },
    historyMaskContainer: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: HEADER_MAX_HEIGHT,
        backgroundColor: 'white',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
    },
    backButton: {
        marginRight: 10,
    },
    noHistoryImage: {
        height: 200,
    },
    noHistoryText: {
        fontSize: 14,
        color: '#614384',
    },
});

interface HistoryProps {
    navigation: any;
}

const History: React.FC<HistoryProps> = ({ navigation }: HistoryProps) => {
    const { t } = useTranslation('history');
    const scrollY = useRef(new Animated.Value(Platform.OS === 'android' ? 0 : -HEADER_MAX_HEIGHT));
    const historyScroll = useRef<FlatList>(null);
    const { wallet, updateWalletInfo, statuses } = useContext(WalletContext);
    const { socketRequest, isConnected } = useContext(SocketContext);
    const [isRefreshing, setIsRefreshing] = useState(false);
    const [convertedBalance, setConvertedBalance] = useState(220000);
    const formattedBalance = (wallet!.balance.confirmed / 10 ** 8).toFixed(2).toString().split('.');

    const sortedTransactions = [...wallet!.transactions].sort(function (a, b) {
        const keyA = new Date(a.time * 1000);
        const keyB = new Date(b.time * 1000);

        if (keyA > keyB) return -1;
        if (keyA < keyB) return 1;
        return 0;
    });

    const balanceTranslateX = scrollY.current.interpolate({
        inputRange:
            Platform.OS === 'android'
                ? [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT]
                : [-HEADER_MAX_HEIGHT, -HEADER_MIN_HEIGHT],
        outputRange: [0, -40],
        extrapolate: 'clamp',
    });

    const scrollTranslateY = scrollY.current.interpolate({
        inputRange:
            Platform.OS === 'android'
                ? [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT]
                : [-HEADER_MAX_HEIGHT, -HEADER_MIN_HEIGHT],
        outputRange: [0, 95],
        extrapolate: 'clamp',
    });

    const headerTranslateY = scrollY.current.interpolate({
        inputRange:
            Platform.OS === 'android'
                ? [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT]
                : [-HEADER_MAX_HEIGHT, -HEADER_MIN_HEIGHT],
        outputRange: [0, -(HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT)],
        extrapolate: 'clamp',
    });

    const scrollOpacity = scrollY.current.interpolate({
        inputRange:
            Platform.OS === 'android'
                ? [0, (HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT) / 2]
                : [-HEADER_MAX_HEIGHT, -(HEADER_MAX_HEIGHT / 1.5)],
        outputRange: [1, 0],
        extrapolate: 'clamp',
    });

    const scrollScale = scrollY.current.interpolate({
        inputRange:
            Platform.OS === 'android'
                ? [0, (HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT) / 2, HEADER_MIN_HEIGHT]
                : [-HEADER_MAX_HEIGHT, -(HEADER_MAX_HEIGHT / 1.5), -HEADER_MIN_HEIGHT],
        outputRange: [1, 1, 0],
        extrapolate: 'clamp',
    });

    const balanceScale = scrollY.current.interpolate({
        inputRange:
            Platform.OS === 'android'
                ? [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT]
                : [-HEADER_MAX_HEIGHT, -HEADER_MIN_HEIGHT],
        outputRange: [1, 0.75],
        extrapolate: 'clamp',
    });

    const stickTo = (contentOffset: { x: number; y: number }) => {
        if (Platform.OS === 'android') {
            if (contentOffset.y > 0 && contentOffset.y <= HEADER_MAX_HEIGHT) {
                if (contentOffset.y <= (HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT) / 2) {
                    historyScroll.current?.scrollToOffset({ animated: true, offset: 0 });
                } else {
                    historyScroll.current?.scrollToOffset({
                        animated: true,
                        offset: HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT,
                    });
                }
            }
        } else if (contentOffset.y > -HEADER_MAX_HEIGHT && contentOffset.y <= -HEADER_MIN_HEIGHT) {
            if (contentOffset.y <= -((HEADER_MAX_HEIGHT + HEADER_MIN_HEIGHT) / 2)) {
                historyScroll.current?.scrollToOffset({ animated: true, offset: -HEADER_MAX_HEIGHT });
            } else {
                historyScroll.current?.scrollToOffset({ animated: true, offset: -HEADER_MIN_HEIGHT });
            }
        }
    };

    useEffect(() => {
        if (updateWalletInfo) {
            updateWalletInfo();
        }
    }, []);

    return (
        <LinearGradient colors={['#FFF', '#E2DEE7']} useAngle angle={180} style={{ flex: 1 }}>
            <View style={styles.container}>
                <FocusAwareStatusBar barStyle="dark-content" />
                <Animated.View
                    style={[
                        styles.headerContainer,
                        {
                            transform: [{ translateY: headerTranslateY }],
                        },
                    ]}
                >
                    <Animated.View style={[styles.titleContainer, { opacity: scrollOpacity }]}>
                        <TouchableOpacity onPress={navigation.goBack} style={styles.backButton}>
                            <EntypoIcon name="chevron-thin-left" size={28} color="#614384" />
                        </TouchableOpacity>
                        <HStack flex={1} justifyContent="flex-start" justifyContent="space-between">
                            <Text style={styles.titleText} numberOfLines={1}>
                                {wallet!.title}
                            </Text>
                            <OcticonsIcon name="settings" size={32} color="#614384" onPress={() => navigation.navigate('Settings')} />
                        </HStack>
                    </Animated.View>
                    <HStack justifyContent="flex-start" alignItems="flex-end" style={{ marginBottom: 30 }}>
                        <Animated.View style={{ flex: 1, transform: [{ translateY: scrollTranslateY }] }}>
                            <Animated.View
                                style={[
                                    styles.balanceContainer,
                                    { transform: [{ scale: balanceScale }, { translateX: balanceTranslateX }] },
                                ]}
                            >
                                <HStack alignItems="flex-end">
                                    <Animated.Text numberOfLines={1} style={[styles.balanceText]}>
                                        <Text style={{ fontSize: 32 }}>
                                            {formattedBalance[0] ? formattedBalance[0] : '0'}
                                        </Text>
                                        <Text style={{ fontSize: 18 }}>
                                            .{formattedBalance[1] ? formattedBalance[1] : '00'}
                                        </Text>
                                    </Animated.Text>
                                    <Coin tintColor="#614384" />
                                </HStack>
                            </Animated.View>
                            <HStack
                                justifyContent="flex-start"
                                alignItems="flex-end"
                                style={styles.lockedBalanceContainer}
                            >
                                <Ionicon name="lock-closed" size={15} color="#ff3d00" style={{ marginRight: 5 }} />
                                <Text style={styles.lockedBalanceText}>
                                    {(wallet!.balance.locked / 10 ** 8).toFixed(4)}
                                </Text>
                                <Coin size="sm" tintColor="#ff3d00" />
                            </HStack>
                        </Animated.View>
                        <Animated.View
                            style={[
                                styles.optionsContainer,
                                {
                                    opacity: Animated.subtract(1, scrollOpacity),
                                    transform: [{ translateY: scrollTranslateY }],
                                },
                            ]}
                        >
                            <IconButton
                                backgroundColor="transparent"
                                onPress={() => navigation.navigate('Deposit')}
                                mr={5}
                            >
                                <Ionicon name="chevron-down" size={25} color="white" />
                            </IconButton>
                            <IconButton
                                backgroundColor="transparent"
                                onPress={() =>
                                    navigation.navigate('Withdraw', {
                                        address: '',
                                        timelock: undefined,
                                        amount: '',
                                    })
                                }
                                mr={5}
                            >
                                <Ionicon name="chevron-up" size={25} color="white" />
                            </IconButton>
                            <IconButton backgroundColor="transparent" onPress={() => navigation.navigate('Settings')}>
                                <OcticonsIcon name="settings" size={20} color="white" />
                            </IconButton>
                        </Animated.View>
                    </HStack>
                    <Animated.View
                        style={[
                            styles.optionsContainer,
                            {
                                opacity: scrollOpacity,
                                transform: [{ translateY: scrollTranslateY }, { scale: scrollScale }],
                            },
                        ]}
                    >
                        <IconButton
                            title={t('qrScanner')}
                            onPress={() => navigation.navigate('QRCodeScanner')}
                            backgroundColor="#614384"
                            color="#614384"
                            mx={20}
                        >
                            <Ionicon name="qr-code-outline" size={20} color="white" />
                        </IconButton>
                        <IconButton
                            title={t('deposit')}
                            onPress={() => navigation.navigate('Deposit')}
                            backgroundColor="#614384"
                            color="#614384"
                            mx={20}
                        >
                            <Ionicon name="chevron-down" size={30} color="white" />
                        </IconButton>
                        <IconButton
                            title={t('withdraw')}
                            onPress={() =>
                                navigation.navigate('Withdraw', {
                                    address: '',
                                    timelock: undefined,
                                    amount: '',
                                })
                            }
                            backgroundColor="#614384"
                            color="#614384"
                            mx={20}
                        >
                            <Ionicon name="chevron-up" size={30} color="white" />
                        </IconButton>
                        <IconButton
                           title={t('tokens')}
                           onPress={() => navigation.navigate('Tokens')}
                           backgroundColor="#614384"
                           color="#614384"
                           mx={20}
                       >
                           <Ionicon name="md-wallet" size={20} color="white" />
                       </IconButton>
                    </Animated.View>
                </Animated.View>
                <MaskedView
                    style={{ flex: 1 }}
                    maskElement={
                        <HStack flex={1}>
                            <Animated.View
                                style={[styles.historyMaskContainer, { transform: [{ translateY: headerTranslateY }] }]}
                            />
                        </HStack>
                    }
                >
                    <Animated.FlatList
                        ref={historyScroll}
                        style={{ backgroundColor: 'white' }}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={Platform.OS === 'android' && { paddingTop: HEADER_MAX_HEIGHT }}
                        contentInset={{ top: HEADER_MAX_HEIGHT }}
                        contentOffset={{ x: 0, y: -HEADER_MAX_HEIGHT }}
                        refreshControl={
                            <RefreshControl
                                progressViewOffset={HEADER_MAX_HEIGHT}
                                refreshing={statuses!.history === 'syncing'}
                                onRefresh={updateWalletInfo}
                                tintColor="black"
                            />
                        }
                        scrollEventThrottle={16}
                        onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: scrollY.current } } }], {
                            useNativeDriver: true,
                        })}
                        onScrollEndDrag={({ nativeEvent: { contentOffset } }) => stickTo(contentOffset)}
                        data={sortedTransactions}
                        keyExtractor={(item) => item.hash}
                        renderItem={({ item }) => (
                            <TransactionItem
                                onPress={() => navigation.navigate('TransactionDetails', { transaction: item })}
                                transaction={item}
                            />
                        )}
                    />
                    {wallet!.transactions.length === 0 && (
                        <VStack
                            style={{ position: 'absolute', left: 0, right: 0, marginTop: HEADER_MAX_HEIGHT + 100 }}
                            flex={1}
                            justifyContent="flex-start"
                        >
                            <Image
                                resizeMode="contain"
                                style={styles.noHistoryImage}
                                source={require('../../assets/no-history.png')}
                            />
                            <Text style={styles.noHistoryText}>{t('noHistory')}</Text>
                        </VStack>
                    )}
                </MaskedView>
            </View>
        </LinearGradient>
    );
};

export default History;
