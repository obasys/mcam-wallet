import React from 'react';
import { StyleSheet, Text, TouchableHighlight, TouchableHighlightProps, View } from 'react-native';
import { Avatar, HStack } from '../../../components';
import { Token } from '../../../types';

const styles = StyleSheet.create({
    container: {
        height: 80,
        padding: 20,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: '#EEEEEE',
        backgroundColor: 'white',
    },
    contentContainer: {
        marginLeft: 10,
    },
    topContentContainer: {
        marginBottom: 5,
    },
    titleText: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    balanceText: {
        fontSize: 12,
        opacity: 0.5,
    },
    favoriteContainer: {
        marginLeft: 10,
    },
});

interface TokenItemProps extends TouchableHighlightProps {
    token: Token;
}

const TokenItem: React.FC<TokenItemProps> = ({ style, token, ...props }: TokenItemProps) => {
    const formattedBalance = (token.balance / 10 ** 4).toFixed(token.units).toString();

    return (
        <TouchableHighlight {...props}>
            <View style={[styles.container, style]}>
                <Avatar title={token.tokenName} backgroundColor="black" color="white" />
                <View style={styles.contentContainer}>
                    <HStack justifyContent="flex-start" style={styles.topContentContainer}>
                        <Text style={styles.titleText}>{token.tokenName}</Text>
                    </HStack>
                    <Text style={styles.balanceText}>
                        {formattedBalance} {token.tokenName}
                    </Text>
                </View>
            </View>
        </TouchableHighlight>
    );
};

export default TokenItem;
