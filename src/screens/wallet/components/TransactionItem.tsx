import React from 'react';
import { StyleSheet, Text, TouchableOpacity, TouchableOpacityProps, View } from 'react-native';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import Ionicon from 'react-native-vector-icons/Ionicons';
import { useSelector } from 'react-redux';
import moment from 'moment';
import { Coin, HStack, VStack } from '../../../components';
import { AddressBook, RootState, Transaction } from '../../../types';

const styles = StyleSheet.create({
    container: {
        padding: 20,
        borderBottomWidth: 1,
        borderColor: '#ECECEC',
    },
    iconContainer: {
        padding: 5,
        marginRight: 15,
        backgroundColor: '#EDEDED',
        borderRadius: 40,
        width: 40,
        height: 40,
    },
    fromContainer: {
        width: '60%',
    },
    fromText: {
        fontSize: 16,
    },
    amountText: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    dateText: {
        fontSize: 12,
        color: '#7D7D7D',
    },
});

interface TransactionItemProps extends TouchableOpacityProps {
    transaction: Transaction;
}

const TransactionItem: React.FC<TransactionItemProps> = ({ transaction, style, ...props }: TransactionItemProps) => {
    const appReducer = useSelector((state: RootState) => state.app);

    const getTitleFromAdressBook = () => {
        const addressItem = appReducer.addressBook.find((addressItemToFind: AddressBook) =>
            transaction.type === 'sent'
                ? addressItemToFind.address === transaction.to
                : addressItemToFind.address === transaction.from,
        );
        return addressItem ? addressItem.title : transaction.type === 'sent' ? transaction.to : transaction.from;
    };

    return (
        <TouchableOpacity {...props}>
            <HStack justifyContent="space-between" style={styles.container}>
                <HStack flex={1} justifyContent="flex-start">
                    <HStack style={styles.iconContainer}>
                        {transaction.confirmations === 0 && (
                            <EntypoIcon name="dots-three-horizontal" size={20} color="black" />
                        )}
                        {transaction.type === 'sent' && transaction.confirmations !== 0 && (
                            <Ionicon name="arrow-up-outline" size={20} color="black" />
                        )}
                        {transaction.type === 'received' && transaction.confirmations !== 0 && (
                            <Ionicon name="arrow-down" size={20} color="black" />
                        )}
                    </HStack>
                    <View style={styles.fromContainer}>
                        <Text
                            numberOfLines={1}
                            ellipsizeMode="middle"
                            style={[styles.fromText, { opacity: transaction.confirmations === 0 ? 0.5 : 1 }]}
                        >
                            {getTitleFromAdressBook()}
                        </Text>
                    </View>
                </HStack>
                <VStack alignItems="flex-end">
                    <HStack alignItems="flex-start" justifyContent="flex-end">
                        <Text style={[styles.amountText, { opacity: transaction.confirmations === 0 ? 0.5 : 1 }]}>
                            {transaction.type === 'sent' ? '-' : '+'}
                            {transaction.tokens.length
                                ? transaction.tokens[0].amount.toFixed(2)
                                : (transaction.amount / 10 ** 8).toFixed(2)}{' '}
                            {transaction.tokens.length ? transaction.tokens[0].name : null}
                        </Text>
                        {transaction.tokens.length === 0 && <Coin size="md" />}
                    </HStack>
                    <Text style={[styles.dateText, { opacity: transaction.confirmations === 0 ? 0.5 : 1 }]}>
                        {moment(transaction.time * 1000).format('D MMM YYYY')}
                    </Text>
                </VStack>
            </HStack>
        </TouchableOpacity>
    );
};

export default TransactionItem;
