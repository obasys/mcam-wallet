import React, { useContext } from 'react';
import { FlatList, Image, SafeAreaView, StyleSheet, Text } from 'react-native';
import { useTranslation } from 'react-i18next';
import { FocusAwareStatusBar, VStack } from '../../components';
import { TokenItem } from './components';
import { WalletContext } from '../../providers';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    alignment: {
        paddingHorizontal: 20,
        alignItems: 'center',
        flex: 1,
    },
    leftAction: {
        flex: 1,
        backgroundColor: '#6D7278',
        justifyContent: 'center',
    },
    rightAction: {
        alignItems: 'flex-end',
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'red',
    },
    actionText: {
        color: 'white',
        fontSize: 16,
        backgroundColor: 'transparent',
        padding: 20,
    },
    noTokensImage: {
        height: 200,
    },
    noTokensText: {
        fontSize: 14,
        color: '#614384',
        textAlign: 'center',
    },
});

interface TokensProps {
    navigation: any;
    route: any;
}

const Tokens: React.FC<TokensProps> = ({ navigation, route }: TokensProps) => {
    const { t } = useTranslation('tokens');
    const { wallet } = useContext(WalletContext);

    return (
        <SafeAreaView style={styles.container}>
            <FocusAwareStatusBar barStyle="dark-content" backgroundColor="white" />
            {wallet!.tokens.length === 0 && (
                <VStack style={styles.alignment}>
                    <Image
                        resizeMode="contain"
                        style={styles.noTokensImage}
                        source={require('../../assets/no-history.png')}
                    />
                    <Text style={styles.noTokensText}>{t('noTokens')}</Text>
                </VStack>
            )}
            {wallet!.tokens.length > 0 && (
                <FlatList
                    data={wallet!.tokens}
                    keyExtractor={(item) => item.tokenName}
                    renderItem={({ item, index }) => <TokenItem token={item} />}
                />
            )}
        </SafeAreaView>
    );
};

export default Tokens;
