// onboarding screens
import Welcome from './onboarding/Welcome';
import RecoveryPhrase from './onboarding/RecoveryPhrase';
import Protect from './onboarding/Protect';
import Legal from './onboarding/Legal';
import Finished from './onboarding/Finished';
import RecoveryTips from './onboarding/RecoveryTips';
import GenerateWallet from './onboarding/GenerateWallet';

// password screens
import Password from './password/Password';
import ChangePasswordMethod from './password/ChangePasswordMethod';

// wallet screens
import History from './wallet/History';
import Deposit from './wallet/Deposit';
import Withdraw from './wallet/Withdraw';
import Settings from './wallet/Settings';
import TransactionDetails from './wallet/TransactionDetails';
import SendTransaction from './wallet/SendTransaction';
import Tokens from './wallet/Tokens';

// address book screens
import AddressBook from './addressBook/AddressBook';
import ManageAddressBookItem from './addressBook/ManageAddressBookItem';

// home screens
import GlobalSettings from './home/GlobalSettings';
import Home from './home/Home';
import Language from './home/Language';
import DeleteWallet from './home/DeleteWallet';
import Networks from './home/Networks';
import ManageNetwork from './home/ManageNetwork';
import ChooseWallet from './home/ChooseWallet';
import DepositRequest from './home/DepositRequest';

// common screens
import QRCodeScanner from './etc/QRCodeScanner';
import Splash from './etc/Splash';
import FactoryReset from './etc/FactoryReset';
import QRCodeRequest from './etc/QRCodeRequest';

export {
    // onboarding
    Welcome,
    RecoveryPhrase,
    Protect,
    Legal,
    Finished,
    RecoveryTips,
    GenerateWallet,
    // password
    Password,
    ChangePasswordMethod,
    // wallet
    History,
    Deposit,
    Withdraw,
    Settings,
    TransactionDetails,
    SendTransaction,
    Tokens,
    // address book
    AddressBook,
    ManageAddressBookItem,
    // home
    GlobalSettings,
    Home,
    Language,
    DeleteWallet,
    Networks,
    ManageNetwork,
    ChooseWallet,
    QRCodeRequest,
    DepositRequest,
    // common
    QRCodeScanner,
    Splash,
    FactoryReset,
};
