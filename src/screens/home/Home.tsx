import React, { useContext, useEffect, useState } from 'react';
import { Image, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import Ionicon from 'react-native-vector-icons/Ionicons';
import { CommonActions } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { showMessage } from 'react-native-flash-message';
import { Coin, FocusAwareStatusBar, HStack, IconButton } from '../../components';
import { PasswordContext, SocketContext, WalletContext } from '../../providers';
import { AddWalletItem, FavoriteItem, WalletItem } from './components';
import { AddressBook, RootState, Wallet } from '../../types';
import { setTokens } from '../../redux/actions/appActions';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    titleText: {
        fontSize: 32,
        fontWeight: 'bold',
        color: '#614384',
    },
    caption: {
        color: '#614384',
        fontSize: 12,
        textAlign: 'center'
    },
    lockedBalanceContainer: {
        opacity: 0.5,
    },
    lockedBalanceText: {
        fontSize: 12,
        color: '#ff3d00',
    },
    balanceText: {
        fontWeight: 'bold',
        color: '#614384',
        textAlign: 'left',
    },
    optionsContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30,
    },
    logoImage: {
        height: 32,
        width: 32,
        tintColor: '#614384',
        marginTop: 20,
    },
    balanceContainer: {
        marginTop: 30,
    },
    headerContainer: {
        marginTop: 30,
    },
    walletsContainer: {
        marginTop: 30,
        marginBottom: 20,
        flexGrow: 0,
    },
    captionContainer: {
        marginTop: 30,
        marginBottom: 10,
    },
    favoritesContainer: {
        flexGrow: 0,
        marginBottom: 30,
    },
});

interface HomeProps {
    navigation: any;
    route: any;
}

const Home: React.FC<HomeProps> = ({ navigation, route }: HomeProps) => {
    const { t } = useTranslation('home');
    const dispatch = useDispatch();
    const defaultParams = { address: '', amount: '', timelock: undefined, fee: '0.0001', token: undefined };
    const params = route.params ? { ...defaultParams, ...route.params } : defaultParams;
    const [isRequestOpened, setIsRequestOpened] = useState(false);
    const appReducer = useSelector((state: RootState) => state.app);
    const { socketRequest } = useContext(SocketContext);
    const { setUuid, uuid } = useContext(WalletContext);
    const { unlockedPassword } = useContext(PasswordContext);
    const totalAmount =
        appReducer.wallets.length > 0
            ? appReducer.wallets.map((wallet: Wallet) => wallet.balance.confirmed).reduce((x, y) => x + y)
            : 0;
    const totaLockedAmount =
        appReducer.wallets.length > 0
            ? appReducer.wallets.map((wallet: Wallet) => wallet.balance.locked).reduce((x, y) => x + y)
            : 0;
    const formattedTotalAmount = (totalAmount / 10 ** 8).toFixed(2).toString().split('.');
    const sortedWallets = [...appReducer.wallets].sort(function (a, b) {
        const keyA = a.balance.confirmed;
        const keyB = b.balance.confirmed;

        if (keyA > keyB) return -1;
        if (keyA < keyB) return 1;
        return 0;
    });
    const favorites = appReducer.addressBook.filter((addressBookItem: AddressBook) => addressBookItem.favorite);
    const [deepLinkedUrl, setDeepLinkUrl] = useState('');

    const openWallet = (openUuid: string) => {
        setUuid!(openUuid);
        navigation.navigate('WalletStack');
    };

    const openWalletSettings = (openUuid: string) => {
        setUuid!(openUuid);
        navigation.navigate('WalletStack', { screen: 'Settings' });
    };

    const openWalletDeposit = (openUuid: string) => {
        setUuid!(openUuid);
        navigation.navigate('WalletStack', { screen: 'Deposit' });
    };

    const openWalletWithdraw = (openUuid: string) => {
        setUuid!(openUuid);
        navigation.navigate('WalletStack', { screen: 'Withdraw' });
    };

    useEffect(() => {
        if (appReducer.wallets.length === 0) {
            navigation.dispatch(
                CommonActions.reset({
                    index: 0,
                    routes: [
                        {
                            name: 'OnboardingStack',
                        },
                    ],
                }),
            );
        } else if (params.address !== '' && params.amount !== '') {
            if (unlockedPassword) {
                if (params.token !== undefined && params.token !== '') {
                    if (
                        appReducer.wallets.find(
                            (wallet) => wallet.tokens.find((token) => token.tokenName === params.token) !== undefined,
                        ) === undefined
                    ) {
                        showMessage({
                            message: t('alerts.noTokensFound.message'),
                            description: t('alerts.noTokensFound.description'),
                            type: 'danger',
                        });
                    } else {
                        navigation.navigate('ChooseWallet', params);
                    }
                } else {
                    navigation.navigate('ChooseWallet', params);
                }
            } else {
                navigation.replace('PasswordStack', {
                    screen: 'Password',
                    params: { type: 'unlock', next: { stack: 'HomeStack', screen: 'Home', params } },
                });
            }
        }
    }, [route.params]);

    useEffect(() => {
        socketRequest!('general.tokens').then((tokens) => {
            dispatch(setTokens(Object.keys(tokens).map((tokenName) => tokens[tokenName])));
        });
    }, []);

    return (
        <LinearGradient colors={['#FFF', '#E2DEE7']} useAngle angle={180} style={{ flex: 1 }}>
            <SafeAreaView style={styles.container}>
                <FocusAwareStatusBar barStyle="dark-content" />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.alignment}>
                        <HStack alignItems="flex-end" justifyContent="space-between">
                            <Image source={require('../../assets/logo.png')} style={styles.logoImage} />
                            <Ionicon name="cog" size={32} color="#614384" onPress={() => navigation.navigate('GlobalSettings')} />
                        </HStack>
                        <View style={[styles.optionsContainer]}>
                            <IconButton
                                title={t('addressBook')}
                                onPress={() => navigation.navigate('AddressBookStack', { screen: 'AddressBook' })}
                                backgroundColor="transparent"
                                color="#614384"
                                mx={10}
                            >
                                <Ionicon name="bookmark" size={25} color="#614384" />
                            </IconButton>
                            <IconButton
                                title={t('qrScanner')}
                                onPress={() => navigation.navigate('QRCodeScanner')}
                                backgroundColor="transparent"
                                color="#614384"
                                mx={10}
                            >
                                <Ionicon name="qr-code-outline" size={25} color="#614384" />
                            </IconButton>
                        </View>
                        <View style={styles.balanceContainer}>
                            <Text style={styles.caption}>{t('totalAmount')}</Text>
                            <HStack alignItems="flex-end" justifyContent="center">
                                <Text style={styles.balanceText}>
                                    <Text style={{ fontSize: 32 }}>
                                        {formattedTotalAmount[0] ? formattedTotalAmount[0] : '0'}
                                    </Text>
                                    <Text style={{ fontSize: 18 }}>
                                        .{formattedTotalAmount[1] ? formattedTotalAmount[1] : '00'}
                                    </Text>
                                </Text>
                                <Coin tintColor="#614384" />
                            </HStack>
                            <HStack
                                justifyContent="center"
                                alignItems="flex-end"
                                style={styles.lockedBalanceContainer}
                            >
                                <Ionicon name="lock-closed" size={15} color="#ff3d00" style={{ marginRight: 5 }} />
                                <Text style={styles.lockedBalanceText}>
                                    {(totaLockedAmount / Math.pow(10, 8)).toFixed(4)}
                                </Text>
                                <Coin tintColor="#ff3d00" size="sm" />
                            </HStack>
                        </View>
                    </View>
                    <ScrollView showsHorizontalScrollIndicator={false} style={styles.walletsContainer}>
                        {sortedWallets.map((wallet: Wallet) => (
                            <WalletItem
                                wallet={wallet}
                                active={wallet.uuid === uuid}
                                key={wallet.createdAt}
                                onPress={() => openWallet(wallet.uuid)}
                                onDeposit={() => openWalletDeposit(wallet.uuid)}
                                onWithdraw={() => openWalletWithdraw(wallet.uuid)}
                                onSettings={() => openWalletSettings(wallet.uuid)}
                            />
                        ))}
                        <AddWalletItem onPress={() => navigation.navigate('OnboardingStack', { screen: 'Welcome' })} />
                    </ScrollView>
                    {favorites.length > 0 && (
                        <View>
                            <View style={[styles.alignment, styles.captionContainer]}>
                                <Text style={styles.caption}>{t('Favorite Addresses')}</Text>
                            </View>
                            <ScrollView
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                style={styles.favoritesContainer}
                            >
                                {favorites.map((addressBookItem: AddressBook) => (
                                    <FavoriteItem
                                        disabled={!uuid}
                                        key={addressBookItem.address}
                                        addressBookItem={addressBookItem}
                                        onPress={() =>
                                            navigation.navigate('WalletStack', {
                                                screen: 'Withdraw',
                                                params: { address: addressBookItem.address },
                                            })
                                        }
                                    />
                                ))}
                            </ScrollView>
                        </View>
                    )}
                </ScrollView>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default Home;
