import React, { useState } from 'react';
import { FlatList, SafeAreaView, StyleSheet } from 'react-native';
import { useTranslation } from 'react-i18next';
import IoniconsIcon from 'react-native-vector-icons/Ionicons';
import { FocusAwareStatusBar, TableItem } from '../../components';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    alignment: {
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
        flex: 1,
    },
});

interface LanguageProps {
    navigation: any;
}

const DATA = [
    { title: { en: 'English', original: 'English' }, code: 'en' },
    { title: { en: 'Korean', original: '한국어' }, code: 'ko' },
    { title: { en: 'Chinese', original: '中文' }, code: 'zh' },
];

const Language: React.FC<LanguageProps> = ({ navigation }: LanguageProps) => {
    const { t, i18n } = useTranslation();
    const [language, setLanguage] = useState('');
    const [languages, setLanguages] = useState(DATA);

    return (
        <SafeAreaView style={styles.container}>
            <FocusAwareStatusBar barStyle="dark-content" backgroundColor="white" />
            <FlatList
                data={languages}
                keyExtractor={(item) => item.code}
                renderItem={({ item }) => (
                    <TableItem
                        bottomDivider
                        title={item.title.en}
                        subtitle={item.title.original}
                        rightContent={
                            i18n.language === item.code && (
                                <IoniconsIcon name="checkmark-circle" size={28} color="#614384" />
                            )
                        }
                        color="#614384"
                        onPress={() => i18n.changeLanguage(item.code)}
                    />
                )}
            />
        </SafeAreaView>
    );
};


export default Language;
