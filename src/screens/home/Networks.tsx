import React, { useContext, useRef } from 'react';
import { Alert, Animated, FlatList, Image, SafeAreaView, StyleSheet, Text } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import IoniconsIcon from 'react-native-vector-icons/Ionicons';
import { showMessage } from 'react-native-flash-message';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import { RectButton } from 'react-native-gesture-handler';
import { FocusAwareStatusBar, TableItem, VStack } from '../../components';
import { deleteNetwork as deleteNetworkItem, setNetwork, unsetNetwork } from '../../redux/actions/appActions';
import { WalletContext } from '../../providers';
import { Network, RootState } from '../../types';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    noNetworksImage: {
        height: 200,
    },
    noNetworksText: {
        fontSize: 14,
        color: '#614384',
        textAlign: 'center',
    },
    alignment: {
        paddingHorizontal: 20,
        alignItems: 'center',
        flex: 1,
    },
    leftAction: {
        flex: 1,
        backgroundColor: '#6D7278',
        justifyContent: 'center',
    },
    rightAction: {
        alignItems: 'flex-end',
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'red',
    },
    actionText: {
        color: 'white',
        fontSize: 16,
        backgroundColor: 'transparent',
        padding: 20,
    },
});

interface NetworksProps {
    navigation: any;
}

const Networks: React.FC<NetworksProps> = ({ navigation }: NetworksProps) => {
    const { t, i18n } = useTranslation('networks');
    const { setUuid } = useContext(WalletContext);
    const appReducer = useSelector((state: RootState) => state.app);
    const dispatch = useDispatch();
    const swipableRefs = useRef<any>({});

    const changeNetwork = (network: Network) => {
        const confirmedChangeNetwork = () => {
            if (appReducer.network && appReducer.network.host === network.host) {
                dispatch(unsetNetwork());
            } else {
                dispatch(setNetwork(network));
            }

            setUuid(undefined);
            showMessage({
                message: t('alerts.networkChanged.message'),
                description: t('alerts.networkChanged.description'),
                backgroundColor: '#614384',
            });
        };

        Alert.alert(
            t('alerts.changeNetworkConfirmation.message'),
            t('alerts.changeNetworkConfirmation.description'),
            [
                {
                    text: t('alerts.changeNetworkConfirmation.cancel'),
                    onPress: () => null,
                    style: 'cancel',
                },
                {
                    text: t('alerts.changeNetworkConfirmation.confirm'),
                    onPress: confirmedChangeNetwork,
                    style: 'default',
                },
            ],
            { cancelable: false },
        );
    };

    const editNetwork = (network: Network) => {
        swipableRefs.current[network.host].close();
        navigation.navigate('ManageNetwork', { ...network, type: 'edit' });
    };

    const deleteNetwork = (network: Network) => {
        dispatch(deleteNetworkItem({ host: network.host }));
    };

    const renderLeftActions = (progress: any, dragX: any, itemIndex: any) => {
        const trans = dragX.interpolate({
            inputRange: [0, 50, 100, 101],
            outputRange: [-20, 0, 0, 1],
        });
        return (
            <RectButton style={styles.leftAction}>
                <Animated.Text
                    style={[
                        styles.actionText,
                        {
                            transform: [{ translateX: trans }],
                        },
                    ]}
                >
                    {t('edit')}
                </Animated.Text>
            </RectButton>
        );
    };

    const renderRightActions = (progress: any, dragX: any, itemIndex: any) => {
        const trans = dragX.interpolate({
            inputRange: [-101, -100, -50, 0],
            outputRange: [-1, 0, 0, 20],
        });

        return (
            <RectButton style={styles.rightAction}>
                <Animated.Text
                    style={[
                        styles.actionText,
                        {
                            transform: [{ translateX: trans }],
                        },
                    ]}
                >
                    {t('delete')}
                </Animated.Text>
            </RectButton>
        );
    };

    return (
        <SafeAreaView style={styles.container}>
            <FocusAwareStatusBar barStyle="dark-content" backgroundColor="white" />
            {appReducer.networks.length === 0 && (
                <VStack style={styles.alignment}>
                    <Image
                        resizeMode="contain"
                        style={styles.noNetworksImage}
                        source={require('../../assets/no-history.png')}
                    />
                    <Text style={styles.noNetworksText}>{t('noNetworks')}</Text>
                </VStack>
            )}
            {appReducer.networks.length > 0 && (
                <FlatList
                    data={appReducer.networks}
                    keyExtractor={(item) => item.host}
                    renderItem={({ item, index }) => (
                        <Swipeable
                            ref={(el) => {
                                swipableRefs.current[item.host] = el;
                            }}
                            renderLeftActions={(progress, dragX) => renderLeftActions(progress, dragX, index)}
                            renderRightActions={(progress, dragX) => renderRightActions(progress, dragX, index)}
                            onSwipeableRightWillOpen={() => deleteNetwork(item)}
                            onSwipeableLeftWillOpen={() => editNetwork(item)}
                        >
                            <TableItem
                                bottomDivider
                                title={item.title}
                                subtitle={item.host}
                                rightContent={
                                    appReducer.network?.host === item.host && (
                                        <IoniconsIcon name="checkmark-circle" size={28} color="black" />
                                    )
                                }
                                onPress={() => changeNetwork(item)}
                            />
                        </Swipeable>
                    )}
                />
            )}
        </SafeAreaView>
    );
};

export default Networks;
