import React, { useEffect } from 'react';
import { ActivityIndicator, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import { showMessage } from 'react-native-flash-message';
import { useTranslation } from 'react-i18next';
import { CommonActions } from '@react-navigation/native';
import { FocusAwareStatusBar, VStack } from '../../components';
import { deleteWallet } from '../../redux/actions/appActions';
import { RootState } from '../../types';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginBottom: 20,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    infoContainer: {
        marginTop: 30,
    },
    titleContainer: {
        marginBottom: 15,
    },
    titleText: {
        color: 'white',
        fontSize: 21,
        fontWeight: 'bold',
    },
    subtitleText: {
        color: 'white',
        fontSize: 14,
        opacity: 0.5,
    },
});

interface DeleteWalletProps {
    navigation: any;
    route: any;
}

const DeleteWallet: React.FC<DeleteWalletProps> = ({ navigation, route }: DeleteWalletProps) => {
    const { t } = useTranslation('deleteWallet');
    const appReducer = useSelector((state: RootState) => state.app);
    const dispatch = useDispatch();
    const { uuid } = route.params ?? {};

    useEffect(() => {
        setTimeout(() => {
            if (!uuid) {
                navigation.goBack();

                showMessage({
                    message: t('alerts.notFound.message'),
                    description: t('alerts.notFound.description'),
                    type: 'danger',
                });
            }
            if (appReducer.wallets.length > 1) {
                dispatch(deleteWallet({ uuid }));

                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [{ name: 'HomeStack' }],
                    }),
                );
            } else {
                dispatch(deleteWallet({ uuid }));

                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [{ name: 'OnboardingStack' }],
                    }),
                );
            }

            showMessage({
                message: t('alerts.deleted.message'),
                description: t('alerts.deleted.description'),
                backgroundColor: '#614384',
            });
        }, 1000);
    }, []);

    return (
        <LinearGradient colors={['#FFF', '#E2DEE7']} useAngle angle={180} style={{ flex: 1 }}>
            <FocusAwareStatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.container}>
                <VStack flex={1} style={styles.alignment}>
                    <ActivityIndicator color="white" size="large" />
                    <VStack style={styles.infoContainer}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.titleText}>{t('deletingWallet')}</Text>
                        </View>
                    </VStack>
                </VStack>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default DeleteWallet;
