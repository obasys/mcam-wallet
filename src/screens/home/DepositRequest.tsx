import React, { useEffect, useState } from 'react';
import {
    FlatList,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import { useSelector } from 'react-redux';
import Clipboard from '@react-native-community/clipboard';
import { useTranslation } from 'react-i18next';
import Config from 'react-native-config';
import { Button, Coin, DismissKeyboard, FocusAwareStatusBar, HStack, Input, VStack } from '../../components';
import { AddressBook, RootState, Token } from '../../types';
import { isAddress } from '../../services/addressService';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    caption: {
        fontSize: 12,
        color: '#232E63',
    },
    inputContainer: {
        width: '100%',
        marginBottom: 15,
    },
    totalText: {
        fontSize: 21,
        fontWeight: 'bold',
    },
    balanceText: {
        fontSize: 12,
        opacity: 0.5,
    },
    totalContainer: {
        marginTop: 10,
    },
    balanceContainer: {
        marginTop: 5,
    },
});

interface DepositRequestProps {
    navigation: any;
    route: any;
}

const DepositRequest: React.FC<DepositRequestProps> = ({ navigation, route }: DepositRequestProps) => {
    const { t, i18n } = useTranslation('depositRequest');
    const appReducer = useSelector((state: RootState) => state.app);
    const defaultParams = { address: '', amount: '', token: undefined };
    const params = route.params ? { ...defaultParams, ...route.params } : defaultParams;
    const [address, setAddress] = useState<string>(params.address);
    const [amount, setAmount] = useState<string>(params.amount);
    const [token, setToken] = useState<Token | undefined>();
    const [isClipboardActive, setIsClipboardActive] = useState(false);
    const formattedTotalDepositRequest = parseFloat(
        !amount || amount === '' || amount === '.' || amount === ',' ? '0' : amount,
    )
        .toFixed(4)
        .toString();

    const checkAmount = (text: string) => {
        const regex = /^[0-9]{0,100}([.,][0-9]{0,8})?$/;
        regex.test(text) && setAmount(text.replace(',', '.'));
    };

    const getFromQRCode = () => {
        navigation.navigate('QRCodeScanner', {
            type: 'withdraw',
        });
    };

    const generateQR = () => {
        navigation.navigate('QRCodeRequest', { address, amount, token });
    };

    const getFromClipboard = () => {
        Clipboard.getString().then((string) => {
            const addresses = string.match(new RegExp(Config.ADDRESS_REGEX));

            if (addresses && addresses.length > 0) {
                setAddress(addresses[0]);
            }
        });
    };

    useEffect(() => {
        if (Clipboard.hasString()) {
            Clipboard.getString().then((string) => {
                const addresses = string.match(new RegExp(Config.ADDRESS_REGEX));

                if (addresses && addresses.length > 0) {
                    setIsClipboardActive(true);
                }
            });
        }
    }, []);

    useEffect(() => {
        if (route.params?.address) {
            setAddress(route.params.address);
        }

        if (route.params?.amount) {
            setAmount(route.params.amount);
        }
    }, [route.params]);

    return (
        <DismissKeyboard>
            <SafeAreaView style={styles.container}>
                <FocusAwareStatusBar barStyle="dark-content" backgroundColor="white" />
                <KeyboardAvoidingView
                    style={styles.container}
                    behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                    keyboardVerticalOffset={100}
                >
                    <ScrollView>
                        {'tokens' in appReducer && appReducer.tokens.length > 0 && (
                            <View style={styles.inputContainer}>
                                <View style={styles.alignment}>
                                    <Text style={styles.caption}>{t('token')}</Text>
                                </View>
                                <HStack>
                                    <FlatList
                                        style={{ flexGrow: 0, width: '100%', paddingLeft: 20 }}
                                        horizontal
                                        data={appReducer.tokens}
                                        keyExtractor={(item) => item.name}
                                        renderItem={({ item }) => (
                                            <Button
                                                title={item.name}
                                                onPress={() =>
                                                    token && token.name === item.name
                                                        ? setToken(undefined)
                                                        : setToken(item)
                                                }
                                                size="md"
                                                border={!token || token.name !== item.name}
                                                borderColor={'#ECECEC'}
                                                backgroundColor={
                                                    token && token.name === item.name ? '#614384' : 'white'
                                                }
                                                color={token && token.name === item.name ? 'white' : 'black'}
                                                mt={10}
                                                mr={10}
                                            />
                                        )}
                                    />
                                </HStack>
                            </View>
                        )}
                        <VStack style={styles.alignment} justifyContent="flex-start" alignItems="flex-start" flex={1}>
                            <View style={styles.inputContainer}>
                                <Text style={styles.caption}>{t('withdrawAddress.title')}</Text>
                                <Input
                                    placeholder={t('withdrawAddress.placeholder')}
                                    autoFocus={!address || address === ''}
                                    onChangeText={(text) => setAddress(text)}
                                    value={address}
                                />
                                <HStack justifyContent="space-between">
                                    <Button
                                        onPress={() =>
                                            navigation.navigate('AddressBookStack', {
                                                screen: 'AddressBook',
                                                params: {
                                                    type: 'choose-address',
                                                    callback: (addressBookItem: AddressBook) =>
                                                        setAddress(addressBookItem.address),
                                                },
                                            })
                                        }
                                        title={t('addressBook')}
                                        size="md"
                                        border
                                        borderColor="#ECECEC"
                                        backgroundColor="white"
                                        color="black"
                                        mt={10}
                                        mr={10}
                                        flex={1}
                                    />
                                    <Button
                                        onPress={getFromClipboard}
                                        disabled={!isClipboardActive}
                                        title={t('clipboard')}
                                        size="md"
                                        border
                                        borderColor="#ECECEC"
                                        backgroundColor="white"
                                        color="black"
                                        mt={10}
                                        flex={1}
                                    />
                                </HStack>
                            </View>
                            <View style={styles.inputContainer}>
                                <Text style={styles.caption}>{t('amount.title')}</Text>
                                <Input
                                    placeholder={t('amount.placeholder')}
                                    autoFocus={address !== undefined && address !== ''}
                                    rightContent={
                                        <View style={{ marginLeft: 5 }}>
                                            <Text style={{ fontWeight: 'bold' }}>
                                                {token ? token.name : Config.COIN_NAME}
                                            </Text>
                                        </View>
                                    }
                                    onChangeText={(text) => checkAmount(text)}
                                    value={amount}
                                    keyboardType="numeric"
                                />
                            </View>
                            <View style={styles.inputContainer}>
                                <Text style={styles.caption}>{t('totalDepositRequest')}</Text>
                                <HStack style={styles.totalContainer} justifyContent="flex-start" alignItems="flex-end">
                                    <Text style={styles.totalText}>
                                        {formattedTotalDepositRequest} {token ? token.name : null}
                                    </Text>
                                    {!token && <Coin />}
                                </HStack>
                            </View>
                        </VStack>
                    </ScrollView>
                    <HStack style={styles.alignment}>
                        <Button
                            title={t('confirmButton')}
                            flex={1}
                            disabled={!isAddress(address) || !amount || amount === ''}
                            onPress={generateQR}
                            mb={20}
                        />
                    </HStack>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </DismissKeyboard>
    );
};

export default DepositRequest;
