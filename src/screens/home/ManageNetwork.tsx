import React, { useEffect, useState } from 'react';
import { KeyboardAvoidingView, Platform, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Button, DismissKeyboard, FocusAwareStatusBar, HStack, Input, VStack } from '../../components';
import { saveNetwork as saveNetworkItem } from '../../redux/actions/appActions';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    caption: {
        fontSize: 12,
        color: '#232E63',
    },
    inputContainer: {
        width: '100%',
        marginBottom: 15,
    },
});

interface ManageNetworkProps {
    navigation: any;
    route: any;
}

const ManageNetwork: React.FC<ManageNetworkProps> = ({ navigation, route }: ManageNetworkProps) => {
    const { t } = useTranslation('manageNetwork');
    const { type, ...params } = route.params ?? { address: '', title: '', favorite: false };
    const dispatch = useDispatch();
    const [host, setHost] = useState<string>(params.host);
    const [title, setTitle] = useState<string>(params.title);
    const [scriptHash, setScriptHash] = useState(params.scriptHash);
    const [publicKeyHash, setPublicKeyHash] = useState(params.publicKeyHash);
    const [wif, setWif] = useState(params.wif);

    const saveNetwork = () => {
        dispatch(saveNetworkItem({ host, title, scriptHash, publicKeyHash, wif }));
        navigation.goBack();
    };

    const isDisabled = () => {
        return (
            !title ||
            title.length < 4 ||
            !host ||
            host === '' ||
            !scriptHash ||
            scriptHash === '' ||
            !publicKeyHash ||
            publicKeyHash === '' ||
            !wif ||
            wif === ''
        );
    };

    useEffect(() => {
        navigation.setOptions({
            title: type === 'edit' ? t('editNetwork') : t('addNetwork'),
        });
    }, []);

    useEffect(() => {
        if (route.params?.host) {
            setHost(route.params.host);
        }

        if (route.params?.title) {
            setTitle(route.params.title);
        }

        if (route.params?.scriptHash) {
            setScriptHash(route.params.scriptHash);
        }

        if (route.params?.publicKeyHash) {
            setPublicKeyHash(route.params.publicKeyHash);
        }

        if (route.params?.wif) {
            setWif(route.params.wif);
        }
    }, [route.params]);

    return (
        <DismissKeyboard>
            <SafeAreaView style={styles.container}>
                <FocusAwareStatusBar barStyle="dark-content" backgroundColor="white" />
                <KeyboardAvoidingView
                    style={styles.container}
                    behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                    keyboardVerticalOffset={100}
                >
                    <ScrollView>
                        <VStack style={styles.alignment} flex={1} alignItems="flex-start" justifyContent="flex-start">
                            <View style={styles.inputContainer}>
                                <Text style={styles.caption}>{t('title.title')}</Text>
                                <Input
                                    placeholder={t('title.placeholder')}
                                    autoFocus
                                    onChangeText={(text) => setTitle(text)}
                                    value={title}
                                />
                            </View>
                            <View style={styles.inputContainer}>
                                <Text style={styles.caption}>{t('host.title')}</Text>
                                <Input
                                    placeholder={t('host.placeholder')}
                                    onChangeText={(text) => setHost(text)}
                                    value={host}
                                />
                            </View>

                            <View style={styles.inputContainer}>
                                <Text style={styles.caption}>{t('scriptHash.title')}</Text>
                                <Input
                                    placeholder={t('scriptHash.placeholder')}
                                    onChangeText={(text) => setScriptHash(text)}
                                    value={scriptHash}
                                />
                            </View>
                            <View style={styles.inputContainer}>
                                <Text style={styles.caption}>{t('publicKeyHash.title')}</Text>
                                <Input
                                    placeholder={t('publicKeyHash.placeholder')}
                                    onChangeText={(text) => setPublicKeyHash(text)}
                                    value={publicKeyHash}
                                />
                            </View>
                            <View style={styles.inputContainer}>
                                <Text style={styles.caption}>{t('wif.title')}</Text>
                                <Input
                                    placeholder={t('wif.placeholder')}
                                    onChangeText={(text) => setWif(text)}
                                    value={wif}
                                />
                            </View>
                        </VStack>
                    </ScrollView>
                    <HStack style={styles.alignment}>
                        <Button
                            title={type === 'edit' ? t('confirmButton.save') : t('confirmButton.add')}
                            onPress={saveNetwork}
                            flex={1}
                            disabled={isDisabled()}
                            mb={20}
                        />
                    </HStack>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </DismissKeyboard>
    );
};

export default ManageNetwork;
