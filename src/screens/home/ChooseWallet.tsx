import React, { useContext } from 'react';
import { FlatList, Image, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import { useTranslation } from 'react-i18next';
import { FocusAwareStatusBar, HStack, VStack } from '../../components';
import { WalletListItem } from './components';
import { WalletContext } from '../../providers';
import { RootState } from '../../types';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    dividerContainer: {
        padding: 20,
    },
    divider: {
        width: 50,
        height: 6,
        borderRadius: 10,
        backgroundColor: '#232E63',
    },
    titleContainer: {
        marginBottom: 15,
    },
    titleText: {
        fontSize: 18,
        color: '#614384',
        fontWeight: 'bold',
    },
    descriptionText: {
        fontSize: 14,
        color: '#614384',
        opacity: 0.5,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    noWalletsImage: {
        height: 200,
    },
    noWalletsText: {
        fontSize: 14,
        color: '#614384',
        textAlign: 'center',
    },
});

interface ChooseWalletProps {
    navigation: any;
    route: any;
}

const ChooseWallet: React.FC<ChooseWalletProps> = ({ navigation, route }: ChooseWalletProps) => {
    const { t } = useTranslation('chooseWallet');
    const appReducer = useSelector((state: RootState) => state.app);
    const dispatch = useDispatch();
    const { setUuid, uuid } = useContext(WalletContext);
    const defaultParams = { address: '', amount: '', token: undefined };
    const params = route.params ? { ...defaultParams, ...route.params } : defaultParams;

    const openWithdraw = (openUuid: string) => {
        setUuid(openUuid);
        navigation.navigate('WalletStack', { screen: 'Withdraw', params });
    };

    return (
        <LinearGradient colors={['#FFF', '#E2DEE7']} useAngle angle={180} style={{ flex: 1 }}>
            <FocusAwareStatusBar barStyle="dark-content" />
            <View style={[styles.alignment, { paddingBottom: 20 }]}>
                <HStack style={styles.dividerContainer}>
                    <View style={styles.divider} />
                </HStack>
                <View style={styles.titleContainer}>
                    <Text style={styles.titleText}>{t('title')}</Text>
                </View>
                <Text style={styles.descriptionText}>{t('description')}</Text>
            </View>
            <SafeAreaView style={styles.container}>
                {appReducer.wallets.length === 0 && (
                    <VStack style={styles.alignment}>
                        <Image
                            resizeMode="contain"
                            style={styles.noWalletsImage}
                            source={require('../../assets/no-history.png')}
                        />
                        <Text style={styles.noWalletsText}>{t('noWallets')}</Text>
                    </VStack>
                )}
                {appReducer.wallets.length > 0 && (
                    <FlatList
                        data={appReducer.wallets}
                        keyExtractor={(item) => item.uuid}
                        renderItem={({ item, index }) => <WalletListItem wallet={item} onNavigate={openWithdraw} />}
                    />
                )}
            </SafeAreaView>
        </LinearGradient>
    );
};

export default ChooseWallet;
