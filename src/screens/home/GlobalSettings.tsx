import React, { useContext, useEffect, useState } from 'react';
import { Alert, SafeAreaView, ScrollView, StyleSheet, Switch, Text, View, Image } from 'react-native';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { showMessage } from 'react-native-flash-message';
import SInfo from 'react-native-sensitive-info';
import { Button, DismissKeyboard, FocusAwareStatusBar, Input, TableItem } from '../../components';
import { PasswordContext } from '../../providers';
import { changeName, clearReducerKey, setPassword } from '../../redux/actions/appActions';
import { RootState } from '../../types';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    groupContainer: {
        marginBottom: 30,
    },
    caption: {
        fontSize: 12,
        color: '#614384',
    },
    inputContainer: {
        marginBottom: 30,
    },
});

interface GlobalSettingsProps {
    navigation: any;
}

const GlobalSettings: React.FC<GlobalSettingsProps> = ({ navigation }: GlobalSettingsProps) => {
    const { t } = useTranslation('globalSettings');
    const { unlockedPassword } = useContext(PasswordContext);
    const dispatch = useDispatch();
    const appReducer = useSelector((state: RootState) => state.app);
    const [name, setName] = useState<string>(appReducer.name);
    const [sensorAvailability, setSensorAvailability] = useState(true);

    const handleEnterName = () => {
        dispatch(changeName({ name }));
        showMessage({
            message: t('alerts.nameChanged.message'),
            description: t('alerts.nameChanged.description'),
            backgroundColor: '#614384',
        });
    };

    const setBiometic = async () => {
        if (appReducer.password.useBiometric) {
            dispatch(setPassword({ useBiometric: !appReducer.password.useBiometric }));
        } else {
            try {
                const settedItem = await SInfo.setItem('password', unlockedPassword as string, {
                    sharedPreferencesName: 'mySharedPrefs',
                    keychainService: 'myKeychain',
                    touchID: true,
                    showModal: true,
                    kSecAccessControl: 'kSecAccessControlBiometryAny',
                });

                dispatch(setPassword({ useBiometric: !appReducer.password.useBiometric }));
            } catch (e) {
                console.log(e);
            }
        }
    };

    const changePassword = () => {
        navigation.navigate('PasswordStack', {
            screen: 'Password',
            params: {
                type: 'unlock',
                goBack: true,
                next: {
                    stack: 'PasswordStack',
                    screen: 'Password',
                    params: {
                        type: 'new-password',
                        goBack: true,
                        next: {
                            stack: 'HomeStack',
                            screen: 'Home',
                        },
                    },
                },
            },
        });
    };

    const deleteAddressBook = () => {
        const confirmedDeleteAddressBook = () => {
            dispatch(clearReducerKey({ key: 'addressBook' }));
            showMessage({
                message: t('alerts.addressBookDeleted.message'),
                description: t('alerts.addressBookDeleted.description'),
                backgroundColor: '#614384',
            });
        };

        Alert.alert(
            t('alerts.addressBookDeleteConfirmation.message'),
            t('alerts.addressBookDeleteConfirmation.description'),
            [
                {
                    text: t('alerts.addressBookDeleteConfirmation.cancel'),
                    onPress: () => null,
                    style: 'cancel',
                },
                {
                    text: t('alerts.addressBookDeleteConfirmation.confirm'),
                    onPress: confirmedDeleteAddressBook,
                    style: 'default',
                },
            ],
            { cancelable: false },
        );
    };

    const factoryReset = () => {
        const confirmedFactoryReset = () => {
            navigation.navigate('PasswordStack', {
                screen: 'Password',
                params: {
                    type: 'unlock',
                    goBack: true,
                    next: {
                        stack: 'RootStack',
                        screen: 'FactoryReset',
                        params: {},
                    },
                },
            });
        };

        Alert.alert(
            t('alerts.factoryResetConfirmation.message'),
            t('alerts.factoryResetConfirmation.description'),
            [
                {
                    text: t('alerts.factoryResetConfirmation.cancel'),
                    onPress: () => null,
                    style: 'cancel',
                },
                {
                    text: t('alerts.factoryResetConfirmation.confirm'),
                    onPress: confirmedFactoryReset,
                    style: 'default',
                },
            ],
            { cancelable: false },
        );
    };

    useEffect(() => {
        SInfo.isSensorAvailable().then((sensor) => {
            if (!sensor) {
                setSensorAvailability(false);
            }
        });
    });

    return (
        <DismissKeyboard>
            <SafeAreaView style={styles.container}>
                <FocusAwareStatusBar barStyle="dark-content" backgroundColor="white" />
                <ScrollView>
                    <View style={styles.alignment}>
                        <View style={styles.inputContainer}>
                            <Text style={styles.caption}>{t('name.title')}</Text>
                            <Input
                                placeholder={t('name.placeholder')}
                                onChangeText={(text: string) => setName(text)}
                                value={name}
                                rightContent={
                                    <Button
                                        title={t('enter')}
                                        backgroundColor="transparent"
                                        color="black"
                                        onPress={handleEnterName}
                                    />
                                }
                            />
                        </View>
                    </View>
                    <View style={styles.groupContainer}>
                        <TableItem
                            title={t('changeLanguage')}
                            color="#614384"
                            icon={<Image source={require('../../assets/settings/change-language.png')} style={{ height: 30, width: 30 }} />}
                            onPress={() => navigation.navigate('Language')}
                            rightContent={<EntypoIcon name="chevron-thin-right" size={20} color="#614384" />}
                        />
                        <TableItem
                            title={t('changeNetwork')}
                            color="#614384"
                            icon={<Image source={require('../../assets/settings/change-network.png')} style={{ height: 30, width: 30 }} />}
                            onPress={() => navigation.navigate('Networks')}
                            rightContent={<EntypoIcon name="chevron-thin-right" size={20} color="#614384" />}
                        />
                        <TableItem
                            title={t('changePassword')}
                            color="#614384"
                            icon={<Image source={require('../../assets/settings/change-password.png')} style={{ height: 30, width: 30 }} />}
                            onPress={changePassword}
                            rightContent={<EntypoIcon name="chevron-thin-right" size={20} color="#614384" />}
                        />
                        <TableItem
                            title={t('useTouchID')}
                            color="#614384"
                            icon={<Image source={require('../../assets/settings/use-bio.png')} style={{ height: 30, width: 30 }} />}
                            rightContent={
                                <Switch
                                    disabled={!sensorAvailability}
                                    onValueChange={setBiometic}
                                    value={appReducer.password.useBiometric}
                                    trackColor={{ false: '#767577', true: '#614384' }}
                                />
                            }
                        />
                    </View>
                    <View style={styles.groupContainer}>
                        <TableItem
                            title={t('deleteAddressBook')}
                            color="#614384"
                            onPress={deleteAddressBook}
                            icon={<Image source={require('../../assets/settings/delete-address.png')} style={{ height: 30, width: 30 }} />}
                        />
                        <TableItem
                            title={t('factoryReset')}
                            color="#614384"
                            onPress={factoryReset}
                            icon={<Image source={require('../../assets/settings/reset-wallet.png')} style={{ height: 30, width: 30 }} />}
                        />
                    </View>
                </ScrollView>
            </SafeAreaView>
        </DismissKeyboard>
    );
};

export default GlobalSettings;
