import React from 'react';
import { StyleSheet, Text, TouchableHighlight, TouchableHighlightProps, View } from 'react-native';
import Config from 'react-native-config';
import { Avatar, HStack } from '../../../components';
import { Wallet } from '../../../types';
import { base64ToHex } from '../../../services/commonService';

const styles = StyleSheet.create({
    container: {
        height: 80,
        padding: 20,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: '#EEEEEE',
        backgroundColor: 'white',
    },
    contentContainer: {
        marginLeft: 10,
    },
    topContentContainer: {
        marginBottom: 5,
    },
    titleText: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    balanceText: {
        fontSize: 12,
        opacity: 0.5,
    },
    favoriteContainer: {
        marginLeft: 10,
    },
});

interface WalletListItemProps extends TouchableHighlightProps {
    wallet: Wallet;
    onNavigate: (uuid: string) => void;
}

const WalletListItem: React.FC<WalletListItemProps> = ({
    style,
    wallet,
    onNavigate,
    ...props
}: WalletListItemProps) => {
    const formattedBalance = (wallet.balance.confirmed / 10 ** 8).toFixed(4);

    return (
        <TouchableHighlight {...props} onPress={() => onNavigate(wallet.uuid)}>
            <View style={[styles.container, style]}>
                <Avatar
                    title={wallet.title}
                    backgroundColor={`#${base64ToHex(wallet.uuid).substring(0, 6)}`}
                    color="white"
                />
                <View style={styles.contentContainer}>
                    <HStack justifyContent="flex-start" style={styles.topContentContainer}>
                        <Text style={styles.titleText}>{wallet.title}</Text>
                    </HStack>
                    <Text style={styles.balanceText}>
                        {formattedBalance} {Config.COIN_NAME}
                    </Text>
                </View>
            </View>
        </TouchableHighlight>
    );
};

export default WalletListItem;
