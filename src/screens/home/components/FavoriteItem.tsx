import React from 'react';
import { StyleSheet, Text, TouchableOpacity, TouchableOpacityProps, View } from 'react-native';
import { Avatar } from '../../../components';
import { AddressBook } from '../../../types';
import { base64ToHex } from '../../../services/commonService';

const styles = StyleSheet.create({
    container: {
        height: 50,
        minWidth: 130,
        marginLeft: 20,
        borderRadius: 10,
        padding: 5,
        backgroundColor: 'white',
        alignItems: 'center',
        flexDirection: 'row',
    },
    titleContainer: {
        marginLeft: 5,
    },
});

interface FavoriteItemProps extends TouchableOpacityProps {
    addressBookItem: AddressBook;
}

const FavoriteItem: React.FC<FavoriteItemProps> = ({
    style,
    disabled,
    addressBookItem,
    ...props
}: FavoriteItemProps) => {
    return (
        <TouchableOpacity
            style={[styles.container, style, { opacity: disabled ? 0.5 : 1 }]}
            disabled={disabled}
            {...props}
        >
            <Avatar
                size="sm"
                title={addressBookItem.title}
                backgroundColor={`#${base64ToHex(addressBookItem.address).substring(0, 6)}`}
                color="white"
            />
            <View style={styles.titleContainer}>
                <Text>{addressBookItem.title}</Text>
            </View>
        </TouchableOpacity>
    );
};

export default FavoriteItem;
