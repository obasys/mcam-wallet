import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, TouchableOpacityProps } from 'react-native';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import { useTranslation } from 'react-i18next';
import { HStack, VStack } from '../../../components';

const styles = StyleSheet.create({
    container: {
        height: 250,
        minWidth: 250,
        padding: 15,
        marginHorizontal: 20,
        borderRadius: 10,
        backgroundColor: '#67567A',
        justifyContent: 'space-between',
    },
});

type AddWalletItemProps = TouchableOpacityProps;

const AddWalletItem: React.FC<AddWalletItemProps> = ({ style, ...props }: AddWalletItemProps) => {
    const { t } = useTranslation('home');

    return (
        <TouchableOpacity style={[styles.container, style]} {...props}>
            <HStack justifyContent="flex-start" style={{ width: '100%' }}>
                <Image
                    source={require('../../../assets/logo.png')}
                    style={{ height: 32, width: 32, tintColor: '#D8D8D8' }}
                />
            </HStack>
            <VStack flex={1}>
                <AntDesignIcon name="plus" size={80} color="#D8D8D8" />
                <Text style={{ fontSize: 12, color: '#D8D8D8' }}>{t('addNewWallet')}</Text>
            </VStack>
        </TouchableOpacity>
    );
};

export default AddWalletItem;
