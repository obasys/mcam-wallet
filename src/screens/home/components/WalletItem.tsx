import React from 'react';
import { StyleSheet, Text, TouchableOpacity, TouchableOpacityProps, View, FlatList } from 'react-native';
import OcticonsIcon from 'react-native-vector-icons/Octicons';
import Ionicon from 'react-native-vector-icons/Ionicons';
import { useTranslation } from 'react-i18next';
import { Coin, HStack, IconButton, VStack } from '../../../components';
import { Wallet } from '../../../types';

const styles = StyleSheet.create({
    container: {
        height: 260,
        marginHorizontal: 20,
        borderRadius: 10,
        backgroundColor: '#67567A',
        justifyContent: 'space-between',
        marginBottom: 10,
    },
    alignment: {
        padding: 15,
    },
    activeWalletContainer: {
        borderRadius: 10,
        height: 10,
        width: 10,
        marginRight: 10,
        backgroundColor: 'lightgreen',
    },
    lockedBalanceContainer: {
        opacity: 0.5,
    },
    lockedBalanceText: {
        fontSize: 12,
        color: 'white'
    },
    buttonsContainer: {
        marginTop: 20,
        paddingHorizontal: 20,
    },
    buttonContainer: {
        flex: 1,
        height: 35,
        marginHorizontal: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
});

interface WalletItemProps extends TouchableOpacityProps {
    wallet: Wallet;
    active: boolean;
    onDeposit?: any;
    onWithdraw?: any;
    onSettings?: any;
}

const WalletItem: React.FC<WalletItemProps> = ({
    style,
    active,
    onDeposit,
    onWithdraw,
    onSettings,
    wallet,
    ...props
}: WalletItemProps) => {
    const { t } = useTranslation('home');
    const formattedBalance = (wallet.balance.confirmed / 10 ** 8).toFixed(2).toString().split('.');

    return (
        <TouchableOpacity style={[styles.container, style]} {...props}>
            <View style={styles.alignment}>
                <HStack justifyContent="space-between">
                    <HStack flex={1} justifyContent="flex-start">
                        {active && <View style={styles.activeWalletContainer} />}
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }} numberOfLines={1}>
                            {wallet.title}
                        </Text>
                    </HStack>
                    <IconButton onPress={onSettings} backgroundColor="white" color="white" disableBackground>
                        <OcticonsIcon name="settings" size={20} color="white" />
                    </IconButton>
                </HStack>
                <VStack alignItems="center" style={{ marginTop: 15 }}>
                    <HStack alignItems="flex-end" justifyContent="flex-start">
                        <Text style={{ fontWeight: 'bold', color: 'white' }}>
                            <Text style={{ fontSize: 24 }}>{formattedBalance[0] ? formattedBalance[0] : '0'}</Text>
                            <Text style={{ fontSize: 14 }}>.{formattedBalance[1] ? formattedBalance[1] : '00'}</Text>
                        </Text>
                        <Coin tintColor='white' />
                    </HStack>
                    <HStack justifyContent="flex-start" style={styles.lockedBalanceContainer}>
                        <Ionicon name="lock-closed" size={15} color="white" style={{ marginRight: 5 }} />
                        <Text style={styles.lockedBalanceText}>{(wallet!.balance.locked / 10 ** 8).toFixed(2)}</Text>
                        <Coin size="sm" tintColor='white' />
                    </HStack>
                    <HStack justifyContent="flex-start" style={styles.buttonsContainer}>
                        <TouchableOpacity
                            onPress={onDeposit}
                            style={[styles.buttonContainer, { borderWidth: 1, borderColor: '#847893', borderRadius: 4 }]}
                        >
                            <Text style={{ textAlign: 'center', color: 'white', fontSize: 12 }}>{t('deposit')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={onWithdraw}
                            style={[styles.buttonContainer, { borderWidth: 1, borderColor: '#847893', borderRadius: 4 }]}
                        >
                            <Text style={{ textAlign: 'center', color: 'white', fontSize: 12 }}>{t('withdraw')}</Text>
                        </TouchableOpacity>
                    </HStack>
                </VStack>
                {wallet.tokens.length > 0 && <HStack justifyContent="space-between" style={{ marginTop: 15, paddingHorizontal: 20, opacity: 0.5 }}>
                    <Text style={styles.lockedBalanceText}>Token</Text>
                    <Text style={styles.lockedBalanceText}>Amount</Text>
                </HStack> }
                <FlatList
                    style={{ flexGrow: 0, width: '100%', paddingHorizontal: 20 }}
                    data={wallet!.tokens.slice(0, 3)}
                    keyExtractor={(item) => item.tokenName}
                    renderItem={({ item }) => (
                        <HStack justifyContent="space-between">
                            <Text style={styles.lockedBalanceText}>{item.tokenName}</Text>
                            <Text style={styles.lockedBalanceText}>{(item.balance / 10 ** 4).toFixed(item.units).toString()}</Text>
                        </HStack>
                    )}
                />
            </View>
        </TouchableOpacity>
    );
};

export default WalletItem;
