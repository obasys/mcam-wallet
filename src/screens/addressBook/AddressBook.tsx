import React, { useEffect, useRef } from 'react';
import { Animated, FlatList, Image, SafeAreaView, StyleSheet, Text } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import { RectButton } from 'react-native-gesture-handler';
import { useTranslation } from 'react-i18next';
import { FocusAwareStatusBar, VStack } from '../../components';
import { AddressBook as AddressBookType, RootState } from '../../types';
import { deleteAddressBookItem } from '../../redux/actions/appActions';
import { AddressBookItem } from './components';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    alignment: {
        paddingHorizontal: 20,
        alignItems: 'center',
        flex: 1,
    },
    leftAction: {
        flex: 1,
        backgroundColor: '#6D7278',
        justifyContent: 'center',
    },
    rightAction: {
        alignItems: 'flex-end',
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'red',
    },
    actionText: {
        color: 'white',
        fontSize: 16,
        backgroundColor: 'transparent',
        padding: 20,
    },
    noAddressBookImage: {
        height: 200,
    },
    noAddressBookText: {
        fontSize: 14,
        color: '#614384',
        textAlign: 'center',
    },
});

interface AddressBookProps {
    navigation: any;
    route: any;
}

const AddressBook: React.FC<AddressBookProps> = ({ navigation, route }: AddressBookProps) => {
    const dispatch = useDispatch();
    const { t } = useTranslation('addressBook');
    const { addressBookItem, type, callback } = route.params ?? {};
    const appReducer = useSelector((state: RootState) => state.app);
    const swipableRefs = useRef<any>({});

    const sortedAddresses = [...appReducer.addressBook].sort(function (a, b) {
        if (a.title > b.title) return 1;
        if (a.title < b.title) return -1;
        return 0;
    });

    const renderLeftActions = (progress: any, dragX: any, itemIndex: any) => {
        const trans = dragX.interpolate({
            inputRange: [0, 50, 100, 101],
            outputRange: [-20, 0, 0, 1],
        });
        return (
            <RectButton style={styles.leftAction}>
                <Animated.Text
                    style={[
                        styles.actionText,
                        {
                            transform: [{ translateX: trans }],
                        },
                    ]}
                >
                    {t('edit')}
                </Animated.Text>
            </RectButton>
        );
    };

    const renderRightActions = (progress: any, dragX: any, itemIndex: any) => {
        const trans = dragX.interpolate({
            inputRange: [-101, -100, -50, 0],
            outputRange: [-1, 0, 0, 20],
        });

        return (
            <RectButton style={styles.rightAction}>
                <Animated.Text
                    style={[
                        styles.actionText,
                        {
                            transform: [{ translateX: trans }],
                        },
                    ]}
                >
                    {t('delete')}
                </Animated.Text>
            </RectButton>
        );
    };

    const chooseAddress = (chosenAddressBookItem: AddressBookType) => {
        callback(chosenAddressBookItem);
        navigation.goBack();
    };

    const editAddress = (editedAddressBookItem: AddressBookType) => {
        swipableRefs.current[editedAddressBookItem.address].close();
        navigation.navigate('ManageAddressBookItem', { ...editedAddressBookItem, type: 'edit' });
    };

    const deleteAddress = (deletedAddressBookItem: AddressBookType) => {
        dispatch(deleteAddressBookItem({ address: deletedAddressBookItem.address }));
    };

    useEffect(() => {
        if (type === 'choose-address') {
            navigation.setOptions({
                headerRight: () => null,
            });
        }
    }, []);

    return (
        <SafeAreaView style={styles.container}>
            <FocusAwareStatusBar barStyle="dark-content" backgroundColor="white" />
            {appReducer.addressBook.length === 0 && (
                <VStack style={styles.alignment}>
                    <Image
                        resizeMode="contain"
                        style={styles.noAddressBookImage}
                        source={require('../../assets/no-history.png')}
                    />
                    <Text style={styles.noAddressBookText}>{t('noAddresses')}</Text>
                </VStack>
            )}
            {appReducer.addressBook.length > 0 && (
                <FlatList
                    data={sortedAddresses}
                    keyExtractor={(item) => item.address}
                    renderItem={({ item, index }) => (
                        <Swipeable
                            ref={(el) => {
                                swipableRefs.current[item.address] = el;
                            }}
                            renderLeftActions={(progress, dragX) => renderLeftActions(progress, dragX, index)}
                            renderRightActions={(progress, dragX) => renderRightActions(progress, dragX, index)}
                            onSwipeableRightWillOpen={() => deleteAddress(item)}
                            onSwipeableLeftWillOpen={() => editAddress(item)}
                        >
                            <AddressBookItem
                                addressBookItem={item}
                                onPress={() => (type === 'choose-address' ? chooseAddress(item) : null)}
                            />
                        </Swipeable>
                    )}
                />
            )}
        </SafeAreaView>
    );
};

export default AddressBook;
