import React, { useEffect, useState } from 'react';
import { KeyboardAvoidingView, Platform, SafeAreaView, StyleSheet, Switch, Text, View } from 'react-native';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import Clipboard from '@react-native-community/clipboard';
import Config from 'react-native-config';
import { showMessage } from 'react-native-flash-message';
import { isAddress } from '../../services/addressService';
import { saveAddressBookItem } from '../../redux/actions/appActions';
import { Button, DismissKeyboard, FocusAwareStatusBar, HStack, Input, VStack } from '../../components';

interface ManageAddressBookItemProps {
    navigation: any;
    route: any;
}

const ManageAddressBookItem: React.FC<ManageAddressBookItemProps> = ({
    navigation,
    route,
}: ManageAddressBookItemProps) => {
    const { t } = useTranslation('manageAddressBookItem');
    const { type, ...params } = route.params ?? { address: '', title: '', favorite: false };
    const dispatch = useDispatch();
    const [address, setAddress] = useState<string>(params.address);
    const [title, setTitle] = useState<string>(params.title);
    const [favorite, setFavorite] = useState<boolean>(params.favorite);
    const [isClipboardActive, setIsClipboardActive] = useState(false);

    const saveAddressBook = () => {
        dispatch(saveAddressBookItem({ address, title, favorite }));
        navigation.goBack();

        showMessage({
            message: t('alerts.addressSaved.message'),
            description: t('alerts.addressSaved.description'),
            backgroundColor: '#614384',
        });
    };

    const getFromClipboard = () => {
        Clipboard.getString().then((string) => {
            const addresses = string.match(new RegExp(Config.ADDRESS_REGEX));

            if (addresses && addresses.length > 0) {
                setAddress(addresses[0]);
            }
        });
    };

    const getFromQRCode = () => {
        navigation.navigate('QRCodeScanner', {
            type: 'address-book',
        });
    };

    useEffect(() => {
        navigation.setOptions({
            title: type === 'edit' ? t('editAddress') : t('addAddress'),
        });

        if (Clipboard.hasString()) {
            Clipboard.getString().then((string) => {
                const addresses = string.match(new RegExp(Config.ADDRESS_REGEX));

                if (addresses && addresses.length > 0) {
                    setIsClipboardActive(true);
                }
            });
        }
    }, []);

    useEffect(() => {
        if (route.params?.address) {
            setAddress(route.params.address);
        }

        if (route.params?.title) {
            setTitle(route.params.title);
        }

        if (route.params?.favorite) {
            setFavorite(route.params.favorite);
        }
    }, [route.params]);

    return (
        <DismissKeyboard>
            <SafeAreaView style={styles.container}>
                <FocusAwareStatusBar barStyle="dark-content" backgroundColor="white" />
                <KeyboardAvoidingView
                    style={styles.container}
                    behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
                    keyboardVerticalOffset={100}
                >
                    <VStack style={styles.alignment} flex={1} alignItems="flex-start" justifyContent="flex-start">
                        <View style={styles.inputContainer}>
                            <Text style={styles.caption}>{t('title.title')}</Text>
                            <Input
                                placeholder={t('title.placeholder')}
                                autoFocus
                                onChangeText={(text) => setTitle(text)}
                                value={title}
                            />
                        </View>
                        <View style={styles.inputContainer}>
                            <Text style={styles.caption}>{t('address.title')}</Text>
                            <Input
                                placeholder={t('address.placeholder')}
                                onChangeText={(text) => setAddress(text)}
                                value={address}
                            />
                            <HStack justifyContent="space-between">
                                <Button
                                    onPress={getFromQRCode}
                                    title={t('qrScanner')}
                                    size="md"
                                    border
                                    borderColor="#ECECEC"
                                    backgroundColor="white"
                                    color="black"
                                    mt={10}
                                    mr={10}
                                    flex={1}
                                />
                                <Button
                                    onPress={getFromClipboard}
                                    disabled={!isClipboardActive}
                                    title={t('clipboard')}
                                    size="md"
                                    border
                                    borderColor="#ECECEC"
                                    backgroundColor="white"
                                    color="black"
                                    mt={10}
                                    flex={1}
                                />
                            </HStack>
                        </View>
                        <HStack style={styles.inputContainer} justifyContent="space-between">
                            <Text style={{ fontSize: 16 }}>{t('favorite')}</Text>
                            <Switch
                                onValueChange={() => setFavorite(!favorite)}
                                value={favorite}
                                trackColor={{ false: '#767577', true: '#614384' }}
                            />
                        </HStack>
                    </VStack>
                    <HStack style={styles.alignment}>
                        <Button
                            title={type === 'edit' ? t('confirmButton.save') : t('confirmButton.add')}
                            onPress={saveAddressBook}
                            flex={1}
                            disabled={!isAddress(address) || !title || title.length === 0}
                            mb={20}
                        />
                    </HStack>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </DismissKeyboard>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    alignment: {
        paddingHorizontal: 20,
    },
    caption: {
        fontSize: 12,
        color: '#614384',
    },
    inputContainer: {
        width: '100%',
        marginBottom: 15,
    },
});

export default ManageAddressBookItem;
