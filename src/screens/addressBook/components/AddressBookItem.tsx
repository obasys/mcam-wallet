import React from 'react';
import { StyleSheet, Text, TouchableHighlight, TouchableHighlightProps, View } from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import { Avatar, HStack } from '../../../components';
import { AddressBook } from '../../../types';
import { base64ToHex } from '../../../services/commonService';

const styles = StyleSheet.create({
    container: {
        height: 80,
        padding: 20,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: '#EEEEEE',
        backgroundColor: 'white',
    },
    contentContainer: {
        marginLeft: 10,
    },
    topContentContainer: {
        marginBottom: 5,
    },
    titleText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#614384'
    },
    addressText: {
        fontSize: 12,
        opacity: 0.5,
        color: '#aaaaaa'
    },
    favoriteContainer: {
        marginRight: 4,
    },
});

interface AddressBookItemProps extends TouchableHighlightProps {
    style?: Record<string, unknown>;
    addressBookItem: AddressBook;
}

const AddressBookItem: React.FC<AddressBookItemProps> = ({
    style,
    addressBookItem,
    ...props
}: AddressBookItemProps) => {
    return (
        <TouchableHighlight {...props}>
            <View style={[styles.container, style]}>
                <View style={styles.contentContainer}>
                    <HStack justifyContent="flex-start" style={styles.topContentContainer}>
                        {addressBookItem.favorite && (
                            <View style={styles.favoriteContainer}>
                                <Ionicon name="star" size={20} color="#F7B500" />
                            </View>
                        )}
                        <Text style={styles.titleText}>{addressBookItem.title}</Text>
                    </HStack>
                    <Text style={styles.addressText}>{addressBookItem.address}</Text>
                </View>
            </View>
        </TouchableHighlight>
    );
};

export default AddressBookItem;
