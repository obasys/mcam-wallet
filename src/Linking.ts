import { Linking } from 'react-native';

const config = {
    screens: {
        ChooseWallet: {
            path: 'send',
        },
    },
};

const linking = {
    prefixes: ['aok://'],
    async getInitialURL() {
        // Check if app was opened from a deep link
        const url = await Linking.getInitialURL();
        console.log('url', url);
        if (url != null) {
            return url;
        }
    },
    config,
};

export default linking;
