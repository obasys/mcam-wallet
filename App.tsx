import React from 'react';
import FlashMessage from 'react-native-flash-message';
import { PersistGate } from 'redux-persist/es/integration/react';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { ModalStack, navigationRef } from './src/routes';
import {
    MigrationProvider,
    NetworkProvider,
    OnboardingProvider,
    PasswordProvider,
    SocketProvider,
    WalletProvider,
} from './src/providers';
import { persistor, store } from './src/redux/store';
import './localization';

const config = {
    screens: {
        RootStack: {
            screens: {
                HomeStack: {
                    screens: {
                        Home: {
                            path: 'send',
                        },
                    },
                },
            },
        },
    },
};

const linking = {
    prefixes: ['aok://'],
    config: config,
};

const App = () => {
    return (
        <SafeAreaProvider>
            <NetworkProvider>
                <Provider store={store}>
                    <PersistGate loading={null} persistor={persistor}>
                        <NavigationContainer ref={navigationRef} linking={linking}>
                            <MigrationProvider>
                                <SocketProvider>
                                    <PasswordProvider>
                                        <OnboardingProvider>
                                            <WalletProvider>
                                                <ModalStack />
                                            </WalletProvider>
                                        </OnboardingProvider>
                                    </PasswordProvider>
                                </SocketProvider>
                            </MigrationProvider>
                        </NavigationContainer>
                        <FlashMessage position="top" floating />
                    </PersistGate>
                </Provider>
            </NetworkProvider>
        </SafeAreaProvider>
    );
};

export default App;
